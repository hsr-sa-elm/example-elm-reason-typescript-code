# example Elm Reason Typescript Code

Example Code of the same application in:
- Elm
- Reason with Reason-React and Reductive
- TypeScript with React and Redux

This repository is for people who are interested in these languages and want to see code examples, which are a bit mor than a hello world.
The application is a small http caller and can be accessed [here](https://hsr-sa-elm.gitlab.io/example-elm-reason-typescript-code) (http calls can't actually be made, because the backend is not deployed).

Probably the most interesting part in this comparison is how side-effects are handled. This can be seen in those features:

- the http call
- All the state gets saved in the local storage
- Uses an external library to format JSON

Apart from side-effects, the general structure of how code is organized is interesting.
Reason and TypeScript are very similar here.



