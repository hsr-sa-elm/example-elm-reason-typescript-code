import { Elm } from './src/Main.elm';
import * as jsonFormatter from 'format-json';

const cacheKey = 'elm-app-data';

const getInitialData = () => {
  const initialDataAsString = localStorage.getItem(cacheKey);
  try {
    return JSON.parse(initialDataAsString);
  } catch (e) {
    return undefined;
  }
};

const app = Elm.Main.init({
  node: document.getElementById('root'),
  flags: getInitialData(),
});

app.ports.save.subscribe(data => {
  localStorage.setItem(cacheKey, JSON.stringify(data));
});

app.ports.askForFormattedBodyPort.subscribe(data => {
  const originalBody = data.body;
  try {
    const formattedBody = jsonFormatter.diffy(JSON.parse(data.body));
    app.ports.receiveFormattedBody.send({ originalBody, formattedBody });
  } catch (e) {
    console.log(e);
    app.ports.receiveFormattedBody.send({
      originalBody,
      formattedBody: originalBody,
    });
  }
});
