module HttpCallTest exposing (..)

import Expect exposing (Expectation)
import HttpCall exposing (..)
import Json.Decode as JD
import Json.Encode as JE
import Test exposing (..)


emptyRequest : CallRequest
emptyRequest =
    { request =
        { method = ""
        , url = ""
        }
    , headers = []
    , body = ""
    }


emptyCallResponse : CallResponse
emptyCallResponse =
    { status =
        { code = 0
        , message = ""
        }
    , headers = []
    , body = ""
    }


nonEmptyCallResponse : CallResponse
nonEmptyCallResponse =
    { status =
        { code = 200
        , message = "OkSDee"
        }
    , headers = [ { key = "first_key", value = "first_value" } ]
    , body = "SOME_TEXT"
    }


nonEmptyRequest : CallRequest
nonEmptyRequest =
    { request =
        { method = "THE_METHOD"
        , url = "https://test.com/path"
        }
    , headers =
        [ { id = "header-1"
          , key = "first header"
          , value = "first value"
          }
        , { id = "header-2"
          , key = "second header"
          , value = "second value"
          }
        ]
    , body = "SOME_TEXT"
    }


testCallRequestEncoderEmpty =
    test "empty" <|
        let
            emptyCallExp =
                JE.object
                    [ ( "request", JE.object [ ( "method", JE.string "" ), ( "url", JE.string "" ) ] )
                    , ( "headers", JE.list (\x -> JE.object []) [] )
                    , ( "body", JE.string "" )
                    ]
        in
        \_ ->
            callRequestEncoder emptyRequest
                |> Expect.equal emptyCallExp


testCallRequestEncoderNonEmpty =
    test "non empty" <|
        let
            nonEmptyCallExp =
                JE.object
                    [ ( "request", JE.object [ ( "method", JE.string "THE_METHOD" ), ( "url", JE.string "https://test.com/path" ) ] )
                    , ( "headers"
                      , JE.list (\x -> x)
                            [ JE.object [ ( "id", JE.string "header-1" ), ( "key", JE.string "first header" ), ( "value", JE.string "first value" ) ]
                            , JE.object [ ( "id", JE.string "header-2" ), ( "key", JE.string "second header" ), ( "value", JE.string "second value" ) ]
                            ]
                      )
                    , ( "body", JE.string "SOME_TEXT" )
                    ]
        in
        \_ ->
            callRequestEncoder nonEmptyRequest
                |> Expect.equal nonEmptyCallExp


testCallResultDecoderEmpty =
    test "empty decoding" <|
        let
            emptyJson =
                """
                        {"status":
                            { "code" : 0
                            , "message" : ""
                            }
                        , "headers" : []
                        , "body" : ""
                        }
                        """
        in
        \_ ->
            JD.decodeString callResponseDecoder emptyJson
                |> Expect.equal (Ok emptyCallResponse)


testCallResultDecoderNonEmpty =
    test "non empty decoding" <|
        let
            nonEmptyJson =
                """
                        {"status":
                            { "code" : 200
                            , "message" : "OkSDee"
                            }
                        , "headers" : [{"key":"first_key","value":"first_value"}]
                        , "body" : "SOME_TEXT"
                        }
                        """
        in
        \_ ->
            JD.decodeString callResponseDecoder nonEmptyJson
                |> Expect.equal (Ok nonEmptyCallResponse)


suite : Test
suite =
    describe "Model.HttpCall Encoder"
        [ describe "callRequestEncoder"
            [ testCallRequestEncoderEmpty
            , testCallRequestEncoderNonEmpty
            ]
        , describe "callResultDecoder"
            [ testCallResultDecoderEmpty
            , testCallResultDecoderNonEmpty
            ]
        ]
