module HistoryTest exposing (..)

import Expect
import History exposing (..)
import HttpCall exposing (..)
import Json.Decode as JD
import Test exposing (..)
import Time


exampleRequest : CallRequest
exampleRequest =
    { request =
        { method = "POST"
        , url = "https://test.com/path"
        }
    , headers =
        [ { id = "header-1"
          , key = "first header"
          , value = "first value"
          }
        , { id = "header-2"
          , key = "second header"
          , value = "second value"
          }
        ]
    , body = "SOME_TEXT"
    }


exampleResponse : CallResponse
exampleResponse =
    { status =
        { code = 400
        , message = "Bad request"
        }
    , headers = [ { key = "first_key", value = "first_value" } ]
    , body = "SOME_TEXT"
    }


examplePerformedHttpCall1 : PerformedHttpCall
examplePerformedHttpCall1 =
    { request = exampleRequest
    , response = exampleResponse
    , requestTime = Time.millisToPosix 0
    , responseTime = Time.millisToPosix 1
    }


examplePerformedHttpCall2 : PerformedHttpCall
examplePerformedHttpCall2 =
    { examplePerformedHttpCall1
        | response = { exampleResponse | body = "Some other response" }
    }


examplePerformedHttpCall3 : PerformedHttpCall
examplePerformedHttpCall3 =
    { examplePerformedHttpCall1
        | requestTime = Time.millisToPosix 500
        , responseTime = Time.millisToPosix 550
    }


examplePerformedHttpCall4 : PerformedHttpCall
examplePerformedHttpCall4 =
    { examplePerformedHttpCall1
        | requestTime = Time.millisToPosix 700
        , responseTime = Time.millisToPosix 750
    }


isEmptySuite : Test
isEmptySuite =
    describe "isEmpty"
        [ test "With empty history" <|
            \_ ->
                isEmpty empty
                    |> Expect.equal True
        , test "With non-empty history" <|
            \_ ->
                isEmpty (push examplePerformedHttpCall1 empty)
                    |> Expect.equal False
        ]


getSelectedSuite : Test
getSelectedSuite =
    describe "getSelected"
        [ test "With empty history" <|
            \_ ->
                getSelected empty
                    |> Expect.equal Nothing
        , test "With history with single entry" <|
            \_ ->
                getSelected (push examplePerformedHttpCall1 empty)
                    |> Expect.equal (Just examplePerformedHttpCall1)
        , test "With history with multiple entries" <|
            \_ ->
                getSelected
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                    )
                    |> Expect.equal (Just examplePerformedHttpCall2)
        ]


getSelectedResponseSuite : Test
getSelectedResponseSuite =
    describe "getSelectedResponse"
        [ test "With empty history" <|
            \_ ->
                getSelectedResponse empty
                    |> Expect.equal Nothing
        , test "With history with single entry" <|
            \_ ->
                getSelectedResponse (push examplePerformedHttpCall1 empty)
                    |> Expect.equal (Just examplePerformedHttpCall1.response)
        , test "With history with multiple entries" <|
            \_ ->
                getSelectedResponse
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                    )
                    |> Expect.equal (Just examplePerformedHttpCall2.response)
        ]


isSelectedSuite : Test
isSelectedSuite =
    describe "isSelected"
        [ test "With empty history" <|
            \_ ->
                isSelected examplePerformedHttpCall1 empty
                    |> Expect.equal False
        , test "With selected call" <|
            \_ ->
                isSelected examplePerformedHttpCall1 (push examplePerformedHttpCall1 empty)
                    |> Expect.equal True
        , test "With not selected call" <|
            \_ ->
                isSelected examplePerformedHttpCall2 (push examplePerformedHttpCall1 empty)
                    |> Expect.equal False
        ]


getAllHttpCallsSuite : Test
getAllHttpCallsSuite =
    describe "getAllHttpCalls"
        [ test "With empty history" <|
            \_ ->
                getAllHttpCalls empty
                    |> Expect.equal []
        , test "With history with single entry" <|
            \_ ->
                getAllHttpCalls (push examplePerformedHttpCall1 empty)
                    |> Expect.equal [ examplePerformedHttpCall1 ]
        , test "With history with multiple entries" <|
            \_ ->
                getAllHttpCalls
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                    )
                    |> Expect.equal [ examplePerformedHttpCall2, examplePerformedHttpCall1 ]
        , test "Without selection" <|
            \_ ->
                getAllHttpCalls
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> removeSelection
                    )
                    |> Expect.equal [ examplePerformedHttpCall2, examplePerformedHttpCall1 ]
        , test "With history containing a call multiple times" <|
            \_ ->
                getAllHttpCalls
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                        |> push examplePerformedHttpCall2
                    )
                    |> Expect.equal [ examplePerformedHttpCall2, examplePerformedHttpCall3, examplePerformedHttpCall2, examplePerformedHttpCall1 ]
        , test "With history containing a call multiple times and changed selection" <|
            \_ ->
                getAllHttpCalls
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                        |> push examplePerformedHttpCall2
                        |> selectHttpCall examplePerformedHttpCall2
                    )
                    |> Expect.equal [ examplePerformedHttpCall2, examplePerformedHttpCall3, examplePerformedHttpCall2, examplePerformedHttpCall1 ]
        ]


selectHttpCallSuite : Test
selectHttpCallSuite =
    describe "selectHttpCall"
        [ test "With not existing call" <|
            \_ ->
                selectHttpCall examplePerformedHttpCall3
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                    )
                    |> getSelected
                    |> Expect.equal Nothing
        , test "With not existing call and list with no selection" <|
            \_ ->
                selectHttpCall examplePerformedHttpCall3
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> removeSelection
                    )
                    |> getSelected
                    |> Expect.equal Nothing
        , test "With existing call" <|
            \_ ->
                selectHttpCall examplePerformedHttpCall1
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                    )
                    |> getSelected
                    |> Expect.equal (Just examplePerformedHttpCall1)
        , test "With existing call and list with no selection" <|
            \_ ->
                selectHttpCall examplePerformedHttpCall1
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> removeSelection
                    )
                    |> getSelected
                    |> Expect.equal (Just examplePerformedHttpCall1)
        , test "With call in middle of history" <|
            \_ ->
                selectHttpCall examplePerformedHttpCall3
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                        |> push examplePerformedHttpCall4
                    )
                    |> getSelected
                    |> Expect.equal (Just examplePerformedHttpCall3)
        ]


selectNextNewerHttpCallSuite : Test
selectNextNewerHttpCallSuite =
    describe "selectNextNewerHttpCall"
        [ test "With empty history" <|
            \_ ->
                selectNextNewerHttpCall empty
                    |> Expect.equal empty
        , test "With newest call selected" <|
            \_ ->
                selectNextNewerHttpCall
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                    )
                    |> Expect.all
                        [ getSelected >> Expect.equal (Just examplePerformedHttpCall3)
                        , getAllHttpCalls >> Expect.equal [ examplePerformedHttpCall3, examplePerformedHttpCall2, examplePerformedHttpCall1 ]
                        , hasNewerHttpCall >> Expect.equal False
                        , hasOlderHttpCall >> Expect.equal True
                        ]
        , test "With oldest call selected" <|
            \_ ->
                selectNextNewerHttpCall
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                        |> selectHttpCall examplePerformedHttpCall1
                    )
                    |> Expect.all
                        [ getSelected >> Expect.equal (Just examplePerformedHttpCall2)
                        , getAllHttpCalls >> Expect.equal [ examplePerformedHttpCall3, examplePerformedHttpCall2, examplePerformedHttpCall1 ]
                        , hasNewerHttpCall >> Expect.equal True
                        , hasOlderHttpCall >> Expect.equal True
                        ]
        ]


selectNextOlderHttpCallSuite : Test
selectNextOlderHttpCallSuite =
    describe "selectNextOlderHttpCall"
        [ test "With empty history" <|
            \_ ->
                selectNextOlderHttpCall empty
                    |> Expect.equal empty
        , test "With newest call selected" <|
            \_ ->
                selectNextOlderHttpCall
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                    )
                    |> Expect.all
                        [ getSelected >> Expect.equal (Just examplePerformedHttpCall2)
                        , getAllHttpCalls >> Expect.equal [ examplePerformedHttpCall3, examplePerformedHttpCall2, examplePerformedHttpCall1 ]
                        , hasNewerHttpCall >> Expect.equal True
                        , hasOlderHttpCall >> Expect.equal True
                        ]
        , test "With oldest call selected" <|
            \_ ->
                selectNextOlderHttpCall
                    (empty
                        |> push examplePerformedHttpCall1
                        |> push examplePerformedHttpCall2
                        |> push examplePerformedHttpCall3
                        |> selectHttpCall examplePerformedHttpCall1
                    )
                    |> Expect.all
                        [ getSelected >> Expect.equal (Just examplePerformedHttpCall1)
                        , getAllHttpCalls >> Expect.equal [ examplePerformedHttpCall3, examplePerformedHttpCall2, examplePerformedHttpCall1 ]
                        , hasNewerHttpCall >> Expect.equal True
                        , hasOlderHttpCall >> Expect.equal False
                        ]
        ]


encodeDecodeSuite : Test
encodeDecodeSuite =
    let
        nonEmptyHistory =
            empty
                |> push examplePerformedHttpCall4
                |> push examplePerformedHttpCall3
                |> push examplePerformedHttpCall2
                |> push examplePerformedHttpCall1
                |> selectHttpCall examplePerformedHttpCall2
    in
    describe "historyEncoder and historyDecoder"
        [ test "With empty history" <|
            \_ ->
                historyEncoder empty
                    |> JD.decodeValue historyDecoder
                    |> Expect.equal (Ok empty)
        , test "With history with entries" <|
            \_ ->
                historyEncoder nonEmptyHistory
                    |> JD.decodeValue historyDecoder
                    |> Expect.equal (Ok nonEmptyHistory)
        ]
