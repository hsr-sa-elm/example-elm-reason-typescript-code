module Tabs.UpdateTest exposing (..)

import Expect
import History exposing (..)
import HttpCall
import Tabs.Model exposing (..)
import Tabs.Update exposing (..)
import Test exposing (..)


exampleRequest : HttpCall.CallRequest
exampleRequest =
    { request =
        { method = "POST"
        , url = "http://test.com"
        }
    , headers =
        [ { id = "header-1", key = "Content-Type", value = "application/json" }
        , { id = "header-2", key = "Cache-Control", value = "no-cache" }
        , { id = "header-3", key = "", value = "" }
        ]
    , body = ""
    }


exampleTab : TabModel
exampleTab =
    { id = ""
    , responseState = ShowSelectedResponse
    , editState = Unchanged
    , requestToEdit = exampleRequest
    , history = History.empty
    }


updateSuite : Test
updateSuite =
    describe "Tab.update"
        [ test "set method" <|
            \_ ->
                update (SetMethod "PATCH") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit = updateMethod "PATCH" exampleTab.requestToEdit
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "set url" <|
            \_ ->
                update (SetUrl "http://x.com") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit =
                                updateUrl "http://x.com" exampleTab.requestToEdit
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "set body" <|
            \_ ->
                update (SetBody "some body") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit = { exampleRequest | body = "some body" }
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "set header key" <|
            \_ ->
                update (SetHeaderKey "header-2" "some key") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit =
                                { exampleRequest
                                    | headers =
                                        [ { id = "header-1", key = "Content-Type", value = "application/json" }
                                        , { id = "header-2", key = "some key", value = "no-cache" }
                                        , { id = "header-3", key = "", value = "" }
                                        ]
                                }
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "set header key with non existing header id" <|
            \_ ->
                update (SetHeaderKey "header-10" "some key") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | editState = Changed
                          }
                        , Cmd.none
                        )
        , test "set header key of last header" <|
            \_ ->
                update (SetHeaderKey "header-3" "some key") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit =
                                { exampleRequest
                                    | headers =
                                        [ { id = "header-1", key = "Content-Type", value = "application/json" }
                                        , { id = "header-2", key = "Cache-Control", value = "no-cache" }
                                        , { id = "header-3", key = "some key", value = "" }
                                        , { id = "header-4", key = "", value = "" }
                                        ]
                                }
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "set header value" <|
            \_ ->
                update (SetHeaderValue "header-2" "some value") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit =
                                { exampleRequest
                                    | headers =
                                        [ { id = "header-1", key = "Content-Type", value = "application/json" }
                                        , { id = "header-2", key = "Cache-Control", value = "some value" }
                                        , { id = "header-3", key = "", value = "" }
                                        ]
                                }
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "remove header" <|
            \_ ->
                update (RemoveHeader "header-2") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | requestToEdit =
                                { exampleRequest
                                    | headers =
                                        [ { id = "header-1", key = "Content-Type", value = "application/json" }
                                        , { id = "header-3", key = "", value = "" }
                                        ]
                                }
                            , editState = Changed
                          }
                        , Cmd.none
                        )
        , test "remove last header" <|
            \_ ->
                update (RemoveHeader "header-3") exampleTab
                    |> Expect.equal
                        ( { exampleTab
                            | editState = Changed
                          }
                        , Cmd.none
                        )
        ]
