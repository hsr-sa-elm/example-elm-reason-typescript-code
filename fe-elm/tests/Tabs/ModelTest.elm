module Tabs.ModelTest exposing (..)

import Expect
import History
import HttpCall exposing (..)
import Json.Decode as JD
import Tabs.Model exposing (..)
import Test exposing (..)


getInitialTabModel : String -> TabModel
getInitialTabModel id =
    { id = id
    , responseState = ShowSelectedResponse
    , editState = Unchanged
    , requestToEdit =
        { request =
            { method = "GET"
            , url = ""
            }
        , headers =
            [ { id = "header-1", key = "Content-Type", value = "application/json" }
            , { id = "header-2", key = "Cache-Control", value = "no-cache" }
            , { id = "header-3", key = "", value = "" }
            ]
        , body = ""
        }
    , history = History.empty
    }


exampleRequest : CallRequest
exampleRequest =
    { request =
        { method = "POST"
        , url = "https://test.com/path"
        }
    , headers =
        [ { id = "header-1"
          , key = "first header"
          , value = "first value"
          }
        , { id = "header-2"
          , key = "second header"
          , value = "second value"
          }
        ]
    , body = "SOME_TEXT"
    }


encodeDecodeSuite : Test
encodeDecodeSuite =
    describe "tabsEncoder and tabsDecoder"
        [ let
            model =
                initial
                    |> addTab
                    |> updateTab (\_ tab -> ( { tab | editState = Unchanged, requestToEdit = exampleRequest, history = History.empty }, Cmd.none )) "tab-1" (SetMethod "POST")
                    |> Tuple.first
          in
          test "With unchanged edit state" <|
            \_ ->
                tabsEncoder model
                    |> JD.decodeValue tabsDecoder
                    |> Expect.equal (Ok model)
        , let
            model =
                initial
                    |> addTab
                    |> updateTab (\_ tab -> ( { tab | editState = Changed, requestToEdit = exampleRequest, history = History.empty }, Cmd.none )) "tab-1" (SetMethod "POST")
                    |> Tuple.first
          in
          test "With changed edit state" <|
            \_ ->
                tabsEncoder model
                    |> JD.decodeValue tabsDecoder
                    |> Expect.equal (Ok model)
        , let
            model =
                initial
                    |> addTab
                    |> updateTab (\_ tab -> ( { tab | editState = Overwritten exampleRequest, requestToEdit = exampleRequest, history = History.empty }, Cmd.none )) "tab-1" (SetMethod "POST")
                    |> Tuple.first
          in
          test "With overwritten edit state" <|
            \_ ->
                tabsEncoder model
                    |> JD.decodeValue tabsDecoder
                    |> Expect.equal (Ok model)
        ]


tabSuite : Test
tabSuite =
    describe "tabs"
        [ let
            state =
                initial
          in
          describe "given initial state"
            [ test "have initial tab data" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal (Just (getInitialTabModel "tab-1"))
            , test "contain one tab" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal [ { isSelected = True, tab = getInitialTabModel "tab-1" } ]
            ]
        , let
            state =
                initial |> addTab
          in
          describe "given a tab is added"
            [ test "select the new tab" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal (Just (getInitialTabModel "tab-2"))
            , test "contain two tabs" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal
                            [ { isSelected = False, tab = getInitialTabModel "tab-1" }
                            , { isSelected = True, tab = getInitialTabModel "tab-2" }
                            ]
            ]
        , let
            state =
                initial
                    |> addTab
                    |> selectTab "tab-1"
          in
          describe "given a tab is added and the old one gets selected"
            [ test "select the old tab" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal (Just (getInitialTabModel "tab-1"))
            , test "contain two tabs" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal
                            [ { isSelected = True, tab = getInitialTabModel "tab-1" }
                            , { isSelected = False, tab = getInitialTabModel "tab-2" }
                            ]
            ]
        , let
            state =
                initial
                    |> addTab
                    |> selectTab "tab-3"
          in
          describe "given a tab is added and a non existing tab gets selected"
            [ test "select the non existing tab" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal Nothing
            , test "contain the two not selected tabs" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal
                            [ { isSelected = False, tab = getInitialTabModel "tab-1" }
                            , { isSelected = False, tab = getInitialTabModel "tab-2" }
                            ]
            ]
        , let
            state =
                initial
                    |> addTab
                    |> addTab
                    |> removeTab "tab-2"
          in
          describe "given a not selected tab is removed"
            [ test "not changed the selected tab" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal (Just (getInitialTabModel "tab-3"))
            , test "not contain the removed tab" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal
                            [ { isSelected = False, tab = getInitialTabModel "tab-1" }
                            , { isSelected = True, tab = getInitialTabModel "tab-3" }
                            ]
            ]
        , let
            state =
                initial
                    |> addTab
                    |> addTab
                    |> removeTab "tab-3"
          in
          describe "given the selected tab is removed"
            [ test "select the first tab" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal (Just (getInitialTabModel "tab-1"))
            , test "not contain the removed tab" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal
                            [ { isSelected = True, tab = getInitialTabModel "tab-1" }
                            , { isSelected = False, tab = getInitialTabModel "tab-2" }
                            ]
            ]
        , let
            state =
                initial
                    |> removeTab "tab-1"
          in
          describe "given the only tab is removed"
            [ test "have no tab selected" <|
                \_ ->
                    state
                        |> getSelected
                        |> Expect.equal Nothing
            , test "not contain the removed tab" <|
                \_ ->
                    state
                        |> getAllTabs
                        |> Expect.equal []
            ]
        ]
