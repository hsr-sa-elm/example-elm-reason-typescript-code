module Tabs.ViewTest exposing (..)

import Expect
import History
import Html.Attributes exposing (placeholder, value)
import HttpCall
import Tabs.Model exposing (..)
import Tabs.View exposing (..)
import Test exposing (..)
import Test.Html.Event as Event
import Test.Html.Query as Query
import Test.Html.Selector exposing (..)
import Time


exampleRequest : HttpCall.CallRequest
exampleRequest =
    { request =
        { method = "POST"
        , url = "http://example.com"
        }
    , headers =
        [ { id = "header-1", key = "Content-Type", value = "application/json" }
        , { id = "header-2", key = "Cache-Control", value = "no-cache" }
        , { id = "header-3", key = "", value = "" }
        ]
    , body = "foo"
    }


viewWithRequestAndEditState : HttpCall.CallRequest -> EditState -> Query.Single TabMsg
viewWithRequestAndEditState request editState =
    view True Time.utc { id = "tab-1", responseState = ShowSelectedResponse, editState = editState, requestToEdit = request, history = History.empty } ""
        |> Query.fromHtml


viewSuite : Test
viewSuite =
    describe "Request component"
        [ let
            output =
                viewWithRequestAndEditState exampleRequest Unchanged
          in
          describe "render request"
            [ test "method" <|
                \_ ->
                    output
                        |> Query.find [ id "method" ]
                        |> Query.has [ attribute (value "POST") ]
            , test "url" <|
                \_ ->
                    output
                        |> Query.find [ id "url" ]
                        |> Query.has [ attribute (value "http://example.com") ]
            , test "body" <|
                \_ ->
                    output
                        |> Query.find [ id "body" ]
                        |> Query.has [ attribute (value "foo") ]
            , test "header keys" <|
                \_ ->
                    output
                        |> Query.findAll [ attribute (placeholder "Name") ]
                        |> Expect.all
                            [ Query.count (Expect.equal 3)
                            , Query.index 0 >> Query.has [ attribute (value "Content-Type") ]
                            , Query.index 1 >> Query.has [ attribute (value "Cache-Control") ]
                            , Query.index 2 >> Query.has [ attribute (value "") ]
                            ]
            , test "header values" <|
                \_ ->
                    output
                        |> Query.findAll [ attribute (placeholder "Value") ]
                        |> Expect.all
                            [ Query.count (Expect.equal 3)
                            , Query.index 0 >> Query.has [ attribute (value "application/json") ]
                            , Query.index 1 >> Query.has [ attribute (value "no-cache") ]
                            , Query.index 2 >> Query.has [ attribute (value "") ]
                            ]
            , test "header remove buttons" <|
                \_ ->
                    output
                        |> Query.findAll [ tag "button", containing [ text "remove" ] ]
                        |> Query.count (Expect.equal 2)
            ]
        , let
            output =
                viewWithRequestAndEditState exampleRequest Changed
          in
          describe "update handler"
            [ test "method" <|
                \_ ->
                    output
                        |> Query.find [ id "method" ]
                        |> Event.simulate (Event.input "PUT")
                        |> Event.expect (SetMethod "PUT")
            , test "url" <|
                \_ ->
                    output
                        |> Query.find [ id "url" ]
                        |> Event.simulate (Event.input "http://x.com")
                        |> Event.expect (SetUrl "http://x.com")
            , test "body" <|
                \_ ->
                    output
                        |> Query.find [ id "body" ]
                        |> Event.simulate (Event.input "bar")
                        |> Event.expect (SetBody "bar")
            , test "header key" <|
                \_ ->
                    output
                        |> Query.findAll [ attribute (placeholder "Name") ]
                        |> Query.index 1
                        |> Event.simulate (Event.input "some key")
                        |> Event.expect (SetHeaderKey "header-2" "some key")
            , test "header value" <|
                \_ ->
                    output
                        |> Query.findAll [ attribute (placeholder "Value") ]
                        |> Query.index 2
                        |> Event.simulate (Event.input "some value")
                        |> Event.expect (SetHeaderValue "header-3" "some value")
            , test "remove header" <|
                \_ ->
                    output
                        |> Query.findAll [ tag "button", containing [ text "remove" ] ]
                        |> Query.index 1
                        |> Event.simulate Event.click
                        |> Event.expect (RemoveHeader "header-2")
            , test "send" <|
                \_ ->
                    output
                        |> Query.find [ id "button-send" ]
                        |> Event.simulate Event.click
                        |> Event.expect
                            (PerformRequest exampleRequest)
            ]
        , let
            output =
                viewWithRequestAndEditState exampleRequest (Overwritten { exampleRequest | body = "other body" })
          in
          describe "with overwritten request"
            [ test "undo" <|
                \_ ->
                    output
                        |> Query.find [ tag "button", containing [ text "Undo" ] ]
                        |> Event.simulate Event.click
                        |> Event.expect
                            (RevertRequest { exampleRequest | body = "other body" })
            ]
        ]
