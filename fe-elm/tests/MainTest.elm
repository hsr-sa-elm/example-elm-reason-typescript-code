module MainTest exposing (..)

import HttpCall exposing (..)
import Main exposing (..)
import Tabs.Model exposing (..)
import Test exposing (..)
import Test.Html.Event as Event
import Test.Html.Query as Query
import Test.Html.Selector exposing (..)
import Time


exampleRequest : CallRequest
exampleRequest =
    { request =
        { method = "POST"
        , url = "https://test.com/path"
        }
    , headers =
        [ { id = "header-1"
          , key = "first header"
          , value = "first value"
          }
        , { id = "header-2"
          , key = "second header"
          , value = "second value"
          }
        ]
    , body = "SOME_TEXT"
    }


exampleModel : Model
exampleModel =
    { zone = Time.utc
    , formattedBody = ""
    , originalBody = ""
    , tabs =
        initial
            |> updateTab (\_ tab -> ( { tab | editState = Unchanged, requestToEdit = exampleRequest }, Cmd.none )) "tab-1" (SetMethod "POST")
            |> Tuple.first
    , isProViewVisible = True
    }


viewSuite : Test
viewSuite =
    let
        output =
            view exampleModel
                |> Query.fromHtml
    in
    describe "Main.view"
        [ test "Trigger HTTP call on button click" <|
            \_ ->
                output
                    |> Query.find [ id "button-send" ]
                    |> Event.simulate Event.click
                    |> Event.expect
                        (TabMessage "tab-1" (PerformRequest exampleRequest))
        ]
