module Util.TimeFormatTest exposing (..)

import Expect
import Test exposing (..)
import Time
import Util.TimeFormat exposing (..)


toDateTimeStringSuite : Test
toDateTimeStringSuite =
    describe "TimeFormat.toDateTimeString"
        [ test "With zero" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 0)
                    |> Expect.equal "01. January 1970 00:00:00"
        , test "With date in january" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1548315114992)
                    |> Expect.equal "24. January 2019 07:31:54"
        , test "With date in february" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1549915114992)
                    |> Expect.equal "11. February 2019 19:58:34"
        , test "With date in march" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1551915114992)
                    |> Expect.equal "06. March 2019 23:31:54"
        , test "With date in april" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1554115114992)
                    |> Expect.equal "01. April 2019 10:38:34"
        , test "With date in may" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1557115114992)
                    |> Expect.equal "06. May 2019 03:58:34"
        , test "With date in june" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1560515114992)
                    |> Expect.equal "14. June 2019 12:25:14"
        , test "With date in july" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1564515114992)
                    |> Expect.equal "30. July 2019 19:31:54"
        , test "With date in august" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1565515114992)
                    |> Expect.equal "11. August 2019 09:18:34"
        , test "With date in september" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1567715114992)
                    |> Expect.equal "05. September 2019 20:25:14"
        , test "With date in october" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1571925144992)
                    |> Expect.equal "24. October 2019 13:52:24"
        , test "With date in november" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1572715114992)
                    |> Expect.equal "02. November 2019 17:18:34"
        , test "With date in december" <|
            \_ ->
                toDateTimeString Time.utc (Time.millisToPosix 1575715114992)
                    |> Expect.equal "07. December 2019 10:38:34"
        ]
