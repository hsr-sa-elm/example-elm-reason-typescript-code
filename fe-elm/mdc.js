import './src/styles/main.scss';
import {
  autoInit,
  drawer,
  list,
  ripple,
  select,
  switchControl,
  textField,
} from 'material-components-web';

autoInit.deregisterAll();
autoInit.register('MDCDrawer', drawer.MDCDrawer);
autoInit.register('MDCTextField', textField.MDCTextField);
autoInit.register('MDCRipple', ripple.MDCRipple);
autoInit.register('MDCList', list.MDCList);
autoInit.register('MDCSelect', select.MDCSelect);
autoInit.register('MDCSwitch', switchControl.MDCSwitch);

const init = () => {
  autoInit();
};
init();
new MutationObserver(init).observe(document.querySelector('.main-wrapper'), {
  childList: true,
});

const mdcDrawer = document.querySelector('.mdc-drawer').MDCDrawer;

const closeDrawer = () => {
  mdcDrawer.open = false;
};

document.querySelector('.sidebar-toggle').addEventListener('click', () => {
  mdcDrawer.open = true;

  document
    .querySelectorAll('.mdc-drawer .mdc-list-item, .mdc-drawer button')
    .forEach(el => {
      el.removeEventListener('click', closeDrawer);
      el.addEventListener('click', closeDrawer);
    });
});
