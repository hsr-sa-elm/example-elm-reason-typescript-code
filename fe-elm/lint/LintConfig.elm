module LintConfig exposing (config)

import Lint.Rule exposing (Rule)
import NoUnused.Variables


config : List Rule
config =
    [ NoUnused.Variables.rule
    ]
