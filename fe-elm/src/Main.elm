port module Main exposing (..)

import Browser
import History
import Html exposing (..)
import Html.Attributes exposing (..)
import HttpCall exposing (..)
import Json.Decode as JD
import Json.Encode as JE
import Tabs.Model as Tabs
import Tabs.Update
import Tabs.View
import Task
import Time
import Util.MaterialDesignComponents as MDC


main =
    Browser.element { init = init, update = updateWithSaveState, view = view, subscriptions = subscriptions }



-- MODEL


type alias Model =
    { zone : Time.Zone
    , tabs : Tabs.Tabs
    , formattedBody : String
    , originalBody : String
    , isProViewVisible : Bool
    }


init : JD.Value -> ( Model, Cmd Msg )
init initialState =
    ( case JD.decodeValue modelDecoder initialState of
        Ok model ->
            model

        Err _ ->
            { zone = Time.utc
            , tabs = Tabs.initial
            , formattedBody = ""
            , originalBody = ""
            , isProViewVisible = True
            }
    , Task.perform AdjustTimeZone Time.here
    )



-- UPDATE


type alias FormattedAndOriginalBody =
    { originalBody : String, formattedBody : String }


type Msg
    = AdjustTimeZone Time.Zone
    | TabMessage String Tabs.TabMsg
    | AddTab
    | RemoveTab String
    | SelectTab String
    | FormattedResponse FormattedAndOriginalBody
    | ChangeProViewVisibility Bool


port save : JE.Value -> Cmd msg


port askForFormattedBodyPort : HttpCall.CallResponse -> Cmd msg


port receiveFormattedBody : (FormattedAndOriginalBody -> msg) -> Sub msg


saveModel : Model -> Cmd msg
saveModel =
    modelEncoder >> save


askForFormattedBody : Model -> Cmd Msg
askForFormattedBody model =
    getResponseForFormatting model
        |> Maybe.andThen
            (\response ->
                if response.body == model.originalBody then
                    Nothing

                else
                    Just (askForFormattedBodyPort response)
            )
        |> Maybe.withDefault Cmd.none


getResponse : Model -> Maybe HttpCall.CallResponse
getResponse model =
    Tabs.getSelected model.tabs
        |> Maybe.andThen (.history >> History.getSelectedResponse)


getResponseForFormatting : Model -> Maybe HttpCall.CallResponse
getResponseForFormatting model =
    let
        hasJsonHeader =
            .headers >> List.any (\header -> header.key == "Content-Type" && String.contains "application/json" header.value)

        hasJsonToMaybe response =
            if hasJsonHeader response then
                Just response

            else
                Nothing
    in
    getResponse model
        |> Maybe.andThen hasJsonToMaybe


setFormattedAndOriginalBodyIfNotJson : Model -> Model
setFormattedAndOriginalBodyIfNotJson model =
    case ( getResponse model, getResponseForFormatting model ) of
        ( Just _, Just _ ) ->
            model

        ( Just response, Nothing ) ->
            { model | formattedBody = response.body, originalBody = response.body }

        _ ->
            { model | formattedBody = "", originalBody = "" }


updateWithSaveState : Msg -> Model -> ( Model, Cmd Msg )
updateWithSaveState msg =
    update msg
        >> (\( model, cmd ) ->
                ( setFormattedAndOriginalBodyIfNotJson model
                , Cmd.batch [ cmd, saveModel model, askForFormattedBody model ]
                )
           )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AdjustTimeZone zone ->
            ( { model | zone = zone }, Cmd.none )

        TabMessage tabId tabMsg ->
            Tabs.updateTab Tabs.Update.update tabId tabMsg model.tabs
                |> Tuple.mapBoth (\tabs -> { model | tabs = tabs }) (Cmd.map (\updateTabMsg -> TabMessage tabId updateTabMsg))

        AddTab ->
            ( { model | tabs = Tabs.addTab model.tabs }, Cmd.none )

        RemoveTab id ->
            ( { model | tabs = Tabs.removeTab id model.tabs }, Cmd.none )

        SelectTab id ->
            ( { model | tabs = Tabs.selectTab id model.tabs }, Cmd.none )

        FormattedResponse { originalBody, formattedBody } ->
            ( { model | originalBody = originalBody, formattedBody = formattedBody }, Cmd.none )

        ChangeProViewVisibility isProViewVisible ->
            ( { model | isProViewVisible = isProViewVisible }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    receiveFormattedBody FormattedResponse



-- VIEW


view : Model -> Html Msg
view model =
    div
        [ class "root-container"
        , classList [ ( "simple-view", not model.isProViewVisible ) ]
        ]
        [ MDC.appBar "HTTP client in Elm" (MDC.switch "view-switch" "Pro view" model.isProViewVisible ChangeProViewVisibility)
        , aside [ attribute "data-mdc-auto-init" "MDCDrawer", class "mdc-drawer", class "mdc-drawer--modal" ]
            [ div [ class "mdc-drawer__content" ] (viewTabList model.tabs)
            ]
        , div [ class "mdc-drawer-scrim" ] []
        , div [ class "main-wrapper" ]
            ((if model.isProViewVisible then
                [ aside [] (viewTabList model.tabs) ]

              else
                []
             )
                ++ [ case Tabs.getSelected model.tabs of
                        Just tabModel ->
                            Tabs.View.view model.isProViewVisible model.zone tabModel model.formattedBody |> Html.map (\tabMsg -> TabMessage tabModel.id tabMsg)

                        Nothing ->
                            p [ class "no-tab-message" ] [ text "No tab selected" ]
                   ]
            )
        ]


viewTabList : Tabs.Tabs -> List (Html Msg)
viewTabList tabs =
    [ MDC.selectableList (List.map viewTabListItem (Tabs.getAllTabs tabs))
    , MDC.buttonWithIcon "add" "New tab" AddTab
    ]


viewTabListItem : { isSelected : Bool, tab : Tabs.TabModel } -> MDC.SelectableListItem Msg
viewTabListItem { isSelected, tab } =
    { firstLine = tab.requestToEdit.request.method
    , secondLine =
        case tab.requestToEdit.request.url of
            "" ->
                "No URL"

            url ->
                url
    , selected = isSelected
    , onClick = SelectTab tab.id
    , secondaryAction = Just { icon = "remove", onClick = RemoveTab tab.id }
    }


modelEncoder : Model -> JE.Value
modelEncoder { tabs, isProViewVisible } =
    JE.object
        [ ( "tabs", Tabs.tabsEncoder tabs )
        , ( "isProViewVisible", JE.bool isProViewVisible )
        ]


modelDecoder : JD.Decoder Model
modelDecoder =
    JD.map5
        Model
        (JD.succeed Time.utc)
        (JD.field "tabs" Tabs.tabsDecoder)
        (JD.succeed "")
        (JD.succeed "")
        (JD.field "isProViewVisible" JD.bool)
