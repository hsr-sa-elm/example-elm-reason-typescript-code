module History exposing (History, PerformedHttpCall, empty, getAllHttpCalls, getSelected, getSelectedResponse, hasNewerHttpCall, hasOlderHttpCall, historyDecoder, historyEncoder, isEmpty, isSelected, push, removeSelection, selectHttpCall, selectNextNewerHttpCall, selectNextOlderHttpCall)

import HttpCall exposing (..)
import Json.Decode as JD
import Json.Encode as JE
import Time


type alias PerformedHttpCall =
    { request : CallRequest
    , response : CallResponse
    , requestTime : Time.Posix
    , responseTime : Time.Posix
    }


type History
    = History HistoryData


type alias HistoryData =
    { selected : Maybe PerformedHttpCall, newer : List PerformedHttpCall, older : List PerformedHttpCall }


empty : History
empty =
    History { selected = Nothing, newer = [], older = [] }


isEmpty : History -> Bool
isEmpty (History { selected, newer, older }) =
    case ( selected, newer, older ) of
        ( Nothing, [], [] ) ->
            True

        _ ->
            False


push : PerformedHttpCall -> History -> History
push call =
    getAllHttpCalls >> (\httpCalls -> History { selected = Just call, newer = [], older = httpCalls })


getSelected : History -> Maybe PerformedHttpCall
getSelected (History { selected }) =
    selected


getSelectedResponse : History -> Maybe CallResponse
getSelectedResponse =
    getSelected >> Maybe.map .response


isSelected : PerformedHttpCall -> History -> Bool
isSelected call (History { selected }) =
    selected == Just call


getAllHttpCalls : History -> List PerformedHttpCall
getAllHttpCalls (History { selected, newer, older }) =
    newer
        ++ (case selected of
                Just selectedCall ->
                    [ selectedCall ]

                Nothing ->
                    []
           )
        ++ older


splitListOn : a -> List a -> ( Bool, List a, List a )
splitListOn element =
    List.foldl
        (\current ( found, left, right ) ->
            case found of
                True ->
                    ( True, left, right ++ [ current ] )

                False ->
                    if current == element then
                        ( True, left, right )

                    else
                        ( False, left ++ [ current ], right )
        )
        ( False, [], [] )


selectHttpCall : PerformedHttpCall -> History -> History
selectHttpCall call =
    getAllHttpCalls
        >> splitListOn call
        >> (\( found, newer, older ) ->
                case found of
                    True ->
                        History { selected = Just call, newer = newer, older = older }

                    False ->
                        History { selected = Nothing, newer = newer, older = [] }
           )


getSelectedAsList : Maybe PerformedHttpCall -> List PerformedHttpCall
getSelectedAsList selected =
    case selected of
        Just call ->
            [ call ]

        Nothing ->
            []


hasNewerHttpCall : History -> Bool
hasNewerHttpCall (History { newer }) =
    List.isEmpty newer |> not


hasOlderHttpCall : History -> Bool
hasOlderHttpCall (History { older }) =
    List.isEmpty older |> not


selectNextNewerHttpCall : History -> History
selectNextNewerHttpCall (History { selected, newer, older }) =
    case List.reverse newer of
        [] ->
            History { selected = selected, newer = newer, older = older }

        next :: rest ->
            History
                { selected = Just next
                , newer = List.reverse rest
                , older = getSelectedAsList selected ++ older
                }


selectNextOlderHttpCall : History -> History
selectNextOlderHttpCall (History { selected, newer, older }) =
    case older of
        [] ->
            History { selected = selected, newer = newer, older = older }

        next :: rest ->
            History
                { selected = Just next
                , newer = newer ++ getSelectedAsList selected
                , older = rest
                }


removeSelection : History -> History
removeSelection =
    getAllHttpCalls
        >> (\newer -> { selected = Nothing, newer = newer, older = [] })
        >> History


historyEncoder : History -> JE.Value
historyEncoder (History { selected, newer, older }) =
    let
        selectedEncoder =
            case selected of
                Just call ->
                    callEncoder call

                Nothing ->
                    JE.null
    in
    JE.object
        [ ( "selected", selectedEncoder )
        , ( "newer", JE.list callEncoder newer )
        , ( "older", JE.list callEncoder older )
        ]


historyDecoder : JD.Decoder History
historyDecoder =
    JD.map
        History
        (JD.map3
            HistoryData
            (JD.field "selected" (JD.nullable callDecoder))
            (JD.field "newer" (JD.list callDecoder))
            (JD.field "older" (JD.list callDecoder))
        )


callEncoder : PerformedHttpCall -> JE.Value
callEncoder { request, response, requestTime, responseTime } =
    JE.object
        [ ( "request", callRequestEncoder request )
        , ( "response", callResponseEncoder response )
        , ( "requestTime", Time.posixToMillis requestTime |> JE.int )
        , ( "responseTime", Time.posixToMillis responseTime |> JE.int )
        ]


callDecoder : JD.Decoder PerformedHttpCall
callDecoder =
    JD.map4
        PerformedHttpCall
        (JD.field "request" callRequestDecoder)
        (JD.field "response" callResponseDecoder)
        (JD.field "requestTime" (JD.map Time.millisToPosix JD.int))
        (JD.field "responseTime" (JD.map Time.millisToPosix JD.int))
