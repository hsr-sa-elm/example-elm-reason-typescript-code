module HttpCall exposing (CallRequest, CallRequestHeader, CallResponse, CallResponseHeader, callRequestDecoder, callRequestEncoder, callResponseDecoder, callResponseEncoder, failedCallDecoder)

import Json.Decode as JD
import Json.Encode as JE


type alias CallResponseHeader =
    { key : String
    , value : String
    }


type alias CallRequestHeader =
    { id : String
    , key : String
    , value : String
    }


type alias CallResponseStatus =
    { code : Int
    , message : String
    }


type alias CallResponse =
    { status : CallResponseStatus
    , headers :
        List CallResponseHeader
    , body : String
    }


type alias CallRequestRequestLine =
    { method : String
    , url : String
    }


type alias CallRequest =
    { request : CallRequestRequestLine
    , headers :
        List CallRequestHeader
    , body : String
    }


responseHeaderDecoder : JD.Decoder CallResponseHeader
responseHeaderDecoder =
    JD.map2
        CallResponseHeader
        (JD.field "key" JD.string)
        (JD.field "value" JD.string)


requestHeaderDecoder : JD.Decoder CallRequestHeader
requestHeaderDecoder =
    JD.map3
        CallRequestHeader
        (JD.field "id" JD.string)
        (JD.field "key" JD.string)
        (JD.field "value" JD.string)


callResponseStatusDecoder : JD.Decoder CallResponseStatus
callResponseStatusDecoder =
    JD.map2
        CallResponseStatus
        (JD.field "code" JD.int)
        (JD.field "message" JD.string)


callResponseDecoder : JD.Decoder CallResponse
callResponseDecoder =
    JD.map3
        CallResponse
        (JD.field "status" callResponseStatusDecoder)
        (JD.field "headers" (JD.list responseHeaderDecoder))
        (JD.field "body" JD.string)


callRequestRequestLineDecoder : JD.Decoder CallRequestRequestLine
callRequestRequestLineDecoder =
    JD.map2
        CallRequestRequestLine
        (JD.field "method" JD.string)
        (JD.field "url" JD.string)


callRequestDecoder : JD.Decoder CallRequest
callRequestDecoder =
    JD.map3
        CallRequest
        (JD.field "request" callRequestRequestLineDecoder)
        (JD.field "headers" (JD.list requestHeaderDecoder))
        (JD.field "body" JD.string)


responseHeaderEncoder : CallResponseHeader -> JE.Value
responseHeaderEncoder { key, value } =
    JE.object
        [ ( "key", JE.string key )
        , ( "value", JE.string value )
        ]


requestHeaderEncoder : CallRequestHeader -> JE.Value
requestHeaderEncoder { id, key, value } =
    JE.object
        [ ( "id", JE.string id )
        , ( "key", JE.string key )
        , ( "value", JE.string value )
        ]


callRequestEncoder : CallRequest -> JE.Value
callRequestEncoder { request, headers, body } =
    JE.object
        [ ( "request"
          , JE.object
                [ ( "method", JE.string request.method )
                , ( "url", JE.string request.url )
                ]
          )
        , ( "headers", JE.list requestHeaderEncoder headers )
        , ( "body", JE.string body )
        ]


callResponseEncoder : CallResponse -> JE.Value
callResponseEncoder { status, headers, body } =
    JE.object
        [ ( "status"
          , JE.object
                [ ( "code", JE.int status.code )
                , ( "message", JE.string status.message )
                ]
          )
        , ( "headers", JE.list responseHeaderEncoder headers )
        , ( "body", JE.string body )
        ]


failedCallDecoder : JD.Decoder String
failedCallDecoder =
    JD.field "message" JD.string
