module Tabs.Model exposing (EditState(..), ResponseState(..), TabModel, TabMsg(..), Tabs, addTab, getAllTabs, getSelected, initial, removeTab, selectTab, tabsDecoder, tabsEncoder, updateTab)

import History
import HttpCall exposing (..)
import Json.Decode as JD
import Json.Encode as JE
import Util.Id


type ResponseState
    = ShowSelectedResponse
    | Failure String
    | Loading


type EditState
    = Unchanged
    | Changed
    | Overwritten CallRequest


type alias TabModel =
    { id : String
    , editState : EditState
    , responseState : ResponseState
    , requestToEdit : CallRequest
    , history : History.History
    }


type alias TabsData =
    { selectedTab : String, tabs : List TabModel }


type Tabs
    = Tabs TabsData


type TabMsg
    = PerformRequest CallRequest
    | CallResultReceived (Result String History.PerformedHttpCall)
    | SetMethod String
    | SetUrl String
    | SetBody String
    | RemoveHeader String
    | SetHeaderValue String String
    | SetHeaderKey String String
    | SelectHistoryEntry History.PerformedHttpCall
    | SelectOlderHistoryEntry
    | SelectNewerHistoryEntry
    | RevertRequest CallRequest


getNextId =
    Util.Id.getNextId "tab-" .id


initial : Tabs
initial =
    Tabs { selectedTab = "", tabs = [] }
        |> addTab


getSelected : Tabs -> Maybe TabModel
getSelected (Tabs { selectedTab, tabs }) =
    List.filter (\{ id } -> id == selectedTab) tabs |> List.head


updateTab : (TabMsg -> TabModel -> ( TabModel, Cmd TabMsg )) -> String -> TabMsg -> Tabs -> ( Tabs, Cmd TabMsg )
updateTab updateFn tabId msg (Tabs { selectedTab, tabs }) =
    List.map
        (\tab ->
            if tab.id == tabId then
                updateFn msg tab

            else
                ( tab, Cmd.none )
        )
        tabs
        |> List.unzip
        |> Tuple.mapBoth (\updatedTabs -> Tabs { selectedTab = selectedTab, tabs = updatedTabs }) Cmd.batch


getAllTabs : Tabs -> List { isSelected : Bool, tab : TabModel }
getAllTabs (Tabs { selectedTab, tabs }) =
    List.map (\tab -> { isSelected = selectedTab == tab.id, tab = tab }) tabs


selectTab : String -> Tabs -> Tabs
selectTab tabId (Tabs { tabs }) =
    Tabs { selectedTab = tabId, tabs = tabs }


addTab : Tabs -> Tabs
addTab (Tabs { tabs }) =
    let
        id =
            getNextId tabs
    in
    Tabs
        { selectedTab = id
        , tabs =
            tabs
                ++ [ { id = id
                     , responseState = ShowSelectedResponse
                     , editState = Unchanged
                     , requestToEdit =
                        { request =
                            { method = "GET"
                            , url = ""
                            }
                        , headers =
                            [ { id = "header-1", key = "Content-Type", value = "application/json" }
                            , { id = "header-2", key = "Cache-Control", value = "no-cache" }
                            , { id = "header-3", key = "", value = "" }
                            ]
                        , body = ""
                        }
                     , history = History.empty
                     }
                   ]
        }


removeTab : String -> Tabs -> Tabs
removeTab tabId (Tabs { selectedTab, tabs }) =
    let
        newTabs =
            List.filter (\{ id } -> id /= tabId) tabs
    in
    Tabs
        { selectedTab =
            if tabId == selectedTab then
                List.head newTabs |> Maybe.map .id |> Maybe.withDefault ""

            else
                selectedTab
        , tabs = newTabs
        }


tabsEncoder : Tabs -> JE.Value
tabsEncoder (Tabs { selectedTab, tabs }) =
    JE.object
        [ ( "selectedTab", JE.string selectedTab )
        , ( "tabs", JE.list tabEncoder tabs )
        ]


tabsDecoder : JD.Decoder Tabs
tabsDecoder =
    JD.map2
        TabsData
        (JD.field "selectedTab" JD.string)
        (JD.field "tabs" (JD.list tabDecoder))
        |> JD.map Tabs


tabEncoder : TabModel -> JE.Value
tabEncoder { id, editState, requestToEdit, history } =
    JE.object
        [ ( "id", JE.string id )
        , ( "editState", editStateEncoder editState )
        , ( "requestToEdit", callRequestEncoder requestToEdit )
        , ( "history", History.historyEncoder history )
        ]


tabDecoder : JD.Decoder TabModel
tabDecoder =
    JD.map5
        TabModel
        (JD.field "id" JD.string)
        (JD.field "editState" editStateDecoder)
        (JD.succeed ShowSelectedResponse)
        (JD.field "requestToEdit" callRequestDecoder)
        (JD.field "history" History.historyDecoder)


editStateEncoder : EditState -> JE.Value
editStateEncoder editState =
    case editState of
        Unchanged ->
            JE.object
                [ ( "state", JE.string "UNCHANGED" )
                , ( "request", JE.null )
                ]

        Changed ->
            JE.object
                [ ( "state", JE.string "CHANGED" )
                , ( "request", JE.null )
                ]

        Overwritten request ->
            JE.object
                [ ( "state", JE.string "OVERWRITTEN" )
                , ( "request", callRequestEncoder request )
                ]


editStateDecoder : JD.Decoder EditState
editStateDecoder =
    JD.field "state" JD.string
        |> JD.andThen
            (\state ->
                case state of
                    "UNCHANGED" ->
                        JD.succeed Unchanged

                    "CHANGED" ->
                        JD.succeed Changed

                    "OVERWRITTEN" ->
                        JD.map Overwritten (JD.field "request" callRequestDecoder)

                    _ ->
                        JD.fail "Unknown edit state"
            )
