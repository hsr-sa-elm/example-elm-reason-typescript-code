module Tabs.Update exposing (..)

import Config
import History
import Http
import HttpCall exposing (..)
import Json.Decode as D
import Tabs.Model exposing (..)
import Task
import Time
import Util.Id


update : TabMsg -> TabModel -> ( TabModel, Cmd TabMsg )
update msg model =
    case msg of
        CallResultReceived (Ok call) ->
            ( { model
                | responseState = ShowSelectedResponse
                , editState = Unchanged
                , history =
                    History.push call model.history
              }
            , Cmd.none
            )

        CallResultReceived (Err error) ->
            ( { model | responseState = Failure error }, Cmd.none )

        PerformRequest request ->
            ( { model | responseState = Loading }
            , Task.attempt CallResultReceived (performRequestWithTimeStamps Config.backendApiUrl request)
            )

        SetMethod method ->
            ( { model | requestToEdit = updateMethod method model.requestToEdit, editState = Changed }, Cmd.none )

        SetUrl url ->
            ( { model | requestToEdit = updateUrl url model.requestToEdit, editState = Changed }, Cmd.none )

        SetBody body ->
            ( { model | requestToEdit = updateBody body model.requestToEdit, editState = Changed }, Cmd.none )

        SetHeaderKey id key ->
            ( { model | requestToEdit = updateHeaderKey id key model.requestToEdit, editState = Changed }, Cmd.none )

        SetHeaderValue id value ->
            ( { model | requestToEdit = updateHeaderValue id value model.requestToEdit, editState = Changed }, Cmd.none )

        RemoveHeader id ->
            ( { model | requestToEdit = removeHeader id model.requestToEdit, editState = Changed }, Cmd.none )

        SelectHistoryEntry call ->
            ( updateHistorySelection (History.selectHttpCall call) model
            , Cmd.none
            )

        SelectOlderHistoryEntry ->
            ( updateHistorySelection History.selectNextOlderHttpCall model
            , Cmd.none
            )

        SelectNewerHistoryEntry ->
            ( updateHistorySelection History.selectNextNewerHttpCall model
            , Cmd.none
            )

        RevertRequest oldRequest ->
            ( { model
                | requestToEdit = oldRequest
                , editState = Changed
              }
            , Cmd.none
            )


updateHistorySelection : (History.History -> History.History) -> TabModel -> TabModel
updateHistorySelection updateHistory model =
    updateHistory model.history
        |> (\history ->
                { model
                    | history = history
                    , requestToEdit =
                        case History.getSelected history of
                            Just call ->
                                call.request

                            Nothing ->
                                model.requestToEdit
                    , editState =
                        case model.editState of
                            Changed ->
                                Overwritten model.requestToEdit

                            _ ->
                                model.editState
                }
           )


updateRequestLine : { method : String, url : String } -> CallRequest -> CallRequest
updateRequestLine requestLine request =
    { request | request = requestLine }


updateMethod : String -> CallRequest -> CallRequest
updateMethod method request =
    request
        |> updateRequestLine
            (request.request
                |> (\requestLine -> { requestLine | method = method })
            )
        |> addHeaderRowIfLastHeaderRowIsNotEmpty


updateUrl : String -> CallRequest -> CallRequest
updateUrl url request =
    request
        |> updateRequestLine
            (request.request
                |> (\requestLine -> { requestLine | url = url })
            )
        |> addHeaderRowIfLastHeaderRowIsNotEmpty


updateBody : String -> CallRequest -> CallRequest
updateBody body request =
    { request | body = body }
        |> addHeaderRowIfLastHeaderRowIsNotEmpty


removeHeader : String -> CallRequest -> CallRequest
removeHeader id request =
    { request | headers = List.filter (\header -> header.id /= id) request.headers }
        |> addHeaderRowIfLastHeaderRowIsNotEmpty


updateHeaderKey : String -> String -> CallRequest -> CallRequest
updateHeaderKey id key =
    applyToHeader id (\header -> { header | key = key })
        >> addHeaderRowIfLastHeaderRowIsNotEmpty


updateHeaderValue : String -> String -> CallRequest -> CallRequest
updateHeaderValue id value =
    applyToHeader id (\header -> { header | value = value })
        >> addHeaderRowIfLastHeaderRowIsNotEmpty


applyToHeader : String -> (CallRequestHeader -> CallRequestHeader) -> CallRequest -> CallRequest
applyToHeader id fn request =
    { request
        | headers =
            List.map
                (\header ->
                    if header.id == id then
                        fn header

                    else
                        header
                )
                request.headers
    }


getNextHeaderId =
    Util.Id.getNextId "header-" .id


addHeaderRowIfLastHeaderRowIsNotEmpty : CallRequest -> CallRequest
addHeaderRowIfLastHeaderRowIsNotEmpty request =
    let
        lastHeader =
            List.reverse request.headers |> List.head

        key =
            Maybe.map .key lastHeader

        value =
            Maybe.map .value lastHeader
    in
    case ( key, value ) of
        ( Just "", Just "" ) ->
            request

        _ ->
            { request
                | headers =
                    request.headers
                        ++ [ { id = getNextHeaderId request.headers
                             , key = ""
                             , value = ""
                             }
                           ]
            }


performRequestWithTimeStamps : String -> CallRequest -> Task.Task String History.PerformedHttpCall
performRequestWithTimeStamps url request =
    Time.now
        |> Task.andThen (\requestTime -> performRequest url request requestTime)
        |> Task.andThen (\addResponseTime -> Task.map addResponseTime Time.now)


performRequest : String -> CallRequest -> Time.Posix -> Task.Task String (Time.Posix -> History.PerformedHttpCall)
performRequest url request requestTime =
    Http.task
        { method = "POST"
        , headers = []
        , url = url
        , body =
            Http.jsonBody (callRequestEncoder { request | headers = List.filter (\{ key } -> key /= "") request.headers })
        , resolver =
            Http.stringResolver
                (\response ->
                    case response of
                        Http.BadUrl_ badUrl ->
                            Err ("Bad Url: " ++ badUrl)

                        Http.Timeout_ ->
                            Err "Http Timeout"

                        Http.NetworkError_ ->
                            Err "Network Error"

                        Http.BadStatus_ metadata body ->
                            case D.decodeString failedCallDecoder body of
                                Ok value ->
                                    Err value

                                Err err ->
                                    Err ("Bad Http Payload (Code: " ++ String.fromInt metadata.statusCode ++ "): " ++ D.errorToString err)

                        Http.GoodStatus_ _ body ->
                            case D.decodeString callResponseDecoder body of
                                Ok value ->
                                    Ok (\responseTime -> { request = request, response = value, requestTime = requestTime, responseTime = responseTime })

                                Err err ->
                                    Err ("Bad Http Payload: " ++ D.errorToString err)
                )
        , timeout = Nothing
        }
