module Tabs.View exposing (view)

import History
import Html exposing (..)
import Html.Attributes exposing (..)
import HttpCall exposing (..)
import Tabs.Model exposing (..)
import Time
import Util.MaterialDesignComponents as MDC
import Util.TimeFormat


view : Bool -> Time.Zone -> TabModel -> String -> Html TabMsg
view isProViewVisible zone tab formattedBody =
    main_ []
        ([ div [ class "request" ] (viewRequest isProViewVisible tab.editState tab.requestToEdit tab.history)
         , div [ class "response" ]
            (h2 [ class "mdc-typography--headline5" ] [ text "Response" ]
                :: viewResponse tab.responseState (History.getSelectedResponse tab.history) formattedBody
            )
         ]
            ++ (if isProViewVisible then
                    [ div [ class "history" ]
                        [ h2 [ class "mdc-typography--headline5" ] [ text "History" ]
                        , viewHistory zone tab.history
                        ]
                    ]

                else
                    []
               )
        )


viewRequest : Bool -> EditState -> CallRequest -> History.History -> List (Html TabMsg)
viewRequest isProViewVisible editState requestToEdit history =
    [ h2 [ class "mdc-typography--headline5" ]
        [ text
            ("Request"
                ++ (if editState == Changed then
                        "*"

                    else
                        ""
                   )
            )
        ]
    , MDC.select "method"
        "Method"
        [ ( "GET", "GET" )
        , ( "POST", "POST" )
        , ( "PUT", "PUT" )
        , ( "PATCH", "PATCH" )
        , ( "DELETE", "DELETE" )
        , ( "HEAD", "HEAD" )
        , ( "CONNECT", "CONNECT" )
        , ( "TRACE", "TRACE" )
        , ( "OPTIONS", "OPTIONS" )
        ]
        requestToEdit.request.method
        SetMethod
    , MDC.textFieldWithAttributes [ class "request-url" ] "url" "URL" requestToEdit.request.url SetUrl
    , div [ class "request-headers" ] (viewRequestHeaders requestToEdit.headers)
    , div [ class "request-body" ] [ MDC.textAreaField "body" "Body" requestToEdit.body SetBody ]
    , MDC.button "button-send"
        "Send"
        (PerformRequest requestToEdit)
    ]
        ++ (case editState of
                Overwritten oldRequest ->
                    [ MDC.snackBarWithSingleAction
                        "The content of the request has been overwritten with the selected request of the history."
                        "Undo"
                        (RevertRequest oldRequest)
                    ]

                _ ->
                    []
           )
        ++ (if isProViewVisible then
                []

            else
                [ div [ class "history-buttons" ]
                    [ MDC.iconButtonWithAttributes
                        [ disabled (History.hasOlderHttpCall history |> not)
                        ]
                        "chevron_left"
                        SelectOlderHistoryEntry
                    , MDC.iconButtonWithAttributes
                        [ disabled (History.hasNewerHttpCall history |> not)
                        ]
                        "chevron_right"
                        SelectNewerHistoryEntry
                    ]
                ]
           )


viewRequestHeader : Bool -> CallRequestHeader -> List (Html TabMsg)
viewRequestHeader isLastHeader header =
    [ MDC.textFieldWithoutLabel "Name" header.key (SetHeaderKey header.id)
    , MDC.textFieldWithoutLabel "Value" header.value (SetHeaderValue header.id)
    ]
        ++ (if isLastHeader then
                []

            else
                [ MDC.iconButton "remove" (RemoveHeader header.id)
                ]
           )


viewRequestHeaders : List CallRequestHeader -> List (Html TabMsg)
viewRequestHeaders headers =
    let
        lastIndex =
            List.length headers - 1
    in
    [ h3 [ class "mdc-typography--headline6" ] [ text "Headers" ]
    ]
        ++ (List.indexedMap (\index -> viewRequestHeader (index == lastIndex)) headers |> List.concatMap identity)


viewResponse : ResponseState -> Maybe CallResponse -> String -> List (Html TabMsg)
viewResponse responseState maybeResponse formattedBody =
    case ( responseState, maybeResponse ) of
        ( ShowSelectedResponse, Just { status, headers, body } ) ->
            [ p [ class "response__status" ] [ text (status.message ++ " (Code: " ++ String.fromInt status.code ++ ")") ]
            , div [ class "header-list" ]
                (List.concatMap
                    (\{ key, value } ->
                        [ div [ class "header-list__header-name" ] [ text key ]
                        , div [ class "header-list__header-value" ] [ text value ]
                        ]
                    )
                    headers
                )
            , p [ class "response__body" ] [ text formattedBody ]
            ]

        ( ShowSelectedResponse, Nothing ) ->
            [ p [ class "message" ] [ text "Please make a request first to see a response" ] ]

        ( Failure message, _ ) ->
            [ p [ class "message" ] [ text ("HTTP request failed with message: " ++ message) ] ]

        ( Loading, _ ) ->
            [ p [ class "message" ] [ text "Loading..." ] ]


viewHistory : Time.Zone -> History.History -> Html TabMsg
viewHistory zone history =
    if History.isEmpty history then
        p [ class "mdc-typography--body1" ] [ text "The history is empty. Make some requests!" ]

    else
        history
            |> History.getAllHttpCalls
            |> List.map
                (\call ->
                    { firstLine =
                        call.request.request.method
                            ++ " "
                            ++ call.request.request.url
                            ++ " ("
                            ++ call.response.status.message
                            ++ ")"
                    , secondLine =
                        Util.TimeFormat.toDateTimeString zone call.responseTime
                            ++ " (Duration: "
                            ++ String.fromInt (Time.posixToMillis call.responseTime - Time.posixToMillis call.requestTime)
                            ++ "ms)"
                    , selected = History.isSelected call history
                    , onClick = SelectHistoryEntry call
                    , secondaryAction = Nothing
                    }
                )
            |> MDC.selectableList
