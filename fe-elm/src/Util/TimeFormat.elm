module Util.TimeFormat exposing (toDateTimeString)

import Time exposing (..)


toDateTimeString : Zone -> Posix -> String
toDateTimeString zone time =
    let
        year =
            String.fromInt (toYear zone time)

        month =
            toMonthName (toMonth zone time)

        day =
            toTwoDigitString (toDay zone time)

        hour =
            toTwoDigitString (toHour zone time)

        minute =
            toTwoDigitString (toMinute zone time)

        second =
            toTwoDigitString (toSecond zone time)
    in
    day ++ ". " ++ month ++ " " ++ year ++ " " ++ hour ++ ":" ++ minute ++ ":" ++ second


toTwoDigitString : Int -> String
toTwoDigitString =
    String.fromInt
        >> (\time ->
                if String.length time == 1 then
                    "0" ++ time

                else
                    time
           )


toMonthName : Month -> String
toMonthName month =
    case month of
        Jan ->
            "January"

        Feb ->
            "February"

        Mar ->
            "March"

        Apr ->
            "April"

        May ->
            "May"

        Jun ->
            "June"

        Jul ->
            "July"

        Aug ->
            "August"

        Sep ->
            "September"

        Oct ->
            "October"

        Nov ->
            "November"

        Dec ->
            "December"
