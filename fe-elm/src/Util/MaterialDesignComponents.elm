module Util.MaterialDesignComponents exposing (SelectableListItem, appBar, button, buttonWithIcon, iconButton, iconButtonWithAttributes, select, selectableList, snackBarWithSingleAction, switch, textAreaField, textFieldWithAttributes, textFieldWithoutLabel)

import Html exposing (Attribute, Html, div, header, i, input, label, li, option, section, span, text, textarea, ul)
import Html.Attributes exposing (..)
import Html.Events as Event
import Json.Decode


type alias SelectableListItem msg =
    { firstLine : String
    , secondLine : String
    , selected : Bool
    , onClick : msg
    , secondaryAction : Maybe { icon : String, onClick : msg }
    }


autoInit : String -> Attribute msg
autoInit =
    attribute "data-mdc-auto-init"


button : String -> String -> msg -> Html msg
button buttonId displayValue msg =
    Html.button [ id buttonId, class "mdc-button", class "mdc-button--raised", autoInit "MDCRipple", Event.onClick msg ]
        [ span [ class "mdc-button__label" ] [ text displayValue ]
        ]


buttonWithIcon : String -> String -> msg -> Html msg
buttonWithIcon icon displayValue msg =
    Html.button [ class "mdc-button", class "mdc-button--unelevated", autoInit "MDCRipple", Event.onClick msg ]
        [ i [ class "material-icons", class "mdc-button__icon" ] [ text icon ]
        , span [ class "mdc-button__label" ] [ text displayValue ]
        ]


iconButton : String -> msg -> Html msg
iconButton =
    iconButtonWithAttributes []


iconButtonWithAttributes : List (Attribute msg) -> String -> msg -> Html msg
iconButtonWithAttributes attributes iconName msg =
    Html.button ([ class "mdc-icon-button", class "material-icons", Event.onClick msg ] ++ attributes)
        [ text iconName ]


textFieldWithAttributes : List (Attribute msg) -> String -> String -> String -> (String -> msg) -> Html msg
textFieldWithAttributes attributes name displayName inputValue msg =
    div ([ class "mdc-text-field", class "mdc-text-field--outlined", autoInit "MDCTextField" ] ++ attributes)
        [ input [ id name, class "mdc-text-field__input", value inputValue, Event.onInput msg ] []
        , div [ class "mdc-notched-outline" ]
            [ div [ class "mdc-notched-outline__leading" ] []
            , div [ class "mdc-notched-outline__notch" ]
                [ label [ class "mdc-floating-label", for name ] [ text displayName ]
                ]
            , div [ class "mdc-notched-outline__trailing" ] []
            ]
        ]


textFieldWithoutLabel : String -> String -> (String -> msg) -> Html msg
textFieldWithoutLabel displayName inputValue msg =
    div [ class "mdc-text-field", class "mdc-text-field--outlined", class "mdc-text-field--no-label", autoInit "MDCTextField" ]
        [ input
            [ class "mdc-text-field__input"
            , value inputValue
            , Event.onInput msg
            , attribute "aria-label" displayName
            , placeholder displayName
            ]
            []
        , div [ class "mdc-notched-outline" ]
            [ div [ class "mdc-notched-outline__leading" ] []
            , div [ class "mdc-notched-outline__trailing" ] []
            ]
        ]


textAreaField : String -> String -> String -> (String -> msg) -> Html msg
textAreaField name displayName inputValue msg =
    div [ class "mdc-text-field", class "mdc-text-field--textarea", class "mdc-text-field--fullwidth", autoInit "MDCTextField" ]
        [ textarea [ id name, class "mdc-text-field__input", value inputValue, Event.onInput msg ] []
        , div [ class "mdc-notched-outline" ]
            [ div [ class "mdc-notched-outline__leading" ] []
            , div [ class "mdc-notched-outline__notch" ]
                [ label [ class "mdc-floating-label", for name ] [ text displayName ]
                ]
            , div [ class "mdc-notched-outline__trailing" ] []
            ]
        ]


select : String -> String -> List ( String, String ) -> String -> (String -> msg) -> Html msg
select name displayName options inputValue msg =
    div [ class "mdc-select", class "mdc-select--outlined", autoInit "MDCSelect" ]
        [ i [ class "mdc-select__dropdown-icon" ] []
        , Html.select
            [ id name
            , class "mdc-select__native-control"
            , value inputValue
            , Event.onInput msg
            ]
            (List.map
                (\( optionValue, optionDisplay ) ->
                    option [ value optionValue ] [ text optionDisplay ]
                )
                options
            )
        , div [ class "mdc-notched-outline" ]
            [ div [ class "mdc-notched-outline__leading" ] []
            , div [ class "mdc-notched-outline__notch" ]
                [ label [ class "mdc-floating-label", for name ] [ text displayName ]
                ]
            , div [ class "mdc-notched-outline__trailing" ] []
            ]
        ]


appBar : String -> List (Html msg) -> Html msg
appBar title actions =
    header [ class "mdc-top-app-bar" ]
        [ div [ class "mdc-top-app-bar__row" ]
            [ section [ class "mdc-top-app-bar__section", class "mdc-top-app-bar__section--align-start" ]
                [ Html.button [ class "sidebar-toggle", class "material-icons", class "mdc-top-app-bar__navigation-icon", class "mdc-icon-button" ] [ text "menu" ]
                , span [ class "mdc-top-app-bar__title" ] [ text title ]
                ]
            , section [ class "mdc-top-app-bar__section", class "mdc-top-app-bar__section--align-end" ]
                actions
            ]
        ]


selectableList : List (SelectableListItem msg) -> Html msg
selectableList =
    List.map selectableListItem >> ul [ class "mdc-list", class "mdc-list--two-line", attribute "role" "listbox", autoInit "MDCList" ]


selectableListItem : SelectableListItem msg -> Html msg
selectableListItem { firstLine, secondLine, selected, onClick, secondaryAction } =
    li
        [ class "mdc-list-item"
        , classList [ ( "mdc-list-item--selected", selected ) ]
        , tabindex 0
        , attribute "role" "option"
        , attribute "aria-selected"
            (if selected then
                "true"

             else
                "false"
            )
        , Event.onClick onClick
        ]
        ([ span [ class "mdc-list-item__text" ]
            [ span [ class "mdc-list-item__primary-text" ] [ text firstLine ]
            , span [ class "mdc-list-item__secondary-text" ] [ text secondLine ]
            ]
         ]
            ++ (case secondaryAction of
                    Just action ->
                        [ span [ class "mdc-list-item__meta" ]
                            [ Html.button
                                [ class "mdc-icon-button"
                                , class "material-icons"
                                , Event.stopPropagationOn "click" (Json.Decode.succeed ( action.onClick, True ))
                                ]
                                [ text action.icon ]
                            ]
                        ]

                    Nothing ->
                        []
               )
        )


snackBarWithSingleAction : String -> String -> msg -> Html msg
snackBarWithSingleAction message actionText action =
    div [ class "mdc-snackbar", class "mdc-snackbar--open" ]
        [ div [ class "mdc-snackbar__surface" ]
            [ div [ class "mdc-snackbar__label" ]
                [ text message
                ]
            , div [ class "mdc-snackbar__actions" ]
                [ Html.button [ class "mdc-button mdc-snackbar__action", Event.onClick action ] [ text actionText ]
                ]
            ]
        ]


switch : String -> String -> Bool -> (Bool -> msg) -> List (Html msg)
switch name displayName isChecked msg =
    [ div
        [ class "mdc-switch"
        , classList [ ( "mdc-switch--checked", isChecked ) ]
        , autoInit "MDCSwitch"
        ]
        [ div [ class "mdc-switch__track" ] []
        , div [ class "mdc-switch__thumb-underlay" ]
            [ div [ class "mdc-switch__thumb" ]
                [ input
                    [ id name
                    , type_ "checkbox"
                    , class "mdc-switch__native-control"
                    , attribute "role" "switch"
                    , checked isChecked
                    , Event.onCheck msg
                    ]
                    []
                ]
            ]
        ]
    , label [ for name ] [ text displayName ]
    ]
