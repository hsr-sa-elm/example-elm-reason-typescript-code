module Util.Id exposing (getNextId)


convertIdStringToInt : String -> String -> Maybe Int
convertIdStringToInt prefix str =
    String.toInt (String.dropLeft (String.length prefix) str)


getNextId : String -> (a -> String) -> List a -> String
getNextId prefix getId elements =
    let
        maxId =
            List.maximum (List.filterMap (getId >> convertIdStringToInt prefix) elements)

        nextId =
            case maxId of
                Just n ->
                    n + 1

                Nothing ->
                    1
    in
    prefix ++ String.fromInt nextId
