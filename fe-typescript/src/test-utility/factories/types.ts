import {
  HttpCallRequest,
  HttpCallResponse,
  HttpRequestHeader,
  HttpResponseHeader,
  PerformedHttpCall,
} from '../../types/http-call';

export const createHttpCallRequest = ({
  request: { method = 'GET', url = '' } = {},
  headers = [
    { id: 'header-0', key: 'Content-Type', value: 'application/json' },
    { id: 'header-1', key: 'Cache-Control', value: 'no-cache' },
    { id: 'header-2', key: '', value: '' },
  ] as readonly HttpRequestHeader[],
  body = '',
} = {}): HttpCallRequest => ({
  request: {
    method,
    url,
  },
  headers,
  body,
});

export const createHttpCallResponse = ({
  status: { code = 200, message = 'OK' } = {},
  headers = [] as readonly HttpResponseHeader[],
  body = '',
} = {}): HttpCallResponse => ({
  status: {
    code,
    message,
  },
  headers,
  body,
});

export const createPerformedHttpCall = ({
  id = 'undefined-id',
  request = createHttpCallRequest(),
  response = createHttpCallResponse(),
  requestTime = '2019-10-29T17:26:42.374Z',
  responseTime = '2019-10-29T17:26:44.123Z',
} = {}): PerformedHttpCall => ({
  id,
  request,
  response,
  requestTime,
  responseTime,
});
