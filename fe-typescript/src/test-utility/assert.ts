const createAssert = (expectation: (actual: any, expected: any) => void) => <
  T
>({
  given,
  should,
  actual,
  expected,
}: {
  readonly given: string;
  readonly should: string;
  readonly actual: () => T;
  readonly expected: T;
}) =>
  test(`Given ${given}: should ${should}`, () =>
    expectation(actual(), expected));

export const assert = createAssert((actual, expected) =>
  expect(actual).toEqual(expected)
);

export const assertMatch = createAssert((actual, expected) =>
  expect(actual).toMatchObject(expected)
);

export const assertStateWithSelectors = <T>({
  given,
  withState,
  expectations,
}: {
  readonly given: string;
  readonly withState: () => T;
  readonly expectations: (
    builder: <R>(
      should: string,
      selector: (state: T) => R,
      expected: R
    ) => [string, (state: T) => any, any]
  ) => [string, (state: T) => any, any][];
}) =>
  expectations((should, selector, expected) => [
    should,
    selector,
    expected,
  ]).forEach(([should, selector, expected]) =>
    assert({
      given,
      should,
      actual: () => selector(withState()),
      expected,
    })
  );
