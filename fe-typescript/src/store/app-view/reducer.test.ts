import { reduce } from 'ramda';
import { getIsProViewVisible } from './selector';
import { rootReducer } from '../index';
import { assert } from '../../test-utility/assert';
import { toggleProViewVisibility } from './action';

const reduceActions = reduce(rootReducer, rootReducer(undefined, {} as any));

describe('getIsProViewVisible()', () => {
  assert({
    given: 'initial state',
    should: 'be in pro view',
    actual: () => getIsProViewVisible(reduceActions([])),
    expected: true,
  });

  assert({
    given: 'toggled once',
    should: 'be in simple view',
    actual: () =>
      getIsProViewVisible(reduceActions([toggleProViewVisibility()])),
    expected: false,
  });

  assert({
    given: 'toggled twice',
    should: 'be in pro view',
    actual: () =>
      getIsProViewVisible(
        reduceActions([toggleProViewVisibility(), toggleProViewVisibility()])
      ),
    expected: true,
  });
});
