import { createReducer } from 'deox';
import { toggleProViewVisibility } from './action';

export type AppViewState = {
  readonly isProViewVisible: boolean;
};

export const initialState: AppViewState = {
  isProViewVisible: true,
};

export const appViewReducer = createReducer(initialState, handleAction => [
  handleAction(toggleProViewVisibility, state => ({
    ...state,
    isProViewVisible: !state.isProViewVisible,
  })),
]);
