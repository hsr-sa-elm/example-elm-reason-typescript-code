import { pipe } from 'ramda';
import { RootState } from '../index';

const getAppViewState = ({ appView }: RootState) => appView;

export const getIsProViewVisible = pipe(
  getAppViewState,
  ({ isProViewVisible }) => isProViewVisible
);
