import { createActionCreator } from 'deox';

export const toggleProViewVisibility = createActionCreator(
  'appView/TOGGLE_PRO_VIEW'
);
