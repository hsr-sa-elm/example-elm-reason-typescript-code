import { createActionCreator } from 'deox';

export const addTab = createActionCreator('tab/ADD_TAB');

export const removeTab = createActionCreator(
  'tab/REMOVE_TAB',
  resolve => (id: string) => resolve({ id })
);

export const activateTab = createActionCreator(
  'tab/ACTIVATE_TAB',
  resolve => (id: string) => resolve({ id })
);
