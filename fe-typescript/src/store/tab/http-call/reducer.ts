import { createReducer } from 'deox';
import { doHttpCall, httpCallFailed, httpCallSucceeded } from './action';

export type HttpCallState = {
  readonly isLoading: boolean;
  readonly error?: string;
};

export const initialState: HttpCallState = {
  isLoading: false,
};

export const httpCallReducer = createReducer(initialState, handleAction => [
  handleAction(doHttpCall, state => ({
    ...state,
    isLoading: true,
    error: undefined,
  })),
  handleAction(httpCallSucceeded, state => ({
    ...state,
    isLoading: false,
    error: undefined,
  })),
  handleAction(httpCallFailed, (state, { payload }) => ({
    ...state,
    isLoading: false,
    error: payload.error,
  })),
]);
