import { createActionCreator } from 'deox';
import { HttpCallRequest, HttpCallResponse } from '../../../types/http-call';

export const doHttpCall = createActionCreator(
  'httpCall/DO_CALL',
  resolve => (tabId: string, request: HttpCallRequest) =>
    resolve({ tabId, request })
);

export const httpCallSucceeded = createActionCreator(
  'httpCall/CALL_SUCCEEDED',
  resolve => (
    tabId: string,
    request: HttpCallRequest,
    response: HttpCallResponse,
    requestTime: string,
    responseTime: string
  ) => resolve({ tabId, request, response, requestTime, responseTime })
);

export const httpCallFailed = createActionCreator(
  'httpCall/CALL_FAILED',
  resolve => (tabId: string, error: string) => resolve({ tabId, error })
);
