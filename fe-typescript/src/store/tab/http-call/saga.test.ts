import { doHttpCall, httpCallFailed, httpCallSucceeded } from './action';
import { httpCallSaga } from './saga';
import { expectSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga-test-plan/matchers';
import { dynamic, throwError } from 'redux-saga-test-plan/providers';
import {
  createHttpCallRequest,
  createHttpCallResponse,
} from '../../../test-utility/factories/types';
import { getCurrentISOTimeStamp } from '../../../functions/utility/utility';

describe('httpCallSaga()', () => {
  const tabId = 'some-tab-id';

  const httpCallRequest = createHttpCallRequest({
    request: {
      method: 'GET',
      url: 'something',
    },
    body: 'some body',
    headers: [
      {
        id: 'header-1',
        key: 'first_key',
        value: 'first_value',
      },
      {
        id: 'header-2',
        key: '2 first_key',
        value: '2 first_value',
      },
      {
        id: 'header-3',
        key: '',
        value: 'empty header (should not be sent)',
      },
    ],
  });

  const response = createHttpCallResponse({
    status: { code: 400, message: 'Bad Request' },
  });

  const requestBody = {
    method: 'POST',
    body: JSON.stringify({
      request: httpCallRequest.request,
      body: httpCallRequest.body,
      headers: [
        { key: 'first_key', value: 'first_value' },
        { key: '2 first_key', value: '2 first_value' },
      ],
    }),
  };

  const requestTime = '2019-10-29T17:26:42.374Z';
  const responseTime = '2019-10-29T17:26:44.123Z';

  function* getTime() {
    yield requestTime;
    yield responseTime;
  }
  const getTimeGenerator = getTime();

  test('success', () =>
    expectSaga(httpCallSaga)
      .provide([
        [
          call(fetch, process.env.REACT_APP_backendUrl, requestBody),
          { json: () => Promise.resolve(response) },
        ],
        [
          call(getCurrentISOTimeStamp),
          dynamic(() => getTimeGenerator.next().value),
        ],
      ])
      .put(
        httpCallSucceeded(
          tabId,
          httpCallRequest,
          response,
          requestTime,
          responseTime
        )
      )
      .dispatch(doHttpCall(tabId, httpCallRequest))
      .silentRun());

  test('http call fail', () =>
    expectSaga(httpCallSaga)
      .provide([
        [
          call(fetch, process.env.REACT_APP_backendUrl, requestBody),
          throwError(new Error('some error')),
        ],
      ])
      .put(httpCallFailed(tabId, 'some error'))
      .dispatch(doHttpCall(tabId, httpCallRequest))
      .silentRun());

  test('json transformation fail', () =>
    expectSaga(httpCallSaga)
      .provide([
        [
          call(fetch, process.env.REACT_APP_backendUrl, requestBody),
          { json: () => Promise.reject('json error') },
        ],
      ])
      .put(httpCallFailed(tabId, 'json error'))
      .dispatch(doHttpCall(tabId, httpCallRequest))
      .silentRun());

  test('API fail', () =>
    expectSaga(httpCallSaga)
      .provide([
        [
          call(fetch, process.env.REACT_APP_backendUrl, requestBody),
          { json: () => Promise.resolve({ message: 'API error' }) },
        ],
      ])
      .put(httpCallFailed(tabId, 'API error'))
      .dispatch(doHttpCall(tabId, httpCallRequest))
      .silentRun());
});
