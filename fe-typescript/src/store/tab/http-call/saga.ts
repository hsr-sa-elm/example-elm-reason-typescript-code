import { call, put, takeLatest } from 'redux-saga/effects';
import { ActionType, getType } from 'deox';
import { doHttpCall, httpCallFailed, httpCallSucceeded } from './action';
import {
  getCurrentISOTimeStamp,
  getErrorMessage,
} from '../../../functions/utility/utility';
import { HttpCallRequest, HttpRequestHeader } from '../../../types/http-call';
import { isEmpty, map, omit, reject } from 'ramda';

const prepareRequestForSending = ({ headers, ...rest }: HttpCallRequest) => ({
  ...rest,
  headers: map(
    omit(['id']),
    reject(({ key }) => isEmpty(key), headers as HttpRequestHeader[])
  ),
});

function* doHttpCallSaga({ payload }: ActionType<typeof doHttpCall>) {
  try {
    const requestTime = yield call(getCurrentISOTimeStamp);
    const response = yield call(
      fetch,
      process.env.REACT_APP_backendUrl as string,
      {
        method: 'POST',
        body: JSON.stringify(prepareRequestForSending(payload.request)),
      }
    );
    const responseBody = yield response.json();
    if (
      typeof responseBody === 'object' &&
      typeof responseBody.message === 'string'
    ) {
      yield put(httpCallFailed(payload.tabId, responseBody.message));
    } else {
      const responseTime = yield call(getCurrentISOTimeStamp);
      yield put(
        httpCallSucceeded(
          payload.tabId,
          payload.request,
          responseBody,
          requestTime,
          responseTime
        )
      );
    }
  } catch (e) {
    yield put(httpCallFailed(payload.tabId, getErrorMessage(e)));
  }
}

export function* httpCallSaga() {
  yield takeLatest(getType(doHttpCall), doHttpCallSaga);
}
