import { assert } from '../../../test-utility/assert';
import { doHttpCall, httpCallFailed, httpCallSucceeded } from './action';
import {
  createHttpCallRequest,
  createHttpCallResponse,
} from '../../../test-utility/factories/types';
import { reduce } from 'ramda';
import { getError, getIsLoading } from './selector';
import { getSelectedTabId } from '../selector';
import { rootReducer } from '../../index';

const request = createHttpCallRequest();
const response = createHttpCallResponse();
const requestTime = '2019-10-29T17:26:42.374Z';
const responseTime = '2019-10-29T17:26:44.123Z';

const reduceActions = reduce(rootReducer, rootReducer(undefined, {} as any));
const tabId = getSelectedTabId(reduceActions([]));

describe('getIsLoading()', () => {
  assert({
    given: 'initial state',
    should: 'not be loading',
    actual: () => getIsLoading(reduceActions([])),
    expected: false,
  });

  assert({
    given: 'a call is made',
    should: 'be loading',
    actual: () =>
      getIsLoading(reduceActions([doHttpCall(tabId, createHttpCallRequest())])),
    expected: true,
  });

  assert({
    given: 'a call is made and succeeds',
    should: 'not be loading',
    actual: () =>
      getIsLoading(
        reduceActions([
          doHttpCall(tabId, request),
          httpCallSucceeded(
            tabId,
            request,
            response,
            requestTime,
            responseTime
          ),
        ])
      ),
    expected: false,
  });

  assert({
    given: 'a call is made and fails',
    should: 'not be loading',
    actual: () =>
      getIsLoading(
        reduceActions([
          doHttpCall(tabId, request),
          httpCallFailed(tabId, 'some error'),
        ])
      ),
    expected: false,
  });
});

describe('getError()', () => {
  assert({
    given: 'initial state',
    should: 'have no error',
    actual: () => getError(reduceActions([])),
    expected: undefined,
  });

  assert({
    given: 'a call is made and succeeds',
    should: 'have no error',
    actual: () =>
      getError(
        reduceActions([
          doHttpCall(tabId, request),
          httpCallSucceeded(
            tabId,
            request,
            response,
            requestTime,
            responseTime
          ),
        ])
      ),
    expected: undefined,
  });

  assert({
    given: 'a call is made and fails',
    should: 'have an error',
    actual: () =>
      getError(
        reduceActions([
          doHttpCall(tabId, request),
          httpCallFailed(tabId, 'some error'),
        ])
      ),
    expected: 'some error',
  });

  assert({
    given: 'a call failed and a new call is made',
    should: 'clear the error',
    actual: () =>
      getError(
        reduceActions([
          doHttpCall(tabId, request),
          httpCallFailed(tabId, 'some error'),
          doHttpCall(tabId, request),
        ])
      ),
    expected: undefined,
  });
});
