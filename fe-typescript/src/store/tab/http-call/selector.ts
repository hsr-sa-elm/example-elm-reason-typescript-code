import { pipe, defaultTo } from 'ramda';
import { getCurrentTab } from '../selector';
import { mapIfNotNil } from '../../../functions/utility/utility';

const getHttpCallState = pipe(
  getCurrentTab,
  mapIfNotNil(({ httpCall }) => httpCall)
);

export const getIsLoading = pipe(
  getHttpCallState,
  mapIfNotNil(({ isLoading }) => isLoading),
  defaultTo(false)
);

export const getError = pipe(
  getHttpCallState,
  mapIfNotNil(({ error }) => error)
);
