import { assert } from '../../test-utility/assert';
import { activateTab, addTab, removeTab } from './action';
import { reduce } from 'ramda';
import { getAllTabs, getSelectedTabId } from './selector';
import { getRequestToEdit } from './request-view/selector';
import { setUrl } from './request-view/action';
import { rootReducer } from '../index';
import { createHttpCallRequest } from '../../test-utility/factories/types';

const reduceActions = reduce(rootReducer, rootReducer(undefined, {} as any));
const tabId = getSelectedTabId(reduceActions([]));

describe('addTab()', () => {
  assert({
    given: 'default values (one tab)',
    should: 'contain two tabs',
    actual: () => getAllTabs(reduceActions([addTab])).length,
    expected: 2,
  });

  const aNewId = getSelectedTabId(reduceActions([addTab]));

  assert({
    given: 'default values',
    should: 'create new unique id',
    actual: () =>
      getAllTabs(reduceActions([])).filter(tab => tab.id === aNewId).length,
    expected: 0,
  });
});

describe('removeTab()', () => {
  const aNewId = getSelectedTabId(reduceActions([addTab]));

  assert({
    given: 'added and removed state',
    should: 'contain two tabs',
    actual: () => getAllTabs(reduceActions([addTab, removeTab(aNewId)])),
    expected: [
      {
        id: tabId,
        method: 'GET',
        url: '',
        isActive: true,
      },
    ],
  });

  assert({
    given: 'remove non existing id',
    should: 'do nothing',
    actual: () =>
      getAllTabs(reduceActions([addTab, removeTab('not existing ID6')])),
    expected: [
      {
        id: tabId,
        method: 'GET',
        url: '',
        isActive: false,
      },
      {
        id: aNewId,
        method: 'GET',
        url: '',
        isActive: true,
      },
    ],
  });

  assert({
    given: 'remove last tab',
    should: 'return an empty list',
    actual: () => getAllTabs(reduceActions([removeTab(tabId)])),
    expected: [],
  });
});

describe('activateTab()', () => {
  assert({
    given: 'an id to select',
    should: 'select the id',
    actual: () => getSelectedTabId(reduceActions([activateTab('new-id')])),
    expected: 'new-id',
  });
});

describe('tabManager()', () => {
  assert({
    given: 'an action for a tab',
    should: 'apply the action to the current tab',
    actual: () =>
      getRequestToEdit(reduceActions([setUrl(tabId, 'test-new-url')])),
    expected: createHttpCallRequest({ request: { url: 'test-new-url' } }),
  });
});
