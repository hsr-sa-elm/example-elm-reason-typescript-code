import { Action, combineReducers } from 'redux';
import { httpCallReducer, HttpCallState } from './http-call/reducer';
import {
  requestViewReducer,
  RequestViewStateForSchema,
} from './request-view/reducer';
import { historyReducer, HistoryState } from './history/reducer';
import { createReducer } from 'deox';
import { activateTab, addTab, removeTab } from './action';
import { getNextId } from '../../functions/utility/utility';
import { head } from 'ramda';
import { DeepWithoutUnionTypes } from '@healthinal/typescript-schema-transformer';

export type TabStateForSchema = {
  readonly id: string;
  readonly tabData: {
    readonly history: HistoryState;
    readonly httpCall: HttpCallState;
    readonly requestView: RequestViewStateForSchema;
  };
};

export type TabsStateForSchema = {
  readonly tabs: TabStateForSchema[];
  readonly selectedTab: string;
};

export type TabState = DeepWithoutUnionTypes<TabStateForSchema>;
export type TabsState = DeepWithoutUnionTypes<TabsStateForSchema>;

const getId = getNextId('tab-', ({ id }: TabState) => id);

const tabDataReducer = combineReducers({
  history: historyReducer,
  httpCall: httpCallReducer,
  requestView: requestViewReducer,
});

const initialTabData = tabDataReducer(undefined, {} as any);

export const initialState: TabsState = {
  selectedTab: getId([]),
  tabs: [
    {
      id: getId([]),
      tabData: initialTabData,
    },
  ],
};

const tabsReducer = createReducer(initialState, handleAction => [
  handleAction(addTab, state => {
    const newId = getId(state.tabs);
    return {
      ...state,
      selectedTab: newId,
      tabs: [
        ...state.tabs,
        {
          id: newId,
          tabData: initialTabData,
        },
      ],
    };
  }),
  handleAction(activateTab, (state, { payload }) => ({
    ...state,
    selectedTab: payload.id,
  })),
  handleAction(removeTab, (state, { payload }) => {
    const newTabs = state.tabs.filter((tab: TabState) => tab.id !== payload.id);
    return {
      ...state,
      selectedTab:
        state.selectedTab === payload.id
          ? (head(newTabs) || { id: '' }).id
          : state.selectedTab,
      tabs: newTabs,
    };
  }),
]);

export const tabReducer = (
  state: TabsState = initialState,
  action: Action
): TabsState =>
  tabsReducer(
    {
      ...state,
      tabs: state.tabs.map(({ id, tabData }) => ({
        id,
        tabData:
          typeof action === 'object' &&
          typeof (action as any).payload === 'object' &&
          (action as any).payload.tabId === id
            ? tabDataReducer(tabData as any, action)
            : tabData,
      })),
    },
    action
  );
