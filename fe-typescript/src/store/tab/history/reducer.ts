import { createReducer } from 'deox';
import { PerformedHttpCall } from '../../../types/http-call';
import {
  selectHistoryEntry,
  selectNextNewerHistoryEntry,
  selectNextOlderHistoryEntry,
} from './action';
import { httpCallSucceeded } from '../http-call/action';
import { getNextId } from '../../../functions/utility/utility';
import { reduce } from 'ramda';

export type HistoryState = {
  readonly selectedId?: string;
  readonly httpCalls: readonly PerformedHttpCall[];
};

export const initialState: HistoryState = {
  httpCalls: [],
};

const getId = getNextId('history-entry-', ({ id }: PerformedHttpCall) => id);

export const historyReducer = createReducer(initialState, handleAction => [
  handleAction(selectHistoryEntry, (state, { payload }) => ({
    ...state,
    selectedId: payload.httpCallId,
  })),
  handleAction(httpCallSucceeded, (state, { payload }) => {
    let id = getId(state.httpCalls);
    return {
      ...state,
      selectedId: id,
      httpCalls: [
        {
          id,
          request: payload.request,
          response: payload.response,
          requestTime: payload.requestTime,
          responseTime: payload.responseTime,
        },
        ...state.httpCalls,
      ],
    };
  }),
  handleAction(selectNextNewerHistoryEntry, state => ({
    ...state,
    selectedId: reduce(
      (acc, { id }) =>
        id === state.selectedId
          ? {
              ...acc,
              foundSelected: true,
            }
          : acc.foundSelected
          ? acc
          : {
              ...acc,
              newerId: id,
            },
      { newerId: '', foundSelected: false },
      state.httpCalls as PerformedHttpCall[]
    ).newerId,
  })),
  handleAction(selectNextOlderHistoryEntry, state => ({
    ...state,
    selectedId: reduce(
      (acc, { id }) =>
        id === state.selectedId
          ? {
              ...acc,
              lastElementIsCurrentlySelected: true,
            }
          : acc.lastElementIsCurrentlySelected
          ? {
              lastElementIsCurrentlySelected: false,
              olderId: id,
            }
          : acc,
      { olderId: '', lastElementIsCurrentlySelected: false },
      state.httpCalls as PerformedHttpCall[]
    ).olderId,
  })),
]);
