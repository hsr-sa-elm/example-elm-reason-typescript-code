import {
  any,
  defaultTo,
  find,
  head,
  isEmpty,
  last,
  map,
  pipe,
  propEq,
} from 'ramda';
import { createSelector } from 'reselect';
import {
  PerformedHttpCall,
  PerformedHttpCallViewModel,
} from '../../../types/http-call';
import { getCurrentTab } from '../selector';
import { mapIfNotNil } from '../../../functions/utility/utility';
import { getFormattedBody } from '../../../functions/utility/format';

const getHistoryState = pipe(
  getCurrentTab,
  mapIfNotNil(({ history }) => history)
);

const getCalls = pipe(
  getHistoryState,
  mapIfNotNil(({ httpCalls }) => httpCalls),
  defaultTo([])
);

const getSelectedId = pipe(
  getHistoryState,
  mapIfNotNil(({ selectedId }) => selectedId)
);

const getIsSelectedSelector = createSelector(
  getSelectedId,
  selectedId => (id: string) => selectedId === id
);

export const getIsHistoryEmpty = pipe(
  getCalls,
  isEmpty
);

const getSelectedCall = createSelector(
  getCalls,
  getSelectedId,
  (httpCalls, selectedId) =>
    find(propEq('id', selectedId), httpCalls as PerformedHttpCall[])
);

export const getSelectedRequest = pipe(
  getSelectedCall,
  mapIfNotNil(({ request }) => request)
);

export const getSelectedResponse = pipe(
  getSelectedCall,
  mapIfNotNil(({ response }) => response)
);

export const getSelectedResponseBodyFormatted = createSelector(
  getSelectedResponse,
  response => (response ? getFormattedBody(response) : '')
);

export const getAllHttpCalls = createSelector(
  getCalls,
  getIsSelectedSelector,
  (calls, getIsSelected) =>
    map(
      (call: PerformedHttpCall): PerformedHttpCallViewModel => ({
        ...call,
        isSelected: getIsSelected(call.id),
      }),
      calls
    ) as PerformedHttpCallViewModel[]
);

export const getHasNewerHistoryEntries = createSelector(
  getCalls,
  getSelectedId,
  (httpCalls, selectedId) =>
    any(propEq('id', selectedId), httpCalls as PerformedHttpCall[]) &&
    head(httpCalls as PerformedHttpCall[]).id !== selectedId
);

export const getHasOlderHistoryEntries = createSelector(
  getCalls,
  getSelectedId,
  (httpCalls, selectedId) =>
    any(propEq('id', selectedId), httpCalls as PerformedHttpCall[]) &&
    last(httpCalls as PerformedHttpCall[]).id !== selectedId
);
