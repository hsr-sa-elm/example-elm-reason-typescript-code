import { createActionCreator } from 'deox';
import { HttpCallRequest } from '../../../types/http-call';

export const selectHistoryEntry = createActionCreator(
  'history/SELECT_ENTRY',
  resolve => (tabId: string, httpCallId: string, request: HttpCallRequest) =>
    resolve({ tabId, httpCallId, request })
);

export const selectNextNewerHistoryEntry = createActionCreator(
  'history/SELECT_NEWER',
  resolve => (tabId: string) => resolve({ tabId })
);

export const selectNextOlderHistoryEntry = createActionCreator(
  'history/SELECT_OLDER',
  resolve => (tabId: string) => resolve({ tabId })
);
