import { reduce } from 'ramda';
import { assert, assertStateWithSelectors } from '../../../test-utility/assert';
import {
  getAllHttpCalls,
  getHasNewerHistoryEntries,
  getHasOlderHistoryEntries,
  getIsHistoryEmpty,
  getSelectedResponse,
} from './selector';
import {
  createHttpCallRequest,
  createHttpCallResponse,
  createPerformedHttpCall,
} from '../../../test-utility/factories/types';
import { httpCallSucceeded } from '../http-call/action';
import {
  selectHistoryEntry,
  selectNextNewerHistoryEntry,
  selectNextOlderHistoryEntry,
} from './action';
import { rootReducer } from '../../index';
import { getSelectedTabId } from '../selector';

const reduceActions = reduce(rootReducer, rootReducer(undefined, {} as any));
const tabId = getSelectedTabId(reduceActions([]));

const request1 = createHttpCallRequest();
const request2 = createHttpCallRequest({
  request: {
    method: 'GET',
    url: 'http://test.com/',
  },
});
const response1 = createHttpCallResponse({
  status: {
    code: 200,
    message: 'OK',
  },
});
const response2 = createHttpCallResponse({
  body: 'other body',
});
const response3 = createHttpCallResponse({
  status: {
    code: 400,
    message: 'Bad Request',
  },
});

const call1 = createPerformedHttpCall({
  id: 'history-entry-1',
  request: request1,
  response: response1,
});
const call2 = createPerformedHttpCall({
  id: 'history-entry-2',
  request: request2,
  response: response2,
});
const call3 = createPerformedHttpCall({
  id: 'history-entry-3',
  request: request1,
  response: response3,
});
const callAction1 = httpCallSucceeded(
  tabId,
  call1.request,
  call1.response,
  call1.requestTime,
  call1.responseTime
);
const callAction2 = httpCallSucceeded(
  tabId,
  call2.request,
  call2.response,
  call2.requestTime,
  call2.responseTime
);
const callAction3 = httpCallSucceeded(
  tabId,
  call3.request,
  call3.response,
  call3.requestTime,
  call3.responseTime
);

describe('getIsHistoryEmpty()', () => {
  assert({
    given: 'initial state',
    should: 'be empty',
    actual: () => getIsHistoryEmpty(reduceActions([])),
    expected: true,
  });

  assert({
    given: 'a call is added to the history',
    should: 'not be empty',
    actual: () => getIsHistoryEmpty(reduceActions([callAction1])),
    expected: false,
  });
});

describe('getSelectedResponse', () => {
  assert({
    given: 'initial state',
    should: 'return none',
    actual: () => getSelectedResponse(reduceActions([])),
    expected: undefined,
  });

  assert({
    given: 'a call is added to the history',
    should: 'select the response',
    actual: () => getSelectedResponse(reduceActions([callAction1])),
    expected: response1,
  });

  assert({
    given: 'multiple calls are added to the history',
    should: 'select the last response',
    actual: () =>
      getSelectedResponse(reduceActions([callAction1, callAction2])),
    expected: response2,
  });

  assert({
    given: 'multiple calls are added to the history and an old one is selected',
    should: 'return the last selected response',
    actual: () =>
      getSelectedResponse(
        reduceActions([
          callAction1,
          callAction2,
          callAction3,
          selectHistoryEntry(tabId, call2.id, call2.request),
          selectHistoryEntry(tabId, call1.id, call1.request),
        ])
      ),
    expected: response1,
  });
});

describe('getAllHttpCallsForView', () => {
  assert({
    given: 'a call is added to the history',
    should: 'add it to the list',
    actual: () => getAllHttpCalls(reduceActions([callAction1])),
    expected: [{ ...call1, isSelected: true }],
  });

  assert({
    given: 'multiple calls are added to the history',
    should: 'should push new calls to the front',
    actual: () =>
      getAllHttpCalls(reduceActions([callAction1, callAction2, callAction3])),
    expected: [
      { ...call3, isSelected: true },
      { ...call2, isSelected: false },
      { ...call1, isSelected: false },
    ],
  });

  assert({
    given: 'multiple calls are added to the history and an old one is selected',
    should: 'keep order but mark the selected one',
    actual: () =>
      getAllHttpCalls(
        reduceActions([
          callAction1,
          callAction2,
          selectHistoryEntry(tabId, call2.id, call2.request),
          callAction3,
          selectHistoryEntry(tabId, call1.id, call1.request),
        ])
      ),
    expected: [
      { ...call3, isSelected: false },
      { ...call2, isSelected: false },
      { ...call1, isSelected: true },
    ],
  });
});

describe('older and newer entries', () => {
  assertStateWithSelectors({
    given: 'initial state',
    withState: () => reduceActions([]),
    expectations: should => [
      should('not have newer entries', getHasNewerHistoryEntries, false),
      should('not have older entries', getHasOlderHistoryEntries, false),
      should('have zero entries', getAllHttpCalls, []),
    ],
  });

  assertStateWithSelectors({
    given: 'three calls are added to the history',
    withState: () => reduceActions([callAction1, callAction2, callAction3]),
    expectations: should => [
      should('not have newer entries', getHasNewerHistoryEntries, false),
      should('have older entries', getHasOlderHistoryEntries, true),
      should('newest should be selected', getAllHttpCalls, [
        { ...call3, isSelected: true },
        { ...call2, isSelected: false },
        { ...call1, isSelected: false },
      ]),
    ],
  });

  assertStateWithSelectors({
    given: 'three calls are added to the history and oldest is selected',
    withState: () =>
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        selectHistoryEntry(tabId, call1.id, call1.request),
      ]),
    expectations: should => [
      should('have newer entries', getHasNewerHistoryEntries, true),
      should('not have older entries', getHasOlderHistoryEntries, false),
      should('newest should be selected', getAllHttpCalls, [
        { ...call3, isSelected: false },
        { ...call2, isSelected: false },
        { ...call1, isSelected: true },
      ]),
    ],
  });

  assertStateWithSelectors({
    given:
      'three calls are added to the history and then the next older call is selected',
    withState: () =>
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        selectNextOlderHistoryEntry(tabId),
      ]),
    expectations: should => [
      should('have newer entries', getHasNewerHistoryEntries, true),
      should('have older entries', getHasOlderHistoryEntries, true),
      should('select second call', getAllHttpCalls, [
        { ...call3, isSelected: false },
        { ...call2, isSelected: true },
        { ...call1, isSelected: false },
      ]),
    ],
  });

  assertStateWithSelectors({
    given: 'oldest call is selected and then the next newer call is selected',
    withState: () =>
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        selectHistoryEntry(tabId, call1.id, call1.request),
        selectNextNewerHistoryEntry(tabId),
      ]),
    expectations: should => [
      should('have newer entries', getHasNewerHistoryEntries, true),
      should('have older entries', getHasOlderHistoryEntries, true),
      should('select second call', getAllHttpCalls, [
        { ...call3, isSelected: false },
        { ...call2, isSelected: true },
        { ...call1, isSelected: false },
      ]),
    ],
  });
});
