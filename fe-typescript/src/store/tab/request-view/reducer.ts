import { createReducer } from 'deox';
import {
  removeHeader,
  revertEditState,
  setBody,
  setHeaderKey,
  setHeaderValue,
  setMethod,
  setRequestToEdit,
  setUrl,
} from './action';
import { HttpCallRequest, HttpRequestHeader } from '../../../types/http-call';
import { httpCallSucceeded } from '../http-call/action';
import { getNextId } from '../../../functions/utility/utility';
import { isNil, last } from 'ramda';
import {
  DeepWithoutUnionTypes,
  UnionType2,
} from '@healthinal/typescript-schema-transformer';

export type RequestViewStateForSchema = {
  readonly isChanged: boolean;
  readonly overwrittenRequest: UnionType2<undefined, HttpCallRequest>;
  readonly requestToEdit: HttpCallRequest;
};

export type RequestViewState = DeepWithoutUnionTypes<RequestViewStateForSchema>;

export const initialState: RequestViewState = {
  isChanged: false,
  requestToEdit: {
    request: {
      method: 'GET',
      url: '',
    },
    headers: [
      { id: 'header-0', key: 'Content-Type', value: 'application/json' },
      { id: 'header-1', key: 'Cache-Control', value: 'no-cache' },
      { id: 'header-2', key: '', value: '' },
    ],
    body: '',
  },
  overwrittenRequest: undefined,
};

const getId = getNextId('header-', ({ id }: HttpRequestHeader) => id);

const addHeaderRowIfLastHeaderRowIsNotEmpty = (
  headers: HttpRequestHeader[]
) => {
  const lastHeader = last(headers);
  return !isNil(lastHeader) && lastHeader.key === '' && lastHeader.value === ''
    ? headers
    : [
        ...headers,
        {
          id: getId(headers),
          key: '',
          value: '',
        },
      ];
};

export const requestViewReducer = createReducer(initialState, handleAction => [
  handleAction(setMethod, (state, { payload }) => ({
    ...state,
    isChanged: true,
    overwrittenRequest: undefined,
    requestToEdit: {
      ...state.requestToEdit,
      request: {
        ...state.requestToEdit.request,
        method: payload.method,
      },
    },
  })),
  handleAction(setUrl, (state, { payload }) => ({
    ...state,
    isChanged: true,
    overwrittenRequest: undefined,
    requestToEdit: {
      ...state.requestToEdit,
      request: {
        ...state.requestToEdit.request,
        url: payload.url,
      },
    },
  })),
  handleAction(setBody, (state, { payload }) => ({
    ...state,
    isChanged: true,
    overwrittenRequest: undefined,
    requestToEdit: {
      ...state.requestToEdit,
      body: payload.body,
    },
  })),
  handleAction(removeHeader, (state, { payload }) => ({
    ...state,
    isChanged: true,
    overwrittenRequest: undefined,
    requestToEdit: {
      ...state.requestToEdit,
      headers: addHeaderRowIfLastHeaderRowIsNotEmpty(
        state.requestToEdit.headers.filter(header => header.id !== payload.id)
      ),
    },
  })),
  handleAction(setHeaderKey, (state, { payload }) => ({
    ...state,
    isChanged: true,
    overwrittenRequest: undefined,
    requestToEdit: {
      ...state.requestToEdit,
      headers: addHeaderRowIfLastHeaderRowIsNotEmpty(
        state.requestToEdit.headers.map(header =>
          header.id !== payload.id ? header : { ...header, key: payload.newKey }
        )
      ),
    },
  })),
  handleAction(setHeaderValue, (state, { payload }) => ({
    ...state,
    isChanged: true,
    overwrittenRequest: undefined,
    requestToEdit: {
      ...state.requestToEdit,
      headers: addHeaderRowIfLastHeaderRowIsNotEmpty(
        state.requestToEdit.headers.map(header =>
          header.id !== payload.id
            ? header
            : { ...header, value: payload.newValue }
        )
      ),
    },
  })),
  handleAction(httpCallSucceeded, state => ({
    ...state,
    isChanged: false,
    overwrittenRequest: undefined,
  })),
  handleAction(setRequestToEdit, (state, { payload }) => ({
    ...state,
    requestToEdit: payload.request,
    ...(state.isChanged
      ? {
          isChanged: false,
          overwrittenRequest: state.requestToEdit,
        }
      : {}),
  })),
  handleAction(revertEditState, (state, { payload }) => ({
    ...state,
    requestToEdit: payload.request,
    isChanged: true,
    overwrittenRequest: undefined,
  })),
]);
