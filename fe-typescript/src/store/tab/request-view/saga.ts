import { put, select, takeLatest } from 'redux-saga/effects';
import { ActionType, getType } from 'deox';
import { setRequestToEdit } from './action';
import { isNil } from 'ramda';
import {
  selectHistoryEntry,
  selectNextNewerHistoryEntry,
  selectNextOlderHistoryEntry,
} from '../history/action';
import { getSelectedRequest } from '../history/selector';

function* dispatchSetRequestToEdit({
  payload,
}:
  | ActionType<typeof selectHistoryEntry>
  | ActionType<typeof selectNextNewerHistoryEntry>
  | ActionType<typeof selectNextOlderHistoryEntry>) {
  const selectedRequest = yield select(getSelectedRequest);
  if (!isNil(selectedRequest)) {
    yield put(setRequestToEdit(payload.tabId, selectedRequest));
  }
}

export function* requestViewSaga() {
  yield takeLatest(getType(selectHistoryEntry), dispatchSetRequestToEdit);
  yield takeLatest(
    getType(selectNextNewerHistoryEntry),
    dispatchSetRequestToEdit
  );
  yield takeLatest(
    getType(selectNextOlderHistoryEntry),
    dispatchSetRequestToEdit
  );
}
