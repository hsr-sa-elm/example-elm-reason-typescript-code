import { assertStateWithSelectors } from '../../../test-utility/assert';
import {
  removeHeader,
  revertEditState,
  setBody,
  setHeaderKey,
  setHeaderValue,
  setMethod,
  setRequestToEdit,
  setUrl,
} from './action';
import { head, init, last, reduce, tail } from 'ramda';
import {
  getIsChanged,
  getOverwrittenRequest,
  getRequestToEdit,
} from './selector';
import { httpCallSucceeded } from '../http-call/action';
import {
  createHttpCallRequest,
  createHttpCallResponse,
} from '../../../test-utility/factories/types';
import { rootReducer } from '../../index';
import { getSelectedTabId } from '../selector';
import { HttpCallRequest, HttpRequestHeader } from '../../../types/http-call';

const reduceActions = reduce(rootReducer, rootReducer(undefined, {} as any));
const tabId = getSelectedTabId(reduceActions([]));

const baseRequest = getRequestToEdit(reduceActions([])) as HttpCallRequest;
const firstHeader = head(baseRequest.headers as HttpRequestHeader[]);
const lastHeader = last(baseRequest.headers as HttpRequestHeader[]);
const changedRequest = createHttpCallRequest({
  request: { method: 'PUT', url: 'http://example.com' },
  headers: [{ id: 'header-1', key: 'Some-Header', value: 'Some value' }],
  body: 'some body',
});

const callSucceededAction = httpCallSucceeded(
  tabId,
  createHttpCallRequest(),
  createHttpCallResponse(),
  '2019-10-29T17:26:42.374Z',
  '2019-10-29T17:26:44.123Z'
);

describe('request-view state', () => {
  assertStateWithSelectors({
    given: 'initial state',
    withState: () => reduceActions([]),
    expectations: should => [
      should('not be changed', getIsChanged, false),
      should('contain the new method', getRequestToEdit, baseRequest),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'method is set',
    withState: () => reduceActions([setMethod(tabId, 'POST')]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the new method', getRequestToEdit, {
        ...baseRequest,
        request: { ...baseRequest.request, method: 'POST' },
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'url is set',
    withState: () => reduceActions([setUrl(tabId, 'https://secure.com')]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the new url', getRequestToEdit, {
        ...baseRequest,
        request: { ...baseRequest.request, url: 'https://secure.com' },
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'body is set',
    withState: () => reduceActions([setBody(tabId, 'foo')]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the new body', getRequestToEdit, {
        ...baseRequest,
        body: 'foo',
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'remove header',
    withState: () => reduceActions([removeHeader(tabId, firstHeader.id)]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('not contain the removed header', getRequestToEdit, {
        ...baseRequest,
        headers: tail(baseRequest.headers as HttpRequestHeader[]),
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'change header key',
    withState: () =>
      reduceActions([setHeaderKey(tabId, firstHeader.id, 'some key')]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the changed key', getRequestToEdit, {
        ...baseRequest,
        headers: [
          {
            ...firstHeader,
            key: 'some key',
          },
          ...tail(baseRequest.headers as HttpRequestHeader[]),
        ],
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'change header value',
    withState: () =>
      reduceActions([setHeaderValue(tabId, firstHeader.id, 'some value')]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the changed value', getRequestToEdit, {
        ...baseRequest,
        headers: [
          {
            ...firstHeader,
            value: 'some value',
          },
          ...tail(baseRequest.headers as HttpRequestHeader[]),
        ],
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'change the value of the last header',
    withState: () =>
      reduceActions([setHeaderValue(tabId, lastHeader.id, 'some value')]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the changed value', getRequestToEdit, {
        ...baseRequest,
        headers: [
          ...init(baseRequest.headers as HttpRequestHeader[]),
          {
            ...lastHeader,
            value: 'some value',
          },
          {
            id: 'header-3',
            key: '',
            value: '',
          },
        ],
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'method and url are set and then overwritten',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'POST'),
        setUrl(tabId, 'https://secure.com'),
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
      ]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the latest method and url', getRequestToEdit, {
        ...baseRequest,
        request: { method: 'PUT', url: 'http://example.com' },
      }),
    ],
  });

  assertStateWithSelectors({
    given: 'an edited request is executed',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        callSucceededAction,
      ]),
    expectations: should => [
      should('not be changed', getIsChanged, false),
      should('not change request', getRequestToEdit, {
        ...baseRequest,
        request: { method: 'PUT', url: 'http://example.com' },
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'a edited request is overwritten by a selected history entry',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        setRequestToEdit(tabId, changedRequest),
      ]),
    expectations: should => [
      should('not be changed', getIsChanged, false),
      should(
        'contain the request of the history entry',
        getRequestToEdit,
        changedRequest
      ),
      should('have an overwritten request', getOverwrittenRequest, {
        ...baseRequest,
        request: { method: 'PUT', url: 'http://example.com' },
      }),
    ],
  });

  assertStateWithSelectors({
    given: 'an overwritten request is reverted',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        setRequestToEdit(tabId, changedRequest),
        revertEditState(tabId, { ...baseRequest, body: 'other body' }),
      ]),
    expectations: should => [
      should('be changed', getIsChanged, true),
      should('contain the reverted request', getRequestToEdit, {
        ...baseRequest,
        body: 'other body',
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'a sent request is overwritten by a selected history entry',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        callSucceededAction,
        setRequestToEdit(tabId, changedRequest),
      ]),
    expectations: should => [
      should('not be changed', getIsChanged, false),
      should('contain the reverted request', getRequestToEdit, changedRequest),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'a edited request is overwritten twice',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        setRequestToEdit(tabId, changedRequest),
        setRequestToEdit(tabId, {
          ...changedRequest,
          body: 'some other body',
        }),
      ]),
    expectations: should => [
      should('not be changed', getIsChanged, false),
      should('contain the reverted request', getRequestToEdit, {
        ...changedRequest,
        body: 'some other body',
      }),
      should('have the originally overwritten request', getOverwrittenRequest, {
        ...baseRequest,
        request: { method: 'PUT', url: 'http://example.com' },
      }),
    ],
  });

  assertStateWithSelectors({
    given: 'a selected request which overwrote another gets changed',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        setRequestToEdit(tabId, changedRequest),
        setBody(tabId, 'foo'),
      ]),
    expectations: should => [
      should('not be changed', getIsChanged, true),
      should('contain the reverted request', getRequestToEdit, {
        ...changedRequest,
        body: 'foo',
      }),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });

  assertStateWithSelectors({
    given: 'a selected request which overwrote another is executed',
    withState: () =>
      reduceActions([
        setMethod(tabId, 'PUT'),
        setUrl(tabId, 'http://example.com'),
        setRequestToEdit(tabId, changedRequest),
        callSucceededAction,
      ]),
    expectations: should => [
      should('not be changed', getIsChanged, false),
      should('contain the reverted request', getRequestToEdit, changedRequest),
      should(
        'not have an overwritten request',
        getOverwrittenRequest,
        undefined
      ),
    ],
  });
});
