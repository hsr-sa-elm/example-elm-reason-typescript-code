import { createActionCreator } from 'deox';
import { HttpCallRequest } from '../../../types/http-call';

export const setMethod = createActionCreator(
  'requestView/METHOD_SET',
  resolve => (tabId: string, method: string) => resolve({ tabId, method })
);

export const setUrl = createActionCreator(
  'requestView/URL_SET',
  resolve => (tabId: string, url: string) => resolve({ tabId, url })
);

export const setBody = createActionCreator(
  'requestView/BODY_SET',
  resolve => (tabId: string, body: string) => resolve({ tabId, body })
);

export const removeHeader = createActionCreator(
  'requestView/HEADER_REMOVE',
  resolve => (tabId: string, id: string) => resolve({ tabId, id })
);

export const setHeaderValue = createActionCreator(
  'requestView/HEADER_SET_VALUE',
  resolve => (tabId: string, id: string, newValue: string) =>
    resolve({ tabId, id, newValue })
);

export const setHeaderKey = createActionCreator(
  'requestView/HEADER_SET_KEY',
  resolve => (tabId: string, id: string, newKey: string) =>
    resolve({ tabId, id, newKey })
);

export const revertEditState = createActionCreator(
  'requestView/REVERT',
  resolve => (tabId: string, request: HttpCallRequest) =>
    resolve({ tabId, request })
);

export const setRequestToEdit = createActionCreator(
  'requestView/SET_REQUEST',
  resolve => (tabId: string, request: HttpCallRequest) =>
    resolve({ tabId, request })
);
