import { pipe, defaultTo } from 'ramda';
import { getCurrentTab } from '../selector';
import { mapIfNotNil } from '../../../functions/utility/utility';

const getRequestViewState = pipe(
  getCurrentTab,
  mapIfNotNil(({ requestView }) => requestView)
);

export const getRequestToEdit = pipe(
  getRequestViewState,
  mapIfNotNil(({ requestToEdit }) => requestToEdit)
);

export const getIsChanged = pipe(
  getRequestViewState,
  mapIfNotNil(({ isChanged }) => isChanged),
  defaultTo(false)
);

export const getOverwrittenRequest = pipe(
  getRequestViewState,
  mapIfNotNil(({ overwrittenRequest }) => overwrittenRequest)
);
