import { find, map, pipe, isNil, complement } from 'ramda';
import { RootState } from '../index';
import { createSelector } from 'reselect';
import { TabView } from '../../types/tab';
import { mapIfNotNil } from '../../functions/utility/utility';
import { TabState } from './reducer';

const getTabState = ({ tab }: RootState) => tab;

const getTabs = pipe(
  getTabState,
  ({ tabs }) => tabs
);

export const getSelectedTabId = pipe(
  getTabState,
  ({ selectedTab }) => selectedTab
);

export const getCurrentTab = createSelector(
  getTabs,
  getSelectedTabId,
  (tabs, selectedTab) =>
    mapIfNotNil(({ tabData }: TabState) => tabData)(
      find(({ id }) => id === selectedTab, tabs)
    )
);

const getIsTabActiveSelector = createSelector(
  getSelectedTabId,
  activeId => (id: string) => id === activeId
);

export const getAllTabs = createSelector(
  getTabs,
  getIsTabActiveSelector,
  (tabs, getIsTabActive) =>
    map(
      ({ id, tabData }): TabView => ({
        id,
        method: tabData.requestView.requestToEdit.request.method,
        url: tabData.requestView.requestToEdit.request.url,
        isActive: getIsTabActive(id),
      }),
      tabs
    )
);

export const hasSelectedTab = createSelector(
  getCurrentTab,
  complement(isNil)
);
