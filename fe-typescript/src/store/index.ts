import { combineReducers } from 'redux';
import { all } from 'redux-saga/effects';
import { httpCallSaga } from './tab/http-call/saga';
import { tabReducer, TabsStateForSchema } from './tab/reducer';
import { persistenceSaga } from './persistence/saga';
import { DeepWithoutUnionTypes } from '@healthinal/typescript-schema-transformer';
import { appViewReducer, AppViewState } from './app-view/reducer';
import { requestViewSaga } from './tab/request-view/saga';

export type RootStateForSchema = {
  readonly appView: AppViewState;
  readonly tab: TabsStateForSchema;
};

export type RootState = DeepWithoutUnionTypes<RootStateForSchema>;

export function* rootSaga() {
  yield all([httpCallSaga(), persistenceSaga(), requestViewSaga()]);
}

export const rootReducer = combineReducers({
  appView: appViewReducer,
  tab: tabReducer,
});
