import {
  addStaticValueSchema,
  createUnionTypeTransformationSchema,
  noTransformationSchema,
  ObjectTransformationSchema,
  optionalStringSchema,
  requiredBooleanSchema,
  requiredNumberSchema,
  requiredStringSchema,
} from '@healthinal/typescript-schema-transformer';
import { RootStateForSchema } from '../index';
import { HttpCallRequest } from '../../types/http-call';

const requestSchema: ObjectTransformationSchema<HttpCallRequest> = {
  request: {
    method: requiredStringSchema(),
    url: requiredStringSchema(),
  },
  headers: [
    {
      id: requiredStringSchema(),
      key: requiredStringSchema(),
      value: requiredStringSchema(),
    },
  ],
  body: requiredStringSchema(),
};

export const rootStateSchema: ObjectTransformationSchema<RootStateForSchema> = {
  appView: {
    isProViewVisible: requiredBooleanSchema(),
  },
  tab: {
    selectedTab: requiredStringSchema(),
    tabs: [
      {
        id: requiredStringSchema(),
        tabData: {
          history: {
            selectedId: optionalStringSchema(),
            httpCalls: [
              {
                id: requiredStringSchema(),
                request: requestSchema,
                response: {
                  status: {
                    code: requiredNumberSchema(),
                    message: requiredStringSchema(),
                  },
                  headers: [
                    {
                      key: requiredStringSchema(),
                      value: requiredStringSchema(),
                    },
                  ],
                  body: requiredStringSchema(),
                },
                requestTime: requiredStringSchema(),
                responseTime: requiredStringSchema(),
              },
            ],
          },
          httpCall: {
            isLoading: addStaticValueSchema(false),
            error: addStaticValueSchema(undefined),
          },
          requestView: {
            isChanged: requiredBooleanSchema(),
            requestToEdit: requestSchema,
            overwrittenRequest: createUnionTypeTransformationSchema<
              any,
              undefined,
              HttpCallRequest
            >(noTransformationSchema, value =>
              typeof value === 'object'
                ? requestSchema
                : addStaticValueSchema(undefined)
            ),
          },
        },
      },
    ],
  },
};
