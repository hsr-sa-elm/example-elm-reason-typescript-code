import { assert } from '../../test-utility/assert';
import { deserializeRootState, serializeRootState } from './helper';
import { rootReducer } from '../index';
import { reduce } from 'ramda';
import { addTab } from '../tab/action';
import { setBody } from '../tab/request-view/action';
import { getSelectedTabId } from '../tab/selector';

const reduceActions = reduce(rootReducer, rootReducer(undefined, {} as any));
const initialState = reduceActions([]);
const tabId = getSelectedTabId(initialState);

describe('serialization', () => {
  assert({
    given: 'initial state is serialized and deserialized',
    should: 'be the same',
    actual: () => deserializeRootState(serializeRootState(initialState)),
    expected: initialState,
  });

  {
    const state = reduceActions([addTab(), setBody(tabId, 'Some body')]);
    assert({
      given: 'changed state is serialized and deserialized',
      should: 'be the same',
      actual: () => deserializeRootState(serializeRootState(state)),
      expected: state,
    });
  }

  assert({
    given: 'incorrect data is deserialized',
    should: 'return initial state',
    actual: () => deserializeRootState('{}'),
    expected: initialState,
  });

  assert({
    given: 'incorrect json is deserialized',
    should: 'return initial state',
    actual: () => deserializeRootState(']}'),
    expected: initialState,
  });
});
