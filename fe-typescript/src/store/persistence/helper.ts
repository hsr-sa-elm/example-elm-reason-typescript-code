import { rootReducer, RootState } from '../index';
import {
  hasNoValidationRemarks,
  transformWithSchema,
} from '@healthinal/typescript-schema-transformer';
import { rootStateSchema } from './schema';
import { pipe } from 'ramda';

const cacheKey = 'ts-app-data';

export const serializeRootState = (state: RootState) => JSON.stringify(state);

export const deserializeRootState = (serializedState: unknown) => {
  try {
    if (typeof serializedState === 'string') {
      const [rootState, validationRemarks] = transformWithSchema(
        rootStateSchema,
        JSON.parse(serializedState)
      );
      if (hasNoValidationRemarks(validationRemarks)) {
        return rootState;
      }
    }
  } catch (e) {}
  // @@INIT is the initialization action of Redux
  return rootReducer(undefined, { type: '@@INIT' });
};

export const saveState = pipe(
  serializeRootState,
  data => localStorage.setItem(cacheKey, data)
);

export const loadState = () =>
  deserializeRootState(localStorage.getItem(cacheKey));
