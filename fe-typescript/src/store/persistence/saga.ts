import { call, select, takeEvery } from 'redux-saga/effects';
import { identity } from 'ramda';
import { saveState } from './helper';

function* saveStateSaga() {
  const state = yield select(identity);
  yield call(saveState, state);
}

export function* persistenceSaga() {
  yield takeEvery('*', saveStateSaga);
}
