import React from 'react';
import { Props } from './TabsContainer';

const Tabs: React.FC<Props> = ({ tabs, activateTab, removeTab, addTab }) => (
  <div>
    <ul
      className="mdc-list mdc-list--two-line"
      role="listbox"
      data-mdc-auto-init="MDCList"
    >
      {tabs.map(({ id, url, method, isActive }) => (
        <li
          key={id}
          onClick={() => activateTab(id)}
          className={`mdc-list-item ${
            isActive ? 'mdc-list-item--selected' : ''
          }`}
          role="option"
          aria-selected={isActive}
        >
          <span className="mdc-list-item__text">
            <span className="mdc-list-item__primary-text">{method}</span>
            <span className="mdc-list-item__secondary-text">
              {url || 'No URL'}
            </span>
          </span>
          <span className="mdc-list-item__meta">
            <button
              className="mdc-icon-button material-icons"
              onClick={event => {
                event.stopPropagation();
                removeTab(id);
              }}
            >
              remove
            </button>
          </span>
        </li>
      ))}
    </ul>
    <button
      data-mdc-auto-init="MDCRipple"
      className="mdc-button mdc-button--unelevated"
      onClick={_event => addTab()}
    >
      <i className="material-icons"> add </i>
      <span className="mdc-button__label"> New Tab </span>
    </button>
  </div>
);

export default Tabs;
