import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { RootState } from '../../store';
import { getAllTabs } from '../../store/tab/selector';
import Tabs from './Tabs';
import { activateTab, addTab, removeTab } from '../../store/tab/action';
import { TabView } from '../../types/tab';

type OwnProps = {};

type StateProps = {
  readonly tabs: readonly TabView[];
};

type DispatchProps = {
  readonly activateTab: (tabId: string) => void;
  readonly removeTab: (tabId: string) => void;
  readonly addTab: () => void;
};

const mapStateToProps: MapStateToProps<
  StateProps,
  OwnProps,
  RootState
> = state => ({
  tabs: getAllTabs(state),
});

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  OwnProps
> = dispatch => ({
  activateTab: id => dispatch(activateTab(id)),
  removeTab: id => dispatch(removeTab(id)),
  addTab: () => dispatch(addTab()),
});

export type Props = OwnProps & StateProps & DispatchProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tabs);
