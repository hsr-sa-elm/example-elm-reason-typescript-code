import React from 'react';
import { Props } from './RequestContainer';
import RequestBody from './RequestBody';
import RequestHeaders from './RequestHeaders';

const Request: React.FC<Props> = ({
  isChanged,
  overwrittenRequest,
  request,
  isProViewVisible,
  hasNewerHistoryEntries,
  hasOlderHistoryEntries,
  setMethod,
  setUrl,
  setBody,
  setHeaderKey,
  setHeaderValue,
  removeHeader,
  send,
  revertEditState,
  selectNextNewerHistoryEntry,
  selectNextOlderHistoryEntry,
}) =>
  !request ? (
    <></>
  ) : (
    <div className="request">
      <h2 className="mdc-typography--headline5">Request{isChanged && '*'}</h2>
      {!isProViewVisible && (
        <div className="history-buttons">
          <button
            className="mdc-icon-button material-icons"
            onClick={selectNextOlderHistoryEntry}
            disabled={!hasOlderHistoryEntries}
          >
            chevron_left
          </button>
          <button
            className="mdc-icon-button material-icons"
            onClick={selectNextNewerHistoryEntry}
            disabled={!hasNewerHistoryEntries}
          >
            chevron_right
          </button>
        </div>
      )}
      <div
        className="mdc-select mdc-select--outlined"
        data-mdc-auto-init="MDCSelect"
      >
        <i className="mdc-select__dropdown-icon" />
        <select
          className="mdc-select__native-control"
          onChange={event => setMethod(event.target.value)}
          value={request.request.method}
          data-testid="method-input"
        >
          <option value="GET">GET</option>
          <option value="POST">POST</option>
          <option value="PUT">PUT</option>
          <option value="PATCH">PATCH</option>
          <option value="DELETE">DELETE</option>
          <option value="HEAD">HEAD</option>
          <option value="CONNECT">CONNECT</option>
          <option value="TRACE">TRACE</option>
          <option value="OPTIONS">OPTIONS</option>
        </select>
        <div className="mdc-notched-outline">
          <div className="mdc-notched-outline__leading" />
          <div className="mdc-notched-outline__notch">
            <label className="mdc-floating-label mdc-floating-label--float-above">
              Method
            </label>
          </div>
          <div className="mdc-notched-outline__trailing" />
        </div>
      </div>
      <div
        className="mdc-text-field mdc-text-field--outlined request-url"
        data-mdc-auto-init="MDCTextField"
      >
        <input
          value={request.request.url}
          onChange={event => setUrl(event.target.value)}
          type="text"
          id="url"
          className="mdc-text-field__input"
          data-testid="url-input"
        />
        <div className="mdc-notched-outline">
          <div className="mdc-notched-outline__leading" />
          <div className="mdc-notched-outline__notch">
            <label className="mdc-floating-label" htmlFor="url">
              URL
            </label>
          </div>
          <div className="mdc-notched-outline__trailing" />
        </div>
      </div>
      <RequestHeaders
        headers={request.headers}
        setHeaderKey={setHeaderKey}
        setHeaderValue={setHeaderValue}
        removeHeader={removeHeader}
      />
      <RequestBody body={request.body} setBody={setBody} />
      <button
        onClick={() => send(request)}
        className="mdc-button mdc-button--raised"
        data-mdc-auto-init="MDCRipple"
        data-testid="send-button"
      >
        <span className="mdc-button__label">Send</span>
      </button>
      {overwrittenRequest && (
        <div className="mdc-snackbar mdc-snackbar--open">
          <div className="mdc-snackbar__surface">
            <div className="mdc-snackbar__label">
              The content of the request has been overwritten with the selected
              request of the history.
            </div>
            <div className="mdc-snackbar__actions">
              <button
                onClick={() => revertEditState(overwrittenRequest)}
                className="mdc-button mdc-snackbar__action"
                data-testid="undo-button"
              >
                Undo
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );

export default Request;
