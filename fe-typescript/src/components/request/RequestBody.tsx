import React from 'react';

const RequestBody: React.FC<{
  body: string;
  setBody: (body: string) => void;
}> = ({ body, setBody }) => (
  <div className="request-body">
    <div
      className="mdc-text-field mdc-text-field--textarea mdc-text-field--fullwidth"
      data-mdc-auto-init="MDCTextField"
    >
      <textarea
        value={body}
        id="request-body"
        className="mdc-text-field__input"
        onChange={event => setBody(event.target.value)}
        data-testid="body-input"
      />
      <div className="mdc-notched-outline">
        <div className="mdc-notched-outline__leading" />
        <div className="mdc-notched-outline__notch">
          <label className="mdc-floating-label" htmlFor="request-body">
            Body
          </label>
        </div>
        <div className="mdc-notched-outline__trailing" />
      </div>
    </div>
  </div>
);

export default RequestBody;
