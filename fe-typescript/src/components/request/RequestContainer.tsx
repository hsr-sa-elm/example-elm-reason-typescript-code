import {
  connect,
  MapDispatchToProps,
  MapStateToProps,
  MergeProps,
} from 'react-redux';
import { RootState } from '../../store';
import { doHttpCall } from '../../store/tab/http-call/action';
import Request from './Request';
import {
  removeHeader,
  revertEditState,
  setBody,
  setHeaderKey,
  setHeaderValue,
  setMethod,
  setUrl,
} from '../../store/tab/request-view/action';
import { HttpCallRequest } from '../../types/http-call';
import {
  getIsChanged,
  getOverwrittenRequest,
  getRequestToEdit,
} from '../../store/tab/request-view/selector';
import { getSelectedTabId } from '../../store/tab/selector';
import { getIsProViewVisible } from '../../store/app-view/selector';
import {
  selectNextNewerHistoryEntry,
  selectNextOlderHistoryEntry,
} from '../../store/tab/history/action';
import {
  getHasNewerHistoryEntries,
  getHasOlderHistoryEntries,
} from '../../store/tab/history/selector';

interface OwnProps {}

interface PassedStateProps {
  readonly request?: HttpCallRequest;
  readonly isChanged: boolean;
  readonly overwrittenRequest?: HttpCallRequest;
  readonly isProViewVisible: boolean;
  readonly hasNewerHistoryEntries: boolean;
  readonly hasOlderHistoryEntries: boolean;
}

interface MergeStateProps {
  readonly tabId: string;
}

type StateProps = PassedStateProps & MergeStateProps;

interface PassedDispatchProps {}

interface MergeDispatchProps {
  readonly setMethodWithTabId: (tabId: string, method: string) => void;
  readonly setUrlWithTabId: (tabId: string, url: string) => void;
  readonly setBodyWithTabId: (tabId: string, body: string) => void;
  readonly setHeaderKeyWithTabId: (
    tabId: string,
    id: string,
    key: string
  ) => void;
  readonly setHeaderValueWithTabId: (
    tabId: string,
    id: string,
    value: string
  ) => void;
  readonly removeHeaderWithTabId: (tabId: string, id: string) => void;
  readonly sendWithTabId: (tabId: string, request: HttpCallRequest) => void;
  readonly revertEditStateWithTabId: (
    tabId: string,
    oldRequest: HttpCallRequest
  ) => void;
  readonly selectNextNewerHistoryEntryWithTabId: (tabId: string) => void;
  readonly selectNextOlderHistoryEntryWithTabId: (tabId: string) => void;
}

type DispatchProps = PassedDispatchProps & MergeDispatchProps;

type MergedProps = PassedStateProps &
  PassedDispatchProps & {
    readonly setMethod: (method: string) => void;
    readonly setUrl: (url: string) => void;
    readonly setBody: (body: string) => void;
    readonly setHeaderKey: (id: string, key: string) => void;
    readonly setHeaderValue: (id: string, value: string) => void;
    readonly removeHeader: (id: string) => void;
    readonly send: (request: HttpCallRequest) => void;
    readonly revertEditState: (oldRequest: HttpCallRequest) => void;
    readonly selectNextNewerHistoryEntry: () => void;
    readonly selectNextOlderHistoryEntry: () => void;
  };

const mapStateToProps: MapStateToProps<
  StateProps,
  OwnProps,
  RootState
> = state => ({
  request: getRequestToEdit(state),
  isChanged: getIsChanged(state),
  overwrittenRequest: getOverwrittenRequest(state),
  tabId: getSelectedTabId(state),
  isProViewVisible: getIsProViewVisible(state),
  hasNewerHistoryEntries: getHasNewerHistoryEntries(state),
  hasOlderHistoryEntries: getHasOlderHistoryEntries(state),
});

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  OwnProps
> = dispatch => ({
  setMethodWithTabId: (tabId, method) => dispatch(setMethod(tabId, method)),
  setUrlWithTabId: (tabId, url) => dispatch(setUrl(tabId, url)),
  setBodyWithTabId: (tabId, body) => dispatch(setBody(tabId, body)),
  setHeaderKeyWithTabId: (tabId, id, key) =>
    dispatch(setHeaderKey(tabId, id, key)),
  setHeaderValueWithTabId: (tabId, id, value) =>
    dispatch(setHeaderValue(tabId, id, value)),
  removeHeaderWithTabId: (tabId, id) => dispatch(removeHeader(tabId, id)),
  sendWithTabId: (tabId, request) => dispatch(doHttpCall(tabId, request)),
  revertEditStateWithTabId: (tabId, request) =>
    dispatch(revertEditState(tabId, request)),
  selectNextNewerHistoryEntryWithTabId: tabId =>
    dispatch(selectNextNewerHistoryEntry(tabId)),
  selectNextOlderHistoryEntryWithTabId: tabId =>
    dispatch(selectNextOlderHistoryEntry(tabId)),
});

const mergeProps: MergeProps<
  StateProps,
  DispatchProps,
  OwnProps,
  MergedProps
> = (
  { tabId, ...stateProps },
  {
    setMethodWithTabId,
    setUrlWithTabId,
    setBodyWithTabId,
    setHeaderKeyWithTabId,
    setHeaderValueWithTabId,
    removeHeaderWithTabId,
    sendWithTabId,
    revertEditStateWithTabId,
    selectNextNewerHistoryEntryWithTabId,
    selectNextOlderHistoryEntryWithTabId,
    ...dispatchProps
  },
  ownProps
) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  setMethod: method => setMethodWithTabId(tabId, method),
  setUrl: url => setUrlWithTabId(tabId, url),
  setBody: body => setBodyWithTabId(tabId, body),
  setHeaderKey: (id, key) => setHeaderKeyWithTabId(tabId, id, key),
  setHeaderValue: (id, value) => setHeaderValueWithTabId(tabId, id, value),
  removeHeader: id => removeHeaderWithTabId(tabId, id),
  send: request => sendWithTabId(tabId, request),
  revertEditState: request => revertEditStateWithTabId(tabId, request),
  selectNextNewerHistoryEntry: () =>
    selectNextNewerHistoryEntryWithTabId(tabId),
  selectNextOlderHistoryEntry: () =>
    selectNextOlderHistoryEntryWithTabId(tabId),
});

export type Props = OwnProps & MergedProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Request);
