import * as React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import Request from './Request';
import { createHttpCallRequest } from '../../test-utility/factories/types';
import { HttpCallRequest } from '../../types/http-call';

const exampleRequest = createHttpCallRequest({
  request: {
    method: 'POST',
    url: 'http://example.com',
  },
  body: 'foo',
});

describe('Request component', () => {
  beforeEach(() => jest.resetAllMocks());
  const onSetMethod = jest.fn();
  const onSetUrl = jest.fn();
  const onSetBody = jest.fn();
  const onSetHeaderKey = jest.fn();
  const onSetHeaderValue = jest.fn();
  const onRemoveHeader = jest.fn();
  const onSend = jest.fn();
  const onRevertEditState = jest.fn();
  const onNextHistory = jest.fn();
  const onOldHistory = jest.fn();

  const renderRequest = (
    request: HttpCallRequest,
    overwrittenRequest?: HttpCallRequest
  ) =>
    render(
      <Request
        isChanged={false}
        hasNewerHistoryEntries={false}
        hasOlderHistoryEntries={false}
        isProViewVisible={false}
        selectNextNewerHistoryEntry={onNextHistory}
        selectNextOlderHistoryEntry={onOldHistory}
        overwrittenRequest={overwrittenRequest}
        request={request}
        setMethod={onSetMethod}
        setUrl={onSetUrl}
        setBody={onSetBody}
        setHeaderKey={onSetHeaderKey}
        setHeaderValue={onSetHeaderValue}
        removeHeader={onRemoveHeader}
        send={onSend}
        revertEditState={onRevertEditState}
      />
    );

  test('should render request', () => {
    const { getByTestId, queryByTestId, queryAllByTestId } = renderRequest(
      exampleRequest
    );
    expect(getByTestId('method-input')).toHaveValue('POST');
    expect(getByTestId('url-input')).toHaveValue('http://example.com');
    expect(getByTestId('body-input')).toHaveValue('foo');
    expect(queryByTestId('undo-button')).not.toBeInTheDocument();

    const headerKeyInputs = queryAllByTestId('header-key-input');
    const headerValueInputs = queryAllByTestId('header-value-input');
    const headerRemoveButtons = queryAllByTestId('header-remove-button');

    expect(headerKeyInputs).toHaveLength(3);
    expect(headerValueInputs).toHaveLength(3);
    expect(headerRemoveButtons).toHaveLength(2);

    expect(headerKeyInputs[0]).toHaveValue(exampleRequest.headers[0].key);
    expect(headerKeyInputs[1]).toHaveValue(exampleRequest.headers[1].key);
    expect(headerKeyInputs[2]).toHaveValue(exampleRequest.headers[2].key);

    expect(headerValueInputs[0]).toHaveValue(exampleRequest.headers[0].value);
    expect(headerValueInputs[1]).toHaveValue(exampleRequest.headers[1].value);
    expect(headerValueInputs[2]).toHaveValue(exampleRequest.headers[2].value);
  });

  test('should send request', () => {
    const { getByTestId } = renderRequest(exampleRequest);

    expect(onSend).toHaveBeenCalledTimes(0);
    fireEvent.click(getByTestId('send-button'));
    expect(onSend).toHaveBeenCalledWith(exampleRequest);
  });

  test('should update method', () => {
    const { getByTestId } = renderRequest(exampleRequest);

    expect(onSetMethod).toHaveBeenCalledTimes(0);
    fireEvent.change(getByTestId('method-input'), { target: { value: 'PUT' } });
    expect(onSetMethod).toHaveBeenCalledWith('PUT');
  });

  test('should update url', () => {
    const { getByTestId } = renderRequest(exampleRequest);

    expect(onSetUrl).toHaveBeenCalledTimes(0);
    fireEvent.change(getByTestId('url-input'), {
      target: { value: 'http://x.com' },
    });
    expect(onSetUrl).toHaveBeenCalledWith('http://x.com');
  });

  test('should update body', () => {
    const { getByTestId } = renderRequest(exampleRequest);

    expect(onSetBody).toHaveBeenCalledTimes(0);
    fireEvent.change(getByTestId('body-input'), { target: { value: 'bar' } });
    expect(onSetBody).toHaveBeenCalledWith('bar');
  });

  test('should update header key', () => {
    const { queryAllByTestId } = renderRequest(exampleRequest);

    expect(onSetHeaderKey).toHaveBeenCalledTimes(0);
    fireEvent.change(queryAllByTestId('header-key-input')[1], {
      target: { value: 'bar' },
    });
    expect(onSetHeaderKey).toHaveBeenCalledWith('header-1', 'bar');
  });

  test('should update header value', () => {
    const { queryAllByTestId } = renderRequest(exampleRequest);

    expect(onSetHeaderValue).toHaveBeenCalledTimes(0);
    fireEvent.change(queryAllByTestId('header-value-input')[2], {
      target: { value: 'bar' },
    });
    expect(onSetHeaderValue).toHaveBeenCalledWith('header-2', 'bar');
  });

  test('should remove header', () => {
    const { queryAllByTestId } = renderRequest(exampleRequest);

    expect(onSetHeaderValue).toHaveBeenCalledTimes(0);
    fireEvent.click(queryAllByTestId('header-remove-button')[1]);
    expect(onRemoveHeader).toHaveBeenCalledWith('header-1');
  });

  test('should revert request', () => {
    const oldRequest = createHttpCallRequest();
    const { getByTestId } = renderRequest(exampleRequest, oldRequest);

    expect(onRevertEditState).toHaveBeenCalledTimes(0);
    fireEvent.click(getByTestId('undo-button'));
    expect(onRevertEditState).toHaveBeenCalledWith(oldRequest);
  });
});
