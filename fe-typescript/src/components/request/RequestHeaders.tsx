import React from 'react';
import { HttpRequestHeader } from '../../types/http-call';

const RequestHeaders: React.FC<{
  headers: readonly HttpRequestHeader[];
  setHeaderKey: (id: string, key: string) => void;
  setHeaderValue: (id: string, value: string) => void;
  removeHeader: (id: string) => void;
}> = ({ headers, setHeaderKey, setHeaderValue, removeHeader }) => (
  <div className="request-headers">
    <h3 className="mdc-typography--headline5">Headers</h3>
    {headers.map(({ id, key, value }, index) => (
      <React.Fragment key={id}>
        <div
          className="mdc-text-field mdc-text-field--outlined mdc-text-field--no-label"
          data-mdc-auto-init="MDCTextField"
        >
          <input
            value={key}
            onChange={event => setHeaderKey(id, event.target.value)}
            type="text"
            className="mdc-text-field__input"
            aria-label="Name"
            placeholder="Name"
            data-testid="header-key-input"
          />
          <div className="mdc-notched-outline">
            <div className="mdc-notched-outline__leading" />
            <div className="mdc-notched-outline__trailing" />
          </div>
        </div>
        <div
          className="mdc-text-field mdc-text-field--outlined mdc-text-field--no-label"
          data-mdc-auto-init="MDCTextField"
        >
          <input
            value={value}
            onChange={event => setHeaderValue(id, event.target.value)}
            type="text"
            className="mdc-text-field__input"
            aria-label="Value"
            placeholder="Value"
            data-testid="header-value-input"
          />
          <div className="mdc-notched-outline">
            <div className="mdc-notched-outline__leading" />
            <div className="mdc-notched-outline__trailing" />
          </div>
        </div>
        {index < headers.length - 1 && (
          <button
            className="mdc-icon-button material-icons"
            onClick={() => removeHeader(id)}
            data-testid="header-remove-button"
          >
            remove
          </button>
        )}
      </React.Fragment>
    ))}
  </div>
);
export default RequestHeaders;
