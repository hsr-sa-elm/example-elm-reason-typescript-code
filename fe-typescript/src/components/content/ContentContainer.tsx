import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { RootState } from '../../store';
import Content from './Content';
import { hasSelectedTab } from '../../store/tab/selector';
import { getIsProViewVisible } from '../../store/app-view/selector';

type OwnProps = {};
type StateProps = {
  readonly hasSelectedTab: boolean;
  readonly isProViewVisible: boolean;
};
type DispatchProps = {};

const mapStateToProps: MapStateToProps<
  StateProps,
  OwnProps,
  RootState
> = state => ({
  hasSelectedTab: hasSelectedTab(state),
  isProViewVisible: getIsProViewVisible(state),
});

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  OwnProps
> = dispatch => ({});

export type Props = OwnProps & StateProps & DispatchProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Content);
