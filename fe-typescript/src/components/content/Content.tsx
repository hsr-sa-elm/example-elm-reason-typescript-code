import React from 'react';
import { Props } from './ContentContainer';
import RequestContainer from '../request/RequestContainer';
import ResponseContainer from '../response/ResponseContainer';
import HistoryContainer from '../history/HistoryContainer';

const Content: React.FC<Props> = ({ hasSelectedTab, isProViewVisible }) =>
  hasSelectedTab ? (
    <main>
      <RequestContainer />
      <ResponseContainer />
      {isProViewVisible && <HistoryContainer />}
    </main>
  ) : (
    <p className="no-tab-message">No tab selected</p>
  );

export default Content;
