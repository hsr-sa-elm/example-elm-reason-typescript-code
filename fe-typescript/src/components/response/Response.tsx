import React from 'react';
import { Props } from './ResponseContainer';
import { isNil } from 'ramda';

const Response: React.FC<Props> = ({ response, error, isLoading, body }) => (
  <div className="response">
    <h2 className="mdc-typography--headline5">Response</h2>
    {isLoading ? (
      <p className="message">Loading...</p>
    ) : !isNil(error) ? (
      <p className="message">HTTP request failed with message: {error}</p>
    ) : !isNil(response) ? (
      <>
        <p className="response__status">
          {response.status.message} (Code: {response.status.code})
        </p>
        <div className="header-list">
          {response.headers.map(({ key, value }, index) => (
            <React.Fragment key={index}>
              <div className="header-list__header-name">{key}</div>
              <div className="header-list__header-value">{value}</div>
            </React.Fragment>
          ))}
        </div>
        <p className="response__body">{body}</p>
      </>
    ) : (
      <p className="message">Please make a request first to see a response</p>
    )}
  </div>
);

export default Response;
