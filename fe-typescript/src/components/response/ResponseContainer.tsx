import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { RootState } from '../../store';
import Response from './Response';
import { getError, getIsLoading } from '../../store/tab/http-call/selector';
import { HttpCallResponse } from '../../types/http-call';
import {
  getSelectedResponse,
  getSelectedResponseBodyFormatted,
} from '../../store/tab/history/selector';

type OwnProps = {};
type StateProps = {
  readonly response?: HttpCallResponse;
  readonly isLoading: boolean;
  readonly error?: string;
  readonly body: string;
};

type DispatchProps = {};

const mapStateToProps: MapStateToProps<
  StateProps,
  OwnProps,
  RootState
> = state => ({
  response: getSelectedResponse(state),
  error: getError(state),
  isLoading: getIsLoading(state),
  body: getSelectedResponseBodyFormatted(state),
});

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  OwnProps
> = dispatch => ({});

export type Props = OwnProps & StateProps & DispatchProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Response);
