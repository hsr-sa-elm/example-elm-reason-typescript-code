import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { RootState } from '../../store';
import App from './App';
import { getIsProViewVisible } from '../../store/app-view/selector';
import { toggleProViewVisibility } from '../../store/app-view/action';

type OwnProps = {};
type StateProps = {
  readonly isProViewVisible: boolean;
};
type DispatchProps = {
  readonly toggleProViewVisibility: () => void;
};

const mapStateToProps: MapStateToProps<
  StateProps,
  OwnProps,
  RootState
> = state => ({
  isProViewVisible: getIsProViewVisible(state),
});

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  OwnProps
> = dispatch => ({
  toggleProViewVisibility: () => dispatch(toggleProViewVisibility()),
});

export type Props = OwnProps & StateProps & DispatchProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
