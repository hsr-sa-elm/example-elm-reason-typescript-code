import React from 'react';
import TabsContainer from '../tab/TabsContainer';
import ContentContainer from '../content/ContentContainer';
import { Props } from './AppContainer';

const App: React.FC<Props> = ({
  isProViewVisible,
  toggleProViewVisibility,
}) => (
  <div className={'root-container' + (isProViewVisible ? '' : ' simple-view')}>
    <header className="mdc-top-app-bar">
      <div className="mdc-top-app-bar__row">
        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
          <button className="sidebar-toggle material-icons mdc-top-app-bar__navigation-icon mdc-icon-button">
            menu
          </button>
          <span className="mdc-top-app-bar__title">
            HTTP client in TypeScript
          </span>
        </section>
        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-end">
          <div
            className={
              'mdc-switch' + (isProViewVisible ? ' mdc-switch--checked' : '')
            }
            data-mdc-auto-init="MDCSwitch"
          >
            <div className="mdc-switch__track" />
            <div className="mdc-switch__thumb-underlay">
              <div className="mdc-switch__thumb">
                <input
                  id="view-switch"
                  type="checkbox"
                  className="mdc-switch__native-control"
                  role="switch"
                  checked={isProViewVisible}
                  onChange={toggleProViewVisibility}
                />
              </div>
            </div>
          </div>
          <label htmlFor="view-switch">Pro view</label>
        </section>
      </div>
    </header>
    <aside
      className="mdc-drawer mdc-drawer--modal"
      data-mdc-auto-init="MDCDrawer"
    >
      <div className="mdc-drawer__content">
        <TabsContainer />
      </div>
    </aside>
    <div className="mdc-drawer-scrim" />
    <div className="main-wrapper">
      {isProViewVisible && (
        <aside>
          <TabsContainer />
        </aside>
      )}
      <ContentContainer />
    </div>
  </div>
);

export default App;
