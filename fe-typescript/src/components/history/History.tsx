import React from 'react';
import { Props } from './HistoryContainer';
import { formatWithOptions } from 'date-fns/fp';
import { de } from 'date-fns/locale';

const History: React.FC<Props> = ({
  isEmpty,
  httpCalls,
  selectHistoryEntry,
}) => (
  <div className="history">
    <h2 className="mdc-typography--headline5">History</h2>
    {isEmpty ? (
      <p className="mdc-typography--body1">
        The history is empty. Make some requests!
      </p>
    ) : (
      <ul
        className="mdc-list mdc-list--two-line"
        role="listbox"
        data-mdc-auto-init="MDCList"
      >
        {httpCalls.map(
          ({
            id,
            request,
            response,
            requestTime,
            responseTime,
            isSelected,
          }) => (
            <li
              key={id}
              onClick={() => selectHistoryEntry(id, request)}
              className={
                'mdc-list-item' + (isSelected ? ' mdc-list-item--selected' : '')
              }
              role="option"
              aria-selected={isSelected}
            >
              <span className="mdc-list-item__text">
                <span className="mdc-list-item__primary-text">
                  {`${request.request.method} ${request.request.url} (${response.status.message})`}
                </span>
                <span className="mdc-list-item__secondary-text">
                  {`${formatWithOptions(
                    { locale: de },
                    'dd. MMMM y HH:mm',
                    new Date(responseTime)
                  )} (Duration: ${new Date(responseTime).getTime() -
                    new Date(requestTime).getTime()}ms)`}
                </span>
              </span>
            </li>
          )
        )}
      </ul>
    )}
  </div>
);

export default History;
