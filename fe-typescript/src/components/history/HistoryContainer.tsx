import {
  connect,
  MapDispatchToProps,
  MapStateToProps,
  MergeProps,
} from 'react-redux';
import { RootState } from '../../store';
import History from './History';
import {
  HttpCallRequest,
  PerformedHttpCallViewModel,
} from '../../types/http-call';
import { selectHistoryEntry } from '../../store/tab/history/action';
import {
  getAllHttpCalls,
  getIsHistoryEmpty,
} from '../../store/tab/history/selector';
import { getSelectedTabId } from '../../store/tab/selector';

interface OwnProps {}

interface PassedStateProps {
  readonly isEmpty: boolean;
  readonly httpCalls: readonly PerformedHttpCallViewModel[];
}

interface MergeStateProps {
  readonly tabId: string;
}

type StateProps = PassedStateProps & MergeStateProps;

interface PassedDispatchProps {}

interface MergeDispatchProps {
  readonly selectHistoryEntryWithTabId: (
    tabId: string,
    callId: string,
    request: HttpCallRequest
  ) => void;
}

type DispatchProps = PassedDispatchProps & MergeDispatchProps;

type MergedProps = PassedStateProps &
  PassedDispatchProps & {
    readonly selectHistoryEntry: (
      callId: string,
      request: HttpCallRequest
    ) => void;
  };

const mapStateToProps: MapStateToProps<
  StateProps,
  OwnProps,
  RootState
> = state => ({
  isEmpty: getIsHistoryEmpty(state),
  httpCalls: getAllHttpCalls(state),
  tabId: getSelectedTabId(state),
});

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  OwnProps
> = dispatch => ({
  selectHistoryEntryWithTabId: (tabId, id, request) =>
    dispatch(selectHistoryEntry(tabId, id, request)),
});

const mergeProps: MergeProps<
  StateProps,
  DispatchProps,
  OwnProps,
  MergedProps
> = (
  { tabId, ...stateProps },
  { selectHistoryEntryWithTabId, ...dispatchProps },
  ownProps
) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  selectHistoryEntry: (id, request) =>
    selectHistoryEntryWithTabId(tabId, id, request),
});

export type Props = OwnProps & MergedProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(History);
