export type TabView = {
  readonly id: string;
  readonly method: string;
  readonly url: string;
  readonly isActive: boolean;
};
