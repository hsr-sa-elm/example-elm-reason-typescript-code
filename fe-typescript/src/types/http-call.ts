export type HttpResponseHeader = {
  readonly key: string;
  readonly value: string;
};

export type HttpRequestHeader = {
  readonly id: string;
  readonly key: string;
  readonly value: string;
};

export type HttpCallRequest = {
  readonly request: {
    readonly method: string;
    readonly url: string;
  };
  readonly headers: readonly HttpRequestHeader[];
  readonly body: string;
};

export type HttpCallResponse = {
  readonly status: {
    readonly code: number;
    readonly message: string;
  };
  readonly headers: readonly HttpResponseHeader[];
  readonly body: string;
};

export type PerformedHttpCall = {
  readonly id: string;
  readonly request: HttpCallRequest;
  readonly response: HttpCallResponse;
  readonly requestTime: string;
  readonly responseTime: string;
};

export type PerformedHttpCallViewModel = PerformedHttpCall & {
  readonly isSelected: boolean;
};
