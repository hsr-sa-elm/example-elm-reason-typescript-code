import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import { rootReducer, rootSaga } from './store';
import createSagaMiddleware from 'redux-saga';
import './styles/main.scss';
import {
  autoInit,
  drawer,
  list,
  ripple,
  select,
  switchControl,
  textField,
} from 'material-components-web';
import { loadState } from './store/persistence/helper';
import AppContainer from './components/app/AppContainer';

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  loadState(),
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root')
);

sagaMiddleware.run(rootSaga);

autoInit.deregisterAll();
autoInit.register('MDCDrawer', drawer.MDCDrawer as any);
autoInit.register('MDCTextField', textField.MDCTextField as any);
autoInit.register('MDCRipple', ripple.MDCRipple as any);
autoInit.register('MDCList', list.MDCList as any);
autoInit.register('MDCSelect', select.MDCSelect as any);
autoInit.register('MDCSwitch', switchControl.MDCSwitch as any);

const init = () => {
  autoInit();
};
init();
new MutationObserver(init).observe(
  document.querySelector('.main-wrapper') as any,
  { childList: true }
);

const drawerContainer: any = document.querySelector('.mdc-drawer');

const mdcDrawer: any = drawerContainer.MDCDrawer;

const closeDrawer = () => {
  mdcDrawer.open = false;
};

document.querySelector('.sidebar-toggle')!.addEventListener('click', () => {
  mdcDrawer.open = true;

  document
    .querySelectorAll('.mdc-drawer .mdc-list-item, .mdc-drawer button')
    .forEach((el: any) => {
      el.removeEventListener('click', closeDrawer);
      el.addEventListener('click', closeDrawer);
    });
});
