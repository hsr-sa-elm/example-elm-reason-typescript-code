import { HttpCallResponse, HttpResponseHeader } from '../../types/http-call';

const json = require('format-json');

export const getFormattedBody = (response: HttpCallResponse) => {
  const hasHeader = response.headers.some((header: HttpResponseHeader) => {
    return (
      header.key === 'Content-Type' && header.value.match(/application\/json/)
    );
  });
  if (!hasHeader) {
    return response.body;
  }
  try {
    const parsedBody = JSON.parse(response.body);
    return json.diffy(parsedBody);
  } catch (e) {
    console.log(e);
    return response.body;
  }
};
