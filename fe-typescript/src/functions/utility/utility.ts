import { complement, filter, isNil, map, pipe, reduce } from 'ramda';

export const getErrorMessage = (error: any) =>
  typeof error === 'object' && typeof error.message === 'string'
    ? error.message
    : typeof error === 'string'
    ? error
    : String(JSON.stringify(error));

export const getNextId = <T>(
  prefix: string,
  getId: (entity: T) => string
): ((existingEntities: readonly T[]) => string) =>
  pipe(
    map(
      pipe(
        getId,
        id =>
          id.startsWith(prefix) ? parseInt(id.substr(prefix.length), 10) : NaN
      )
    ),
    filter(complement(isNaN)),
    reduce(Math.max, 0),
    id => prefix + (id + 1)
  ) as ((existingEntities: readonly T[]) => string);

export const getCurrentISOTimeStamp = () => new Date().toISOString();

export const mapIfNotNil = <T, R>(f: (t: T) => R) => (
  t: T | undefined | null
): R | undefined => (isNil(t) ? undefined : f(t));
