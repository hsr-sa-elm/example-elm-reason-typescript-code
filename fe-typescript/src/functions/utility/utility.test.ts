import { assert } from '../../test-utility/assert';
import { getErrorMessage, getNextId } from './utility';

describe('getErrorMessage()', () => {
  assert({
    given: 'an error object',
    should: 'return the containing message',
    actual: () => getErrorMessage(new Error('foo')),
    expected: 'foo',
  });

  assert({
    given: 'a string',
    should: 'return the string',
    actual: () => getErrorMessage('bar'),
    expected: 'bar',
  });

  assert({
    given: 'an object',
    should: 'return the serialized object',
    actual: () => getErrorMessage({ a: 'b' }),
    expected: '{"a":"b"}',
  });

  assert({
    given: 'undefined',
    should: 'return the string representation of undefined',
    actual: () => getErrorMessage(undefined),
    expected: 'undefined',
  });
});

describe('getNextId()', () => {
  const getNextIdForStrings = getNextId('prefix-', (x: string) => x);

  assert({
    given: 'no existing entities',
    should: 'return the first id',
    actual: () => getNextIdForStrings([]),
    expected: 'prefix-1',
  });

  assert({
    given: 'some existing entities',
    should: 'return the next free id',
    actual: () => getNextIdForStrings(['prefix-3', 'prefix-1', 'prefix-7']),
    expected: 'prefix-8',
  });

  assert({
    given: 'some existing entities with incorrect formatted ids',
    should: 'return the next free id ignoring the incorrect ids',
    actual: () =>
      getNextIdForStrings([
        'asdf-3',
        '123',
        'prefix-prefix-4',
        'prefix-abc',
        'prefix-1',
        'abc',
        'suffix-3',
      ]),
    expected: 'prefix-2',
  });
});
