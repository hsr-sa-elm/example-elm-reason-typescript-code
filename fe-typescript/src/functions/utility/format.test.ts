import { createHttpCallResponse } from '../../test-utility/factories/types';
import { assert } from '../../test-utility/assert';
import { getFormattedBody } from './format';

describe('format.getFormattedBody', () => {
  const exampleResponse = createHttpCallResponse({
    headers: [{ key: 'Content-Type', value: 'application/json' }],
    body:
      '{"headers":[{"key":"Content-Type","value":"application/json"}],"body":""}',
  });

  assert({
    given: 'An application/json Response',
    should: 'format the JSON Body',
    actual: () => getFormattedBody(exampleResponse),
    expected:
      '{ "headers":\n' +
      '  [ { "key": "Content-Type"\n' +
      '    , "value": "application/json"\n' +
      '    }\n' +
      '  ]\n' +
      ', "body": ""\n' +
      '}',
  });
});
