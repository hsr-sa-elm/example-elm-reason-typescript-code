const Runtime$Bisect = require('bisect_ppx/src/runtime/bucklescript/runtime.js');
Runtime$Bisect.write_coverage_data_on_exit(/* () */ 0);

const noop = () => {};
global.describe = (_, cb) => cb();
global.test = (_, cb) => cb();
global.beforeEach = noop;

const expectation = {
  toEqual: noop,
  toThrow: noop,
};
expectation.not = expectation;
global.expect = () => expectation;
global.expect.extend = noop;

global.jest = {
  fn: () => {
    const mockFn = () => {};
    mockFn.mock = {
      calls: [],
    };
    return mockFn;
  },
};

const { JSDOM } = require('jsdom');
const { window } = new JSDOM('<!DOCTYPE html>');
global.window = window;
global.document = window.document;
global.navigator = window.navigator;

const fs = require('fs');
const loadTestsRecursively = dir => {
  fs.readdirSync(dir).forEach(file => {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      loadTestsRecursively(dir + '/' + file);
    } else if (file.endsWith('.bs.js')) {
      require(dir + '/' + file);
    }
  });
};

loadTestsRecursively(__dirname + '/../__tests__');
