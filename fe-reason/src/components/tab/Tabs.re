open ReactUtil;
open TabState;

[@react.component]
let make =
    (
      ~tabs: list(tabView),
      ~removeTab: string => unit,
      ~activateTab: string => unit,
      ~addTab: unit => unit,
    ) =>
  <div>
    {withAutoInit(
       "MDCList",
       <ul className="mdc-list mdc-list--two-line" role="listbox">
         {mapToElements(
            (tab: tabView) =>
              <li
                key={tab.id}
                onClick={_ => activateTab(tab.id)}
                className={
                  "mdc-list-item"
                  ++ (tab.isActive ? " mdc-list-item--selected" : "")
                }
                role="option"
                ariaSelected={tab.isActive}>
                <span className="mdc-list-item__text">
                  <span className="mdc-list-item__primary-text">
                    {s(tab.method)}
                  </span>
                  <span className="mdc-list-item__secondary-text">
                    {s(tab.url == "" ? "No URL" : tab.url)}
                  </span>
                </span>
                <span className="mdc-list-item__meta">
                  <button
                    className="mdc-icon-button material-icons"
                    onClick={_event => {
                      stopPropagation(_event);
                      removeTab(tab.id);
                    }}>
                    {s("remove")}
                  </button>
                </span>
              </li>,
            tabs,
          )}
       </ul>,
     )}
    {withAutoInit(
       "MDCRipple",
       <button
         className="mdc-button mdc-button--unelevated" onClick={_ => addTab()}>
         <i className="material-icons"> {s("add")} </i>
         <span className="mdc-button__label"> {s("New Tab")} </span>
       </button>,
     )}
  </div>;
