open Store;
open RootSelector.Tab;
open TabActions;

[@react.component]
let make = () => {
  let tabs = useSelector(getAllTabs);
  let dispatch = useDispatch();

  <Tabs
    tabs
    removeTab={id => dispatch(RemoveTab(id))}
    activateTab={id => dispatch(ActivateTab(id))}
    addTab={() => dispatch(AddTab)}
  />;
};
