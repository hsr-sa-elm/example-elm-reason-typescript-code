open Store;
open RootSelector.HttpCall;
open RootSelector.History;

[@react.component]
let make = () => {
  let response = useSelector(getSelectedResponse);
  let responseState = useSelector(getResponseState);
  let bodyFormatted = useSelector(getResponseBodyFormatted);

  <Response responseState response bodyFormatted />;
};
