open ReactUtil;
open HttpCallState;
open HttpTypes;

[@react.component]
let make =
    (
      ~responseState: responseState,
      ~response: option(httpCallResponse),
      ~bodyFormatted: string,
    ) =>
  <div className="response">
    <h2 className="mdc-typography--headline5"> {s("Response")} </h2>
    {switch (responseState, response) {
     | (ShowSelectedResponse, Some({status, headers, _})) =>
       <>
         <p className="response__status">
           {s(
              status.message
              ++ " (Code: "
              ++ string_of_int(status.code)
              ++ ")",
            )}
         </p>
         <div className="header-list">
           {mapToElements(
              ({key, value}: httpResponseHeader) =>
                <React.Fragment key>
                  <div className="header-list__header-name"> {s(key)} </div>
                  <div className="header-list__header-value">
                    {s(value)}
                  </div>
                </React.Fragment>,
              headers,
            )}
         </div>
         <p className="response__body"> {s(bodyFormatted)} </p>
       </>
     | (ShowSelectedResponse, None) =>
       <p className="message">
         {s("Please make a request first to see a response")}
       </p>
     | (RequestFailed(errorMessage), _) =>
       <p className="message">
         {s("HTTP request failed with message: " ++ errorMessage)}
       </p>
     | (Loading, _) => <p className="message"> {s("Loading...")} </p>
     }}
  </div>;
