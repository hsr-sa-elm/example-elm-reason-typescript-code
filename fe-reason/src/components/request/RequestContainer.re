open Store;
open RootSelector.AppView;
open RootSelector.RequestView;
open RootSelector.Tab;
open RootSelector.History;
open RequestViewActions;
open HttpCallActions;
open HistoryActions;
open TabActions;

[@react.component]
let make = () => {
  let tabId = useSelector(getSelectedTabId);
  let optionRequest = useSelector(getRequestToEdit);
  let editState = useSelector(getEditState);
  let isProViewVisible = useSelector(getIsProViewVisible);
  let hasNewerHistoryEntries = useSelector(getHasNewerHistoryEntries);
  let hasOlderHistoryEntries = useSelector(getHasOlderHistoryEntries);
  let dispatch = useDispatch();

  switch (optionRequest) {
  | Some(request) =>
    <Request
      request
      editState
      isProViewVisible
      hasNewerHistoryEntries
      hasOlderHistoryEntries
      setMethod={newMethod =>
        dispatch(TabAction(tabId, SetMethod(newMethod)))
      }
      setUrl={newUrl => dispatch(TabAction(tabId, SetUrl(newUrl)))}
      setBody={newBody => dispatch(TabAction(tabId, SetBody(newBody)))}
      removeHeader={id => dispatch(TabAction(tabId, RemoveHeader(id)))}
      setHeaderValue={(id, newValue) =>
        dispatch(TabAction(tabId, SetHeaderValue({id, newValue})))
      }
      setHeaderKey={(id, newKey) =>
        dispatch(TabAction(tabId, SetHeaderKey({id, newKey})))
      }
      send={() => dispatch(TabAction(tabId, DoCall(request)))}
      revertEditState={oldRequest =>
        dispatch(TabAction(tabId, RevertEditState(oldRequest)))
      }
      selectNextNewerHistoryEntry={() =>
        dispatch(TabAction(tabId, SelectNextNewerHistoryEntry))
      }
      selectNextOlderHistoryEntry={() =>
        dispatch(TabAction(tabId, SelectNextOlderHistoryEntry))
      }
    />
  | None => ReasonReact.null
  };
};
