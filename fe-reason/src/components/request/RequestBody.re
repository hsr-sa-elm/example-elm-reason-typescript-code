open ReactUtil;

[@react.component]
let make = (~body: string, ~setBody: string => unit) =>
  <div className="request-body">
    {withAutoInit(
       "MDCTextField",
       <div
         className="mdc-text-field mdc-text-field--textarea mdc-text-field--fullwidth">
         <textarea
           value=body
           id="request-body"
           className="mdc-text-field__input"
           onChange={_event => setBody(getValue(_event))}
         />
         <div className="mdc-notched-outline">
           <div className="mdc-notched-outline__leading" />
           <div className="mdc-notched-outline__notch">
             <label className="mdc-floating-label" htmlFor="request-body">
               {s("Body")}
             </label>
           </div>
           <div className="mdc-notched-outline__trailing" />
         </div>
       </div>,
     )}
  </div>;
