open ReactUtil;
open HttpTypes;

[@react.component]
let make =
    (
      ~headers: list(httpRequestHeader),
      ~setHeaderKey: (string, string) => unit,
      ~setHeaderValue: (string, string) => unit,
      ~removeHeader: string => unit,
    ) =>
  <div className="request-headers">
    <h3 className="mdc-typography--headline6"> {s("Headers")} </h3>
    {mapToElementsWithIndex(
       (index, {id, key, value}: httpRequestHeader) =>
         <React.Fragment key=id>
           {withAutoInit(
              "MDCTextField",
              <div
                className="mdc-text-field mdc-text-field--outlined mdc-text-field--no-label">
                <input
                  value=key
                  onChange={_event => setHeaderKey(id, getValue(_event))}
                  type_="text"
                  id={"header-key-" ++ id}
                  className="mdc-text-field__input"
                  ariaLabel="Name"
                  placeholder="Name"
                />
                <div className="mdc-notched-outline">
                  <div className="mdc-notched-outline__leading" />
                  <div className="mdc-notched-outline__trailing" />
                </div>
              </div>,
            )}
           {withAutoInit(
              "MDCTextField",
              <div
                className="mdc-text-field mdc-text-field--outlined mdc-text-field--no-label">
                <input
                  value
                  onChange={_event => setHeaderValue(id, getValue(_event))}
                  type_="text"
                  id={"header-value-" ++ id}
                  className="mdc-text-field__input"
                  ariaLabel="Value"
                  placeholder="Value"
                />
                <div className="mdc-notched-outline">
                  <div className="mdc-notched-outline__leading" />
                  <div className="mdc-notched-outline__trailing" />
                </div>
              </div>,
            )}
           {index < List.length(headers) - 1
              ? <button
                  className="mdc-icon-button material-icons"
                  onClick={_ => removeHeader(id)}>
                  {s("remove")}
                </button>
              : ReasonReact.null}
         </React.Fragment>,
       headers,
     )}
  </div>;
