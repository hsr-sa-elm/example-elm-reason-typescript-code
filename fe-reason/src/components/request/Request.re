open ReactUtil;
open HttpTypes;
open RequestViewState;

[@react.component]
let make =
    (
      ~request: httpCallRequest,
      ~editState: editState,
      ~isProViewVisible: bool,
      ~hasNewerHistoryEntries: bool,
      ~hasOlderHistoryEntries: bool,
      ~setMethod: string => unit,
      ~setUrl: string => unit,
      ~setBody: string => unit,
      ~setHeaderKey: (string, string) => unit,
      ~setHeaderValue: (string, string) => unit,
      ~removeHeader: string => unit,
      ~send: unit => unit,
      ~revertEditState: httpCallRequest => unit,
      ~selectNextNewerHistoryEntry: unit => unit,
      ~selectNextOlderHistoryEntry: unit => unit,
    ) =>
  <div className="request">
    <h2 className="mdc-typography--headline5">
      {s("Request" ++ (editState == Changed ? "*" : ""))}
    </h2>
    {!isProViewVisible
       ? <div className="history-buttons">
           <button
             className="mdc-icon-button material-icons"
             onClick={_ => selectNextOlderHistoryEntry()}
             disabled={!hasOlderHistoryEntries}>
             {s("chevron_left")}
           </button>
           <button
             className="mdc-icon-button material-icons"
             onClick={_ => selectNextNewerHistoryEntry()}
             disabled={!hasNewerHistoryEntries}>
             {s("chevron_right")}
           </button>
         </div>
       : ReasonReact.null}
    {withAutoInit(
       "MDCSelect",
       <div className="mdc-select mdc-select--outlined">
         <i className="mdc-select__dropdown-icon" />
         <select
           id="method"
           className="mdc-select__native-control"
           onChange={_event => setMethod(getValue(_event))}
           value={request.request.method}>
           <option value="GET"> {s("GET")} </option>
           <option value="POST"> {s("POST")} </option>
           <option value="PUT"> {s("PUT")} </option>
           <option value="PATCH"> {s("PATCH")} </option>
           <option value="DELETE"> {s("DELETE")} </option>
           <option value="HEAD"> {s("HEAD")} </option>
           <option value="CONNECT"> {s("CONNECT")} </option>
           <option value="TRACE"> {s("TRACE")} </option>
           <option value="OPTIONS"> {s("OPTIONS")} </option>
         </select>
         <div className="mdc-notched-outline">
           <div className="mdc-notched-outline__leading" />
           <div className="mdc-notched-outline__notch">
             <label
               className="mdc-floating-label mdc-floating-label--float-above"
               htmlFor="method">
               {s("Method")}
             </label>
           </div>
           <div className="mdc-notched-outline__trailing" />
         </div>
       </div>,
     )}
    {withAutoInit(
       "MDCTextField",
       <div className="mdc-text-field mdc-text-field--outlined request-url">
         <input
           value={request.request.url}
           onChange={_event => setUrl(getValue(_event))}
           type_="text"
           id="url"
           className="mdc-text-field__input"
         />
         <div className="mdc-notched-outline">
           <div className="mdc-notched-outline__leading" />
           <div className="mdc-notched-outline__notch">
             <label className="mdc-floating-label" htmlFor="url">
               {s("URL")}
             </label>
           </div>
           <div className="mdc-notched-outline__trailing" />
         </div>
       </div>,
     )}
    <RequestHeaders
      headers={request.headers}
      setHeaderKey
      setHeaderValue
      removeHeader
    />
    <RequestBody body={request.body} setBody />
    {withAutoInit(
       "MDCRipple",
       <button
         onClick={_event => send()} className="mdc-button mdc-button--raised">
         <span className="mdc-button__label"> {s("Send")} </span>
       </button>,
     )}
    {switch (editState) {
     | Overwritten(oldRequest) =>
       <div className="mdc-snackbar mdc-snackbar--open">
         <div className="mdc-snackbar__surface">
           <div className="mdc-snackbar__label">
             {s(
                "The content of the request has been overwritten with the selected request of the history.",
              )}
           </div>
           <div className="mdc-snackbar__actions">
             <button
               onClick={_event => revertEditState(oldRequest)}
               className="mdc-button mdc-snackbar__action">
               {s("Undo")}
             </button>
           </div>
         </div>
       </div>
     | Changed
     | Unchanged => ReasonReact.null
     }}
  </div>;
