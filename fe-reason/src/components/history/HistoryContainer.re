open Store;
open RootSelector.History;
open RootSelector.Tab;
open HistoryActions;
open TabActions;

[@react.component]
let make = () => {
  let tabId = useSelector(getSelectedTabId);
  let isEmpty = useSelector(getIsEmpty);
  let httpCalls = useSelector(getAllHttpCallsForView);
  let dispatch = useDispatch();

  <History
    isEmpty
    httpCalls
    selectHistoryEntry={call =>
      dispatch(TabAction(tabId, SelectHistoryEntry(call)))
    }
  />;
};
