open ReactUtil;
open DateUtil;
open HttpTypes;

[@react.component]
let make =
    (
      ~isEmpty: bool,
      ~httpCalls: list(performedHttpCallViewModel),
      ~selectHistoryEntry: performedHttpCall => unit,
    ) =>
  <div className="history">
    <h2 className="mdc-typography--headline5"> {s("History")} </h2>
    {isEmpty
       ? <p className="mdc-typography--body1">
           {s("The history is empty. Make some requests!")}
         </p>
       : withAutoInit(
           "MDCList",
           <ul className="mdc-list mdc-list--two-line" role="listbox">
             {mapToElements(
                ({performedHttpCall, isSelected}) =>
                  <li
                    key={performedHttpCall.id}
                    onClick={_event => selectHistoryEntry(performedHttpCall)}
                    className={
                      "mdc-list-item"
                      ++ (isSelected ? " mdc-list-item--selected" : "")
                    }
                    role="option"
                    ariaSelected=isSelected>
                    <span className="mdc-list-item__text">
                      <span className="mdc-list-item__primary-text">
                        {s(
                           performedHttpCall.request.request.method
                           ++ " "
                           ++ performedHttpCall.request.request.url
                           ++ " ("
                           ++ performedHttpCall.response.status.message
                           ++ ")",
                         )}
                      </span>
                      <span className="mdc-list-item__secondary-text">
                        {s(
                           toDateTimeString(performedHttpCall.responseTime)
                           ++ " (Duration: "
                           ++ Js.Float.toString(
                                performedHttpCall.responseTime
                                -. performedHttpCall.requestTime,
                              )
                           ++ "ms)",
                         )}
                      </span>
                    </span>
                  </li>,
                httpCalls,
              )}
           </ul>,
         )}
  </div>;
