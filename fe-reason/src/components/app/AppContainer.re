open Store;
open RootSelector.AppView;
open AppViewActions;

[@react.component]
let make = () => {
  let isProViewVisible = useSelector(getIsProViewVisible);
  let dispatch = useDispatch();

  <App
    isProViewVisible
    toggleProViewVisibility={() => dispatch(ToggleProView)}
  />;
};
