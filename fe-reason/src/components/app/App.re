open ReactUtil;

[@react.component]
let make = (~isProViewVisible: bool, ~toggleProViewVisibility: unit => unit) =>
  <div
    className={"root-container" ++ (!isProViewVisible ? " simple-view" : "")}>
    <header className="mdc-top-app-bar">
      <div className="mdc-top-app-bar__row">
        <section
          className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
          <button
            className="sidebar-toggle material-icons mdc-top-app-bar__navigation-icon mdc-icon-button">
            {s("menu")}
          </button>
          <span className="mdc-top-app-bar__title">
            {s("HTTP client in Reason")}
          </span>
        </section>
        <section
          className="mdc-top-app-bar__section mdc-top-app-bar__section--align-end">
          {withAutoInit(
             "MDCSwitch",
             <div
               className={
                 "mdc-switch"
                 ++ (isProViewVisible ? " mdc-switch--checked" : "")
               }>
               <div className="mdc-switch__track" />
               <div className="mdc-switch__thumb-underlay">
                 <div className="mdc-switch__thumb">
                   <input
                     id="view-switch"
                     type_="checkbox"
                     className="mdc-switch__native-control"
                     role="switch"
                     checked=isProViewVisible
                     onChange={_ => toggleProViewVisibility()}
                   />
                 </div>
               </div>
             </div>,
           )}
          <label htmlFor="view-switch"> {s("Pro view")} </label>
        </section>
      </div>
    </header>
    {withAutoInit(
       "MDCDrawer",
       <aside className="mdc-drawer mdc-drawer--modal">
         <div className="mdc-drawer__content"> <TabsContainer /> </div>
       </aside>,
     )}
    <div className="mdc-drawer-scrim" />
    <div className="main-wrapper">
      {isProViewVisible ? <aside> <TabsContainer /> </aside> : ReasonReact.null}
      <ContentContainer />
    </div>
  </div>;
