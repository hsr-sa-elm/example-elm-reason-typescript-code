open ReactUtil;

[@react.component]
let make = (~hasCurrentTabData: bool, ~isProViewVisible: bool) =>
  hasCurrentTabData
    ? <main>
        <RequestContainer />
        <ResponseContainer />
        {isProViewVisible ? <HistoryContainer /> : ReasonReact.null}
      </main>
    : <p className="no-tab-message"> {s("No tab selected")} </p>;
