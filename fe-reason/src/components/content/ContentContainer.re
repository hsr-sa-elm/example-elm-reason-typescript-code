open Store;
open RootSelector.Tab;
open RootSelector.AppView;

[@react.component]
let make = () => {
  let hasCurrentTabData = useSelector(getHasCurrentTabData);
  let isProViewVisible = useSelector(getIsProViewVisible);

  <Content hasCurrentTabData isProViewVisible />;
};
