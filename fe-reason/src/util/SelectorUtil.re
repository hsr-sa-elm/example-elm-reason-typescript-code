let getSelector1 =
    (selector1: 'rootState => 'a, combiner: 'a => 'z, rootState: 'rootState)
    : 'z =>
  combiner(selector1(rootState));

let getSelector2 =
    (
      selector1: 'rootState => 'a,
      selector2: 'rootState => 'b,
      combiner: ('a, 'b) => 'z,
      rootState: 'rootState,
    )
    : 'z =>
  combiner(selector1(rootState), selector2(rootState));

let getSelector3 =
    (
      selector1: 'rootState => 'a,
      selector2: 'rootState => 'b,
      selector3: 'rootState => 'c,
      combiner: ('a, 'b, 'c) => 'z,
      rootState: 'rootState,
    )
    : 'z =>
  combiner(
    selector1(rootState),
    selector2(rootState),
    selector3(rootState),
  );

let getSelector4 =
    (
      selector1: 'rootState => 'a,
      selector2: 'rootState => 'b,
      selector3: 'rootState => 'c,
      selector4: 'rootState => 'd,
      combiner: ('a, 'b, 'c, 'd) => 'z,
      rootState: 'rootState,
    )
    : 'z =>
  combiner(
    selector1(rootState),
    selector2(rootState),
    selector3(rootState),
    selector4(rootState),
  );

let getSelector5 =
    (
      selector1: 'rootState => 'a,
      selector2: 'rootState => 'b,
      selector3: 'rootState => 'c,
      selector4: 'rootState => 'd,
      selector5: 'rootState => 'd,
      combiner: ('a, 'b, 'c, 'd, 'e) => 'z,
      rootState: 'rootState,
    )
    : 'z =>
  combiner(
    selector1(rootState),
    selector2(rootState),
    selector3(rootState),
    selector4(rootState),
    selector5(rootState),
  );
