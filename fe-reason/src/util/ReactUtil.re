open FunctionUtil;

let getValue = event => ReactEvent.Form.target(event)##value;

let stopPropagation = event => ReactEvent.Mouse.stopPropagation(event);

let s = ReasonReact.string;

let renderList = Array.of_list >> ReasonReact.array;
let mapToElements = (f, list) => List.map(f, list) |> renderList;
let mapToElementsWithIndex = (f, list) => List.mapi(f, list) |> renderList;

let withAutoInit = (autoInitName: string, element: ReasonReact.reactElement) =>
  ReasonReact.cloneElement(
    element,
    ~props=
      Obj.magic(Js.Dict.fromList([("data-mdc-auto-init", autoInitName)])),
    [||],
  );
