let map: ('a => 'b, option('a)) => option('b) =
  (f, value) =>
    switch (value) {
    | Some(innerValue) => Some(f(innerValue))
    | None => None
    };

let flatMap: ('a => option('b), option('a)) => option('b) =
  (f, value) =>
    switch (value) {
    | Some(innerValue) => f(innerValue)
    | None => None
    };

let withDefault: ('a, option('a)) => 'a =
  (defaultValue, value) =>
    switch (value) {
    | Some(innerValue) => innerValue
    | None => defaultValue
    };
