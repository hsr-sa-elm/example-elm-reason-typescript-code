open FunctionUtil;

let toMonthName: float => string =
  month =>
    switch (month) {
    | 0. => "January"
    | 1. => "February"
    | 2. => "March"
    | 3. => "April"
    | 4. => "May"
    | 5. => "June"
    | 6. => "July"
    | 7. => "August"
    | 8. => "September"
    | 9. => "October"
    | 10. => "November"
    | 11. => "December"
    | _ => "Unknown"
    };

let toTwoDigitString: float => string =
  Js.Float.toString
  >> (
    intString => String.length(intString) == 1 ? "0" ++ intString : intString
  );

let toDateTimeString: float => string =
  Js.Date.fromFloat
  >> (
    date => {
      let year = Js.Float.toString(Js.Date.getFullYear(date));
      let month = toMonthName(Js.Date.getMonth(date));
      let day = toTwoDigitString(Js.Date.getDate(date));
      let hour = toTwoDigitString(Js.Date.getHours(date));
      let minute = toTwoDigitString(Js.Date.getMinutes(date));
      let second = toTwoDigitString(Js.Date.getSeconds(date));

      day
      ++ ". "
      ++ month
      ++ " "
      ++ year
      ++ " "
      ++ hour
      ++ ":"
      ++ minute
      ++ ":"
      ++ second;
    }
  );
