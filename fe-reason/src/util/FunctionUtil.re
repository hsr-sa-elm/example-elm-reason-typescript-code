let pipe = (f, g, x) => g(f(x));
let (>>) = pipe;

let identity = x => x;
let always = (x, _) => x;
