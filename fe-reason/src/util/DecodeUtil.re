let decodeHttpResponse = (json): HttpTypes.httpCallResponse => {
  let decodeStatus = (json): HttpTypes.httpCallResponseStatus => {
    Json.Decode.{
      code: json |> field("code", int),
      message: json |> field("message", string),
    };
  };
  let decodeHeader = (json): HttpTypes.httpResponseHeader => {
    Json.Decode.{
      key: json |> field("key", string),
      value: json |> withDefault("", field("value", string)),
    };
  };
  Json.Decode.{
    HttpTypes.body: json |> field("body", string),
    HttpTypes.status: json |> field("status", decodeStatus),
    HttpTypes.headers: json |> field("headers", list(decodeHeader)),
  };
};

let decodeHttpResultFailure = (json): HttpTypes.httpCallResultFailure => {
  Json.Decode.{HttpTypes.message: json |> field("message", string)};
};

let decodeHttpResult =
    (json)
    : CommonTypes.result(
        HttpTypes.httpCallResponse,
        HttpTypes.httpCallResultFailure,
      ) =>
  try (Ok(decodeHttpResponse(json))) {
  | _ => Error(decodeHttpResultFailure(json))
  };

let decodeHttpRequest = (json): HttpTypes.httpCallRequest => {
  let decodeRequestLine = (json): HttpTypes.httpCallRequestRequest => {
    Json.Decode.{
      method: json |> field("method", string),
      url: json |> field("url", string),
    };
  };
  let decodeHeader = (json): HttpTypes.httpRequestHeader => {
    Json.Decode.{
      id: json |> field("id", string),
      key: json |> field("key", string),
      value: json |> field("value", string),
    };
  };
  Json.Decode.{
    request: json |> field("request", decodeRequestLine),
    body: json |> field("body", string),
    headers: json |> field("headers", list(decodeHeader)),
  };
};

let decodePerformedHttpCall = (json): HttpTypes.performedHttpCall =>
  Json.Decode.{
    id: json |> field("id", string),
    request: json |> field("request", decodeHttpRequest),
    response: json |> field("response", decodeHttpResponse),
    requestTime: json |> field("requestTime", float),
    responseTime: json |> field("responseTime", float),
  };

let decodeHistoryState = (json): HistoryState.state =>
  Json.Decode.{
    selected: json |> field("selected", optional(decodePerformedHttpCall)),
    newer: json |> field("newer", list(decodePerformedHttpCall)),
    older: json |> field("older", list(decodePerformedHttpCall)),
  };

let decodeRequestViewState = (json): RequestViewState.state =>
  Json.Decode.{
    editState:
      json
      |> field(
           "editState",
           andThen(
             status =>
               RequestViewState.(
                 switch (status) {
                 | "UNCHANGED" => (_ => Unchanged)
                 | "CHANGED" => (_ => Changed)
                 | "OVERWRITTEN" => (
                     subJson =>
                       Overwritten(
                         subJson |> field("request", decodeHttpRequest),
                       )
                   )
                 | _ => failwith("Unknown edit state")
                 }
               ),
             field("status", string),
           ),
         ),
    requestToEdit: json |> field("requestToEdit", decodeHttpRequest),
  };

let decodeTabState = (json): TabState.tabState =>
  Json.Decode.{
    id: json |> field("id", string),
    tabData: {
      historyState:
        json |> at(["tabData", "historyState"], decodeHistoryState),
      httpCallState: HttpCallReducer.initialState,
      requestViewState:
        json |> at(["tabData", "requestViewState"], decodeRequestViewState),
    },
  };

let decodeTabsState = (json): TabState.state =>
  Json.Decode.{
    selectedTab: json |> field("selectedTab", string),
    tabs: json |> field("tabs", list(decodeTabState)),
  };

let decodeAppViewState = (json): AppViewState.state =>
  Json.Decode.{isProViewVisible: json |> field("isProViewVisible", bool)};

let decodeRootState = (json): RootState.rootState =>
  Json.Decode.{
    appView: json |> field("appView", decodeAppViewState),
    tab: json |> field("tab", decodeTabsState),
  };
