open Dom.Storage;

let (loadState, saveState) = {
  let cacheKey = "reason-app-data";
  let loadState = () =>
    getItem(cacheKey, localStorage)
    |> (
      result =>
        switch (result) {
        | Some(data) =>
          try (Some(DecodeUtil.decodeRootState(Json.parseOrRaise(data)))) {
          | _ => None
          }
        | None => None
        }
    );
  let saveState = (rootState: RootState.rootState) => {
    setItem(
      cacheKey,
      Js.Json.stringify(EncodeUtil.encodeRootState(rootState)),
      localStorage,
    );
  };
  (loadState, saveState);
};
