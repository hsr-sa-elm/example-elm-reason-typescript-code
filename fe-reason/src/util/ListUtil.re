let find: ('a => bool, list('a)) => option('a) =
  (f, elements) =>
    try (Some(List.find(f, elements))) {
    | _ => None
    };
