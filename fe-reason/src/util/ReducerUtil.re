open FunctionUtil;

let convertIdStringToInt = (prefix: string, idString: string) =>
  try (
    Some(
      int_of_string(
        String.sub(
          idString,
          String.length(prefix),
          String.length(idString) - String.length(prefix),
        ),
      ),
    )
  ) {
  | _ => None
  };

let getNextId: (string, 'a => string, list('a)) => string =
  (prefix, getId) =>
    List.map(getId >> convertIdStringToInt(prefix))
    >> List.fold_left(
         (highestId, currentId) =>
           switch (currentId) {
           | Some(id) => id > highestId ? id : highestId
           | None => highestId
           },
         0,
       )
    >> (id => prefix ++ string_of_int(id + 1));
