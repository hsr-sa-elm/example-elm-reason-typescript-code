type parsedBody;

[@bs.module "format-json"] external format: parsedBody => string = "diffy";

[@bs.scope "JSON"] [@bs.val]
external parseBody: string => parsedBody = "parse";

let getFormattedBody = (response: HttpTypes.httpCallResponse): string => {
  let isApplicationJsonHeader = (header: HttpTypes.httpResponseHeader) =>
    header.key == "Content-Type"
    && Js.String.match([%re "/application\/json/"], header.value) != None;
  if (List.exists(isApplicationJsonHeader, response.headers)) {
    try (format(parseBody(response.body))) {
    | _ => response.body
    };
  } else {
    response.body;
  };
};
