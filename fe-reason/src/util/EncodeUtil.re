let encodeHttpRequest = (request: HttpTypes.httpCallRequest) => {
  let headerEncoder = (header: HttpTypes.httpRequestHeader) => {
    Json.Encode.(
      object_([
        ("id", string(header.id)),
        ("key", string(header.key)),
        ("value", string(header.value)),
      ])
    );
  };

  Json.Encode.(
    object_([
      (
        "request",
        object_([
          ("url", string(request.request.url)),
          ("method", string(request.request.method)),
        ]),
      ),
      (
        "headers",
        request.headers
        |> List.map(headerEncoder)
        |> Array.of_list
        |> jsonArray,
      ),
      ("body", string(request.body)),
    ])
  );
};

let encodeHttpResponse = (response: HttpTypes.httpCallResponse) => {
  let headerEncoder = (header: HttpTypes.httpResponseHeader) => {
    Json.Encode.(
      object_([
        ("key", string(header.key)),
        ("value", string(header.value)),
      ])
    );
  };

  Json.Encode.(
    object_([
      (
        "status",
        object_([
          ("code", int(response.status.code)),
          ("message", string(response.status.message)),
        ]),
      ),
      (
        "headers",
        response.headers
        |> List.map(headerEncoder)
        |> Array.of_list
        |> jsonArray,
      ),
      ("body", string(response.body)),
    ])
  );
};

let encodePerformedHttpCall = (call: HttpTypes.performedHttpCall) =>
  Json.Encode.(
    object_([
      ("id", string(call.id)),
      ("request", encodeHttpRequest(call.request)),
      ("response", encodeHttpResponse(call.response)),
      ("requestTime", float(call.requestTime)),
      ("responseTime", float(call.responseTime)),
    ])
  );

let encodeHistoryState = (historyState: HistoryState.state) =>
  Json.Encode.(
    object_([
      ("selected", nullable(encodePerformedHttpCall, historyState.selected)),
      (
        "newer",
        historyState.newer
        |> List.map(encodePerformedHttpCall)
        |> Array.of_list
        |> jsonArray,
      ),
      (
        "older",
        historyState.older
        |> List.map(encodePerformedHttpCall)
        |> Array.of_list
        |> jsonArray,
      ),
    ])
  );

let encodeRequestViewState = (requestViewState: RequestViewState.state) =>
  Json.Encode.(
    object_([
      RequestViewState.(
        "editState",
        object_(
          switch (requestViewState.editState) {
          | Unchanged => [("status", string("UNCHANGED"))]
          | Changed => [("status", string("CHANGED"))]
          | Overwritten(request) => [
              ("status", string("OVERWRITTEN")),
              ("request", encodeHttpRequest(request)),
            ]
          },
        ),
      ),
      ("requestToEdit", encodeHttpRequest(requestViewState.requestToEdit)),
    ])
  );

let encodeTabState = (tabState: TabState.tabState) =>
  Json.Encode.(
    object_([
      ("id", string(tabState.id)),
      (
        "tabData",
        object_([
          (
            "historyState",
            encodeHistoryState(tabState.tabData.historyState),
          ),
          (
            "requestViewState",
            encodeRequestViewState(tabState.tabData.requestViewState),
          ),
        ]),
      ),
    ])
  );

let encodeTabsState = (tabsState: TabState.state) =>
  Json.Encode.(
    object_([
      (
        "tabs",
        tabsState.tabs
        |> List.map(encodeTabState)
        |> Array.of_list
        |> jsonArray,
      ),
      ("selectedTab", string(tabsState.selectedTab)),
    ])
  );

let encodeAppViewState = (appViewState: AppViewState.state) =>
  Json.Encode.(
    object_([("isProViewVisible", bool(appViewState.isProViewVisible))])
  );

let encodeRootState = (rootState: RootState.rootState) =>
  Json.Encode.(
    object_([
      ("appView", encodeAppViewState(rootState.appView)),
      ("tab", encodeTabsState(rootState.tab)),
    ])
  );
