open TabActions;
open HttpTypes;

type newHeaderValueWithId = {
  id: string,
  newValue: string,
};

type newHeaderKeyWithId = {
  id: string,
  newKey: string,
};

type tabAction +=
  | SetMethod(string)
  | SetBody(string)
  | RemoveHeader(string)
  | SetHeaderValue(newHeaderValueWithId)
  | SetHeaderKey(newHeaderKeyWithId)
  | SetUrl(string)
  | RevertEditState(httpCallRequest)
  | SetRequestToEdit(httpCallRequest);
