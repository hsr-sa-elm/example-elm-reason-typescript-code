open HttpTypes;

type editState =
  | Unchanged
  | Changed
  | Overwritten(httpCallRequest);

type state = {
  editState,
  requestToEdit: httpCallRequest,
};
