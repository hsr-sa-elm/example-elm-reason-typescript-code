open HttpCallActions;
open RequestViewActions;
open RequestViewState;

let initialState: state = {
  editState: Unchanged,
  requestToEdit: {
    request: {
      method: "GET",
      url: "",
    },
    headers: [
      {id: "header-0", key: "Content-Type", value: "application/json"},
      {id: "header-1", key: "Cache-Control", value: "no-cache"},
      {id: "header-2", key: "", value: ""},
    ],
    body: "",
  },
};

let getNextId =
  ReducerUtil.getNextId("header-", ({id, _}: HttpTypes.httpRequestHeader) =>
    id
  );

let updateHeaders = (id, update) =>
  List.map((header: HttpTypes.httpRequestHeader) =>
    header.id == id ? update(header) : header
  );

let removeHeader = id =>
  List.filter((header: HttpTypes.httpRequestHeader) => header.id != id);

let addHeaderRowIfLastHeaderRowIsNotEmpty =
    (headers: list(HttpTypes.httpRequestHeader)) =>
  switch (List.nth(headers, List.length(headers) - 1)) {
  | {key: "", value: "", _} => headers
  | _ => headers @ [{id: getNextId(headers), key: "", value: ""}]
  };

let reducer = (state, action) =>
  switch (action) {
  | SetMethod(method) => {
      editState: Changed,
      requestToEdit: {
        ...state.requestToEdit,
        request: {
          ...state.requestToEdit.request,
          method,
        },
      },
    }
  | SetUrl(url) => {
      editState: Changed,
      requestToEdit: {
        ...state.requestToEdit,
        request: {
          ...state.requestToEdit.request,
          url,
        },
      },
    }
  | SetBody(body) => {
      editState: Changed,
      requestToEdit: {
        ...state.requestToEdit,
        body,
      },
    }
  | RemoveHeader(id) => {
      editState: Changed,
      requestToEdit: {
        ...state.requestToEdit,
        headers:
          removeHeader(id, state.requestToEdit.headers)
          |> addHeaderRowIfLastHeaderRowIsNotEmpty,
      },
    }
  | SetHeaderValue({id, newValue}) => {
      editState: Changed,
      requestToEdit: {
        ...state.requestToEdit,
        headers:
          updateHeaders(
            id,
            header => {...header, value: newValue},
            state.requestToEdit.headers,
          )
          |> addHeaderRowIfLastHeaderRowIsNotEmpty,
      },
    }
  | SetHeaderKey({id, newKey}) => {
      editState: Changed,
      requestToEdit: {
        ...state.requestToEdit,
        headers:
          updateHeaders(
            id,
            header => {...header, key: newKey},
            state.requestToEdit.headers,
          )
          |> addHeaderRowIfLastHeaderRowIsNotEmpty,
      },
    }
  | CallSucceeded(_) => {...state, editState: Unchanged}
  | RevertEditState(oldRequest) => {
      editState: Changed,
      requestToEdit: oldRequest,
    }
  | SetRequestToEdit(request) => {
      editState:
        switch (state.editState) {
        | Changed => Overwritten(state.requestToEdit)
        | Overwritten(_)
        | Unchanged => state.editState
        },
      requestToEdit: request,
    }
  | _ => state
  };
