open FunctionUtil;
open RequestViewState;

module type ParentStateOfRequestViewState = {
  type parentState;
  let getRequestViewState: parentState => option(state);
};

module Make = (ParentState: ParentStateOfRequestViewState) => {
  open ParentState;
  let getRequestToEdit =
    getRequestViewState >> OptionUtil.map(state => state.requestToEdit);
  let getEditState =
    getRequestViewState
    >> OptionUtil.map(state => state.editState)
    >> OptionUtil.withDefault(Unchanged);
};
