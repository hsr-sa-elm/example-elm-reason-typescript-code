open FunctionUtil;
open RootState;

module AppView =
  AppViewSelector.Make({
    type parentState = rootState;
    let getAppViewState = state => state.appView;
  });

module Tab =
  TabSelector.Make({
    type parentState = rootState;
    let getTabState = state => state.tab;
  });

module History =
  HistorySelector.Make({
    type parentState = rootState;
    let getHistoryState =
      Tab.getCurrentTabData
      >> OptionUtil.map(({historyState, _}: TabState.tabData) =>
           historyState
         );
  });

module HttpCall =
  HttpCallSelector.Make({
    type parentState = rootState;
    let getHttpCallState =
      Tab.getCurrentTabData
      >> OptionUtil.map(({httpCallState, _}: TabState.tabData) =>
           httpCallState
         );
  });

module RequestView =
  RequestViewSelector.Make({
    type parentState = rootState;
    let getRequestViewState =
      Tab.getCurrentTabData
      >> OptionUtil.map(({requestViewState, _}: TabState.tabData) =>
           requestViewState
         );
  });
