open Reductive.Store;

let sideEffectMiddleware = (store, next, action) => {
  SideEffectsHttpCall.proceed(dispatch(store), action);
  next(action);
  SideEffectsPersistence.proceed(getState(store));
  SideEffectsOverwriteRequest.proceed(
    dispatch(store),
    action,
    getState(store),
  );
};
