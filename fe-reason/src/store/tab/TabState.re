type tabData = {
  historyState: HistoryState.state,
  httpCallState: HttpCallState.state,
  requestViewState: RequestViewState.state,
};

type tabState = {
  id: string,
  tabData,
};

type tabView = {
  id: string,
  method: string,
  url: string,
  isActive: bool,
};

type state = {
  tabs: list(tabState),
  selectedTab: string,
};
