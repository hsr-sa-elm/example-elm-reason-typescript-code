open TabState;
open RootActions;
open TabActions;

let initialTabData: tabData = {
  historyState: HistoryReducer.initialState,
  httpCallState: HttpCallReducer.initialState,
  requestViewState: RequestViewReducer.initialState,
};

let getNextId = ReducerUtil.getNextId("tab-", ({id, _}: tabState) => id);

let initialTabState: tabState = {tabData: initialTabData, id: getNextId([])};

let initialState: state = {
  tabs: [initialTabState],
  selectedTab: getNextId([]),
};

let tabDataReducer = (tab: tabData, action: tabAction) => {
  historyState: HistoryReducer.reducer(tab.historyState, action),
  httpCallState: HttpCallReducer.reducer(tab.httpCallState, action),
  requestViewState: RequestViewReducer.reducer(tab.requestViewState, action),
};

let reducer = (state: state, action: rootAction): state => {
  switch (action) {
  | AddTab =>
    getNextId(state.tabs)
    |> (
      id => {
        selectedTab: id,
        tabs: state.tabs @ [{id, tabData: initialTabData}],
      }
    )
  | ActivateTab(id) => {...state, selectedTab: id}
  | RemoveTab(id) =>
    state.tabs
    |> List.filter((tab: tabState) => tab.id != id)
    |> (
      newTabs => {
        selectedTab:
          state.selectedTab == id
            ? switch (newTabs) {
              | [] => ""
              | [first, ..._] => first.id
              }
            : state.selectedTab,
        tabs: newTabs,
      }
    )
  | TabAction(tabId, tabAction) => {
      ...state,
      tabs:
        List.map(
          ({id, tabData}: tabState) =>
            {
              id,
              tabData:
                id == tabId ? tabDataReducer(tabData, tabAction) : tabData,
            },
          state.tabs,
        ),
    }
  | _ => state
  };
};
