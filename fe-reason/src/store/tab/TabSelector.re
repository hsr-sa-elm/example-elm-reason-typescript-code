open FunctionUtil;
open TabState;

module type ParentStateOfTabState = {
  type parentState;
  let getTabState: parentState => state;
};

module Make = (ParentState: ParentStateOfTabState) => {
  open ParentState;

  let getCurrentTab =
    getTabState
    >> (
      state =>
        ListUtil.find(
          (tab: tabState) => tab.id == state.selectedTab,
          state.tabs,
        )
    );

  let getCurrentTabData =
    getCurrentTab >> OptionUtil.map(state => state.tabData);

  let getHasCurrentTabData = getCurrentTabData >> (tabData => tabData != None);

  let getSelectedTabId = getTabState >> (state => state.selectedTab);

  let getAllTabs =
    getTabState
    >> (
      (state: TabState.state) =>
        List.map(
          (tab: tabState): tabView =>
            {
              id: tab.id,
              method:
                tab.tabData.requestViewState.requestToEdit.request.method,
              url: tab.tabData.requestViewState.requestToEdit.request.url,
              isActive: tab.id == state.selectedTab,
            },
          state.tabs,
        )
    );
};
