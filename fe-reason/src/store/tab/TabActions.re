open RootActions;

type tabAction = ..;

type rootAction +=
  | AddTab
  | RemoveTab(string)
  | ActivateTab(string)
  | TabAction(string, tabAction);
