open AppViewState;
open RootActions;
open AppViewActions;

let initialState: state = {isProViewVisible: true};

let reducer = (state: state, action: rootAction): state => {
  switch (action) {
  | ToggleProView => {isProViewVisible: !state.isProViewVisible}
  | _ => state
  };
};
