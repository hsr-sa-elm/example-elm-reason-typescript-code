open FunctionUtil;
open AppViewState;

module type ParentStateOfAppViewState = {
  type parentState;
  let getAppViewState: parentState => state;
};

module Make = (ParentState: ParentStateOfAppViewState) => {
  open ParentState;

  let getIsProViewVisible =
    getAppViewState >> (state => state.isProViewVisible);
};
