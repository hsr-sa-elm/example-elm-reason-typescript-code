open FunctionUtil;
open SelectorUtil;
open HistoryState;
open HttpTypes;
open ResponseBodyFormatter;

module type ParentStateOfHistoryState = {
  type parentState;
  let getHistoryState: parentState => option(state);
};

module Make = (ParentState: ParentStateOfHistoryState) => {
  open ParentState;

  let getIsEmpty: parentState => bool =
    getHistoryState
    >> OptionUtil.map((state: state) =>
         switch (state) {
         | {selected: None, newer: [], older: []} => true
         | _ => false
         }
       )
    >> OptionUtil.withDefault(false);

  let getSelectedCall: parentState => option(performedHttpCall) =
    getHistoryState >> OptionUtil.flatMap(({selected, _}: state) => selected);

  let getSelectedRequest: parentState => option(httpCallRequest) =
    getSelectedCall
    >> OptionUtil.map(({request, _}: performedHttpCall) => request);

  let getSelectedResponse: parentState => option(httpCallResponse) =
    getSelectedCall
    >> OptionUtil.map(({response, _}: performedHttpCall) => response);

  let getIsSelectedSelector: (parentState, string) => bool =
    getHistoryState
    >> OptionUtil.map(({selected, _}: state) =>
         switch (selected) {
         | Some({id, _}) => (idToCheck => idToCheck == id)
         | None => always(false)
         }
       )
    >> OptionUtil.withDefault(always(false));

  let getAllHttpCalls: parentState => list(performedHttpCall) =
    getHistoryState
    >> OptionUtil.map(({selected, newer, older}: state) =>
         newer
         @ (
           switch (selected) {
           | Some(call) => [call]
           | None => []
           }
         )
         @ older
       )
    >> OptionUtil.withDefault([]);

  let getAllHttpCallsForView: parentState => list(performedHttpCallViewModel) =
    getSelector2(getIsSelectedSelector, getAllHttpCalls, getIsSelected =>
      List.map(performedHttpCall =>
        {performedHttpCall, isSelected: getIsSelected(performedHttpCall.id)}
      )
    );

  let getResponseBodyFormatted: parentState => string =
    getSelectedResponse
    >> (
      (response: option(httpCallResponse)) =>
        switch (response) {
        | None => ""
        | Some(r) => getFormattedBody(r)
        }
    );

  let getHasNewerHistoryEntries: parentState => bool =
    getHistoryState
    >> OptionUtil.map(({newer, _}) => newer != [])
    >> OptionUtil.withDefault(false);

  let getHasOlderHistoryEntries: parentState => bool =
    getHistoryState
    >> OptionUtil.map(({older, _}) => older != [])
    >> OptionUtil.withDefault(false);
};
