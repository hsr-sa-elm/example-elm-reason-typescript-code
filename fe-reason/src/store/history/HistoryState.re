open HttpTypes;

type state = {
  selected: option(performedHttpCall),
  newer: list(performedHttpCall),
  older: list(performedHttpCall),
};
