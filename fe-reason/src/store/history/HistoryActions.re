open TabActions;
open HttpTypes;

type tabAction +=
  | SelectHistoryEntry(performedHttpCall)
  | SelectNextNewerHistoryEntry
  | SelectNextOlderHistoryEntry;
