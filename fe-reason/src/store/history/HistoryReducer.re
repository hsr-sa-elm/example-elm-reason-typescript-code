open FunctionUtil;
open HistoryActions;
open HttpCallActions;
open HistoryState;
open HttpTypes;
module HistorySelectorInternal =
  HistorySelector.Make({
    type parentState = state;
    let getHistoryState = x => Some(x);
  });
open HistorySelectorInternal;

let initialState: state = {selected: None, newer: [], older: []};

let getNextId =
  ReducerUtil.getNextId(
    "history-entry-", ({id, _}: HttpTypes.performedHttpCall) =>
    id
  );

let reducer = {
  let splitListOn:
    (performedHttpCall, list(performedHttpCall)) =>
    (bool, list(performedHttpCall), list(performedHttpCall)) =
    element =>
      List.fold_left(
        ((found, left, right), current) =>
          if (found) {
            (true, left, right @ [current]);
          } else if (element == current) {
            (true, left, right);
          } else {
            (false, left @ [current], right);
          },
        (false, [], []),
      );
  let selectHistoryEntry: (performedHttpCall, state) => state =
    call =>
      getAllHttpCalls
      >> splitListOn(call)
      >> (
        ((found, newer, older)) => (
          if (found) {
            {selected: Some(call), newer, older};
          } else {
            {selected: None, newer, older};
          }: state
        )
      );

  let push: (performedHttpCall, state) => state =
    call =>
      getAllHttpCalls
      >> (
        (calls) => ({selected: Some(call), newer: [], older: calls}: state)
      );

  let optionToList = optionValue =>
    switch (optionValue) {
    | Some(value) => [value]
    | None => []
    };

  let selectNextNewerHistoryEntry: state => state =
    ({selected, newer, older} as state) =>
      switch (List.rev(newer)) {
      | [] => state
      | [next, ...rest] => {
          selected: Some(next),
          newer: List.rev(rest),
          older: optionToList(selected) @ older,
        }
      };

  let selectNextOlderHistoryEntry: state => state =
    ({selected, newer, older} as state) =>
      switch (older) {
      | [] => state
      | [next, ...rest] => {
          selected: Some(next),
          newer: newer @ optionToList(selected),
          older: rest,
        }
      };

  (state, action) =>
    switch (action) {
    | SelectHistoryEntry(call) => selectHistoryEntry(call, state)
    | SelectNextNewerHistoryEntry => selectNextNewerHistoryEntry(state)
    | SelectNextOlderHistoryEntry => selectNextOlderHistoryEntry(state)
    | CallSucceeded(request, response, requestTime, responseTime) =>
      push(
        {
          id: getNextId(getAllHttpCalls(state)),
          request,
          response,
          requestTime,
          responseTime,
        },
        state,
      )
    | _ => state
    };
};
