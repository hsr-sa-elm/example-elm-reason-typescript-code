open HttpCallActions;
open HttpCallState;

let initialState: state = {responseState: ShowSelectedResponse};

let reducer = (state, action) =>
  switch (action) {
  | DoCall(_) => {responseState: Loading}
  | CallSucceeded(_) => {responseState: ShowSelectedResponse}
  | CallFailed(error) => {responseState: RequestFailed(error)}
  | _ => state
  };
