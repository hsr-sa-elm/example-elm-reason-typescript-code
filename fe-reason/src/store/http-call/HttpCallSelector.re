open FunctionUtil;
open HttpCallState;

module type ParentStateOfHttpCallState = {
  type parentState;
  let getHttpCallState: parentState => option(state);
};

module Make = (ParentState: ParentStateOfHttpCallState) => {
  open ParentState;
  let getResponseState =
    getHttpCallState
    >> OptionUtil.map(state => state.responseState)
    >> OptionUtil.withDefault(ShowSelectedResponse);
};
