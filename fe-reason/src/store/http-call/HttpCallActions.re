open TabActions;
open HttpTypes;

type tabAction +=
  | DoCall(httpCallRequest)
  | CallSucceeded(httpCallRequest, httpCallResponse, float, float)
  | CallFailed(string);
