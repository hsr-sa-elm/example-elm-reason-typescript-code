type responseState =
  | ShowSelectedResponse
  | RequestFailed(string)
  | Loading;

type state = {responseState};
