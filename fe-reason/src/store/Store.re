open RootActions;
open RootState;

let initalState = {
  appView: AppViewReducer.initialState,
  tab: TabReducer.initialState,
};

let rootReducer = (state: rootState, action: rootAction) => {
  appView: AppViewReducer.reducer(state.appView, action),
  tab: TabReducer.reducer(state.tab, action),
};

let storeEnhancer =
  ReductiveDevTools.(
    Connectors.reductiveEnhancer(
      Extension.enhancerOptions(~name="Reason HTTP client", ()),
    )
  );

let middleware = (store, next) =>
  Middleware.sideEffectMiddleware(store) @@ next;

let preloadedState =
  PersistenceUtil.loadState() |> OptionUtil.withDefault(initalState);

let store =
  (storeEnhancer @@ Reductive.Store.create)(
    ~reducer=rootReducer,
    ~preloadedState,
    ~enhancer=middleware,
    (),
  );

include ReductiveContext.Make({
  type state = rootState;
  type action = rootAction;
});
