type httpRequestHeader = {
  id: string,
  key: string,
  value: string,
};

type httpResponseHeader = {
  key: string,
  value: string,
};

type httpCallRequestRequest = {
  method: string,
  url: string,
};

type httpCallRequest = {
  request: httpCallRequestRequest,
  headers: list(httpRequestHeader),
  body: string,
};

type httpCallResultFailure = {message: string};

type httpCallResponseStatus = {
  code: int,
  message: string,
};

type httpCallResponse = {
  status: httpCallResponseStatus,
  headers: list(httpResponseHeader),
  body: string,
};

type performedHttpCall = {
  id: string,
  request: httpCallRequest,
  response: httpCallResponse,
  requestTime: float,
  responseTime: float,
};

type performedHttpCallViewModel = {
  performedHttpCall,
  isSelected: bool,
};
