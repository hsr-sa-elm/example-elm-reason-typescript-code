type result('good, 'bad) =
  | Ok('good)
  | Error('bad);
