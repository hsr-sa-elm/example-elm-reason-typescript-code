open RootActions;
open TabActions;
open HistoryActions;
open RequestViewActions;
open RootState;
open RootSelector.History;

let proceed = (dispatch, action: rootAction, state: rootState) =>
  switch (action) {
  | TabAction(tabId, SelectHistoryEntry(_))
  | TabAction(tabId, SelectNextNewerHistoryEntry)
  | TabAction(tabId, SelectNextOlderHistoryEntry) =>
    switch (getSelectedRequest(state)) {
    | Some(request) =>
      dispatch(TabAction(tabId, SetRequestToEdit(request)))
    | None => ()
    }
  | _ => ()
  };
