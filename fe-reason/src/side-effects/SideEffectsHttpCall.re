open HttpTypes;
open TabActions;
open HttpCallActions;

let removeEmptyHeaders = (request: httpCallRequest): httpCallRequest => {
  ...request,
  headers:
    List.filter(
      ({key, _}: HttpTypes.httpRequestHeader) => key != "",
      request.headers,
    ),
};

let doHttpCall = (tabId: string, request: httpCallRequest, dispatch) => {
  let requestTime = Js.Date.now();
  ignore(
    Js.Promise.(
      Fetch.fetchWithInit(
        Env.backendUrl,
        Fetch.RequestInit.make(
          ~method_=Post,
          ~body=
            Fetch.BodyInit.make(
              Js.Json.stringify(
                EncodeUtil.encodeHttpRequest(removeEmptyHeaders(request)),
              ),
            ),
          ~headers=
            Fetch.HeadersInit.make({"Content-Type": "application/json"}),
          (),
        ),
      )
      |> then_(Fetch.Response.json)
      |> then_(json =>
           json
           |> DecodeUtil.decodeHttpResult
           |> CommonTypes.(
                result =>
                  switch (result) {
                  | Ok(response) =>
                    dispatch(
                      TabAction(
                        tabId,
                        CallSucceeded(
                          request,
                          response,
                          requestTime,
                          Js.Date.now(),
                        ),
                      ),
                    )
                  | Error({message}) =>
                    dispatch(TabAction(tabId, CallFailed(message)))
                  }
              )
           |> resolve
         )
      |> catch(_err => {
           dispatch(TabAction(tabId, CallFailed("Network error")));
           Js.log(_err) |> resolve;
         })
    ),
  );
};

let proceed = (dispatch, action) => {
  switch (action) {
  | TabAction(tabId, DoCall(request)) =>
    doHttpCall(tabId, request, dispatch)
  | _ => ()
  };
};
