open Jest;
open TestUtility;
open ResponseBodyFormatter;

let exampleResponse: HttpTypes.httpCallResponse = {
  status: {
    code: 200,
    message: "OK",
  },
  headers: [{key: "Content-Type", value: "application/json"}],
  body: "{\"headers\":[{\"key\":\"Content-Type\",\"value\":\"application/json\"}],\"body\":\"\"}",
};

describe("format.getFormattedBody", () =>
  assertEqual({
    given: "An application/json Response",
    should: "format the JSON Body",
    actual: () => getFormattedBody(exampleResponse),
    expected:
      "{ \"headers\":\n"
      ++ "  [ { \"key\": \"Content-Type\"\n"
      ++ "    , \"value\": \"application/json\"\n"
      ++ "    }\n"
      ++ "  ]\n"
      ++ ", \"body\": \"\"\n"
      ++ "}",
  })
);
