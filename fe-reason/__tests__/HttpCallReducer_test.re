open Jest;
open TestUtility;
open HttpCallActions;
open HttpCallReducer;
open HttpCallState;
module HttpCallSelectorInternal =
  HttpCallSelector.Make({
    type parentState = state;
    let getHttpCallState = x => Some(x);
  });
open HttpCallSelectorInternal;

let request: HttpTypes.httpCallRequest = {
  request: {
    method: "GET",
    url: "http://test.com/",
  },
  headers: [],
  body: "",
};

let response: HttpTypes.httpCallResponse = {
  status: {
    code: 200,
    message: "OK",
  },
  headers: [],
  body: "",
};

let reduceActions = List.fold_left(reducer, initialState);

describe("getResponseState", () => {
  assertEqual({
    given: "initial state",
    should: "show selected response",
    actual: () => reduceActions([]) |> getResponseState,
    expected: ShowSelectedResponse,
  });

  assertEqual({
    given: "a call is made",
    should: "be loading",
    actual: () => reduceActions([DoCall(request)]) |> getResponseState,
    expected: Loading,
  });

  assertEqual({
    given: "a call is made and succeeds",
    should: "show selected response",
    actual: () =>
      reduceActions([
        DoCall(request),
        CallSucceeded(request, response, 1., 2.),
      ])
      |> getResponseState,
    expected: ShowSelectedResponse,
  });

  assertEqual({
    given: "a call is made and fails",
    should: "show error",
    actual: () =>
      reduceActions([DoCall(request), CallFailed("some error")])
      |> getResponseState,
    expected: RequestFailed("some error"),
  });
});
