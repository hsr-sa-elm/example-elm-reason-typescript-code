open Jest;
open Expect;
open ReactTestingLibrary;
open RequestViewState;
open TestUtility;

let exampleRequest: HttpTypes.httpCallRequest = {
  request: {
    method: "POST",
    url: "http://test.com/",
  },
  headers: [
    {id: "header-1", key: "Content-Type", value: "application/json"},
  ],
  body: "foo",
};

let (setMethodSpy, setMethod) = createMockFn((_: string) => ());
let (setUrlSpy, setUrl) = createMockFn((_: string) => ());
let (setBodySpy, setBody) = createMockFn((_: string) => ());
let (setHeaderKeySpy, setHeaderKey) =
  createMockFn2((_: string, _: string) => ());
let (setHeaderValueSpy, setHeaderValue) =
  createMockFn2((_: string, _: string) => ());
let (removeHeaderSpy, removeHeader) = createMockFn((_: string) => ());
let (sendSpy, send) = createMockFn(() => ());
let (revertEditStateSpy, revertEditState) =
  createMockFn((_: HttpTypes.httpCallRequest) => ());

let renderRequest = (request: HttpTypes.httpCallRequest, editState: editState) =>
  render(
    <Request
      request
      editState
      isProViewVisible=true
      hasNewerHistoryEntries=false
      hasOlderHistoryEntries=false
      setMethod
      setUrl
      setBody
      setHeaderKey
      setHeaderValue
      removeHeader
      send
      revertEditState
      selectNextNewerHistoryEntry={() => ()}
      selectNextOlderHistoryEntry={() => ()}
    />,
  );

describe("Request component", () => {
  beforeEach(JestJs.resetAllMocks);

  test("render method", () =>
    renderRequest(exampleRequest, Changed)
    |> getByLabelText(~matcher=`Str("Method"))
    |> toHaveValue("POST")
  );
  test("render url", () =>
    renderRequest(exampleRequest, Unchanged)
    |> getByLabelText(~matcher=`Str("URL"))
    |> toHaveValue("http://test.com/")
  );
  test("render body", () =>
    renderRequest(exampleRequest, Unchanged)
    |> getByLabelText(~matcher=`Str("Body"))
    |> toHaveValue("foo")
  );
  // The header fields are only tested with a single header because the BuckleScript bindings of
  // the React-Testing-Library do not include the getAll* functions
  test("render header name", () =>
    renderRequest(exampleRequest, Unchanged)
    |> getByPlaceholderText("Name")
    |> toHaveValue("Content-Type")
  );
  test("render header value", () =>
    renderRequest(exampleRequest, Unchanged)
    |> getByPlaceholderText("Value")
    |> toHaveValue("application/json")
  );

  test("set method", () => {
    renderRequest(exampleRequest, Unchanged)
    |> getByLabelText(~matcher=`Str("Method"))
    |> FireEvent.change(~eventInit={
                          "target": {
                            "value": "PUT",
                          },
                        });
    expect(setMethodSpy |> MockJs.calls) |> toEqual([|"PUT"|]);
  });
  test("set url", () => {
    renderRequest(exampleRequest, Unchanged)
    |> getByLabelText(~matcher=`Str("URL"))
    |> FireEvent.change(~eventInit={
                          "target": {
                            "value": "http://x.com",
                          },
                        });
    expect(setUrlSpy |> MockJs.calls) |> toEqual([|"http://x.com"|]);
  });
  test("set body", () => {
    renderRequest(exampleRequest, Unchanged)
    |> getByLabelText(~matcher=`Str("Body"))
    |> FireEvent.change(~eventInit={
                          "target": {
                            "value": "Some body",
                          },
                        });
    expect(setBodySpy |> MockJs.calls) |> toEqual([|"Some body"|]);
  });
  test("set header name", () => {
    renderRequest(exampleRequest, Unchanged)
    |> getByPlaceholderText("Name")
    |> FireEvent.change(~eventInit={
                          "target": {
                            "value": "Accept",
                          },
                        });
    expect(setHeaderKeySpy |> MockJs.calls)
    |> toEqual([|("header-1", "Accept")|]);
  });
  test("set header value", () => {
    renderRequest(exampleRequest, Unchanged)
    |> getByPlaceholderText("Value")
    |> FireEvent.change(~eventInit={
                          "target": {
                            "value": "*/*",
                          },
                        });
    expect(setHeaderValueSpy |> MockJs.calls)
    |> toEqual([|("header-1", "*/*")|]);
  });
  test("remove header", () => {
    renderRequest(
      {
        ...exampleRequest,
        headers: [
          {id: "header-1", key: "Content-Type", value: "application/json"},
          {id: "header-2", key: "", value: ""},
        ],
      },
      Unchanged,
    )
    |> getByText(~matcher=`Str("remove"))
    |> FireEvent.click;
    expect(removeHeaderSpy |> MockJs.calls) |> toEqual([|"header-1"|]);
  });
  test("send", () => {
    renderRequest(exampleRequest, Unchanged)
    |> getByText(~matcher=`Str("Send"))
    |> FireEvent.click;
    expect(sendSpy |> MockJs.calls) |> toEqual([|()|]);
  });
  test("revert edit state", () => {
    let oldRequest = {...exampleRequest, body: "some other body"};
    renderRequest(exampleRequest, Overwritten(oldRequest))
    |> getByText(~matcher=`Str("Undo"))
    |> FireEvent.click;
    expect(revertEditStateSpy |> MockJs.calls) |> toEqual([|oldRequest|]);
  });
});
