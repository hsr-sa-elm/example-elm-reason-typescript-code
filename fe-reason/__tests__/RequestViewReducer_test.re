open Jest;
open TestUtility;
open HttpCallActions;
open RequestViewActions;
open RequestViewReducer;
open RequestViewState;
module RequestViewSelectorInternal =
  RequestViewSelector.Make({
    type parentState = state;
    let getRequestViewState = x => Some(x);
  });
open RequestViewSelectorInternal;

let request = initialState.requestToEdit;
let changedRequest = {
  ...request,
  request: {
    method: "POST",
    url: "http://x.com",
  },
};

let reduceActions = List.fold_left(reducer, initialState);

let response: HttpTypes.httpCallResponse = {
  status: {
    code: 200,
    message: "OK",
  },
  headers: [],
  body: "",
};
let callSuccededAction = CallSucceeded(request, response, 1., 2.);

describe("request-view state", () => {
  {
    let given = "initial state";
    let state = reduceActions([]);
    assertEqual({
      given,
      should: "contain initial request",
      actual: () => getRequestToEdit(state),
      expected: Some(request),
    });
    assertEqual({
      given,
      should: "be unchanged",
      actual: () => getEditState(state),
      expected: Unchanged,
    });
  };

  {
    let given = "method is set";
    let state = reduceActions([SetMethod("POST")]);
    assertEqual({
      given,
      should: "contain the new method",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          request: {
            ...request.request,
            method: "POST",
          },
        }),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "url is set";
    let state = reduceActions([SetUrl("https://secure.com")]);
    assertEqual({
      given,
      should: "contain the new url",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          request: {
            ...request.request,
            url: "https://secure.com",
          },
        }),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "body is set";
    let state = reduceActions([SetBody("foo")]);
    assertEqual({
      given,
      should: "contain the new body",
      actual: () => getRequestToEdit(state),
      expected: Some({...request, body: "foo"}),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "header is removed";
    let state = reduceActions([RemoveHeader("header-0")]);
    assertEqual({
      given,
      should: "not contain the removed header",
      actual: () => getRequestToEdit(state),
      expected: Some({...request, headers: List.tl(request.headers)}),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "header key is changed";
    let state =
      reduceActions([SetHeaderKey({id: "header-0", newKey: "some key"})]);
    assertEqual({
      given,
      should: "contain the changed key",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          headers: [
            {...List.hd(request.headers), key: "some key"},
            ...List.tl(request.headers),
          ],
        }),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "header value is changed";
    let state =
      reduceActions([
        SetHeaderValue({id: "header-0", newValue: "some value"}),
      ]);
    assertEqual({
      given,
      should: "contain the changed value",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          headers: [
            {...List.hd(request.headers), value: "some value"},
            ...List.tl(request.headers),
          ],
        }),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "last header value is changed";
    let state =
      reduceActions([
        SetHeaderValue({id: "header-2", newValue: "some value"}),
      ]);
    assertEqual({
      given,
      should: "add new row",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          headers: [
            {id: "header-0", key: "Content-Type", value: "application/json"},
            {id: "header-1", key: "Cache-Control", value: "no-cache"},
            {id: "header-2", key: "", value: "some value"},
            {id: "header-3", key: "", value: ""},
          ],
        }),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "method and url are set and then overwritten";
    let state =
      reduceActions([
        SetMethod("POST"),
        SetUrl("https://secure.com"),
        SetMethod("PUT"),
        SetUrl("http://example.com"),
      ]);
    assertEqual({
      given,
      should: "contain the latest method and url",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          request: {
            method: "PUT",
            url: "http://example.com",
          },
        }),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "an edited request is executed";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        callSuccededAction,
      ]);
    assertEqual({
      given,
      should: "not change request",
      actual: () => getRequestToEdit(state),
      expected:
        Some({
          ...request,
          request: {
            method: "PUT",
            url: "http://example.com",
          },
        }),
    });
    assertEqual({
      given,
      should: "be unchanged",
      actual: () => getEditState(state),
      expected: Unchanged,
    });
  };

  {
    let given = "a edited request is overwritten by a selected history entry";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        SetRequestToEdit(changedRequest),
      ]);
    assertEqual({
      given,
      should: "contain the request of the history entry",
      actual: () => getRequestToEdit(state),
      expected: Some(changedRequest),
    });
    assertEqual({
      given,
      should: "have an overwritten request",
      actual: () => getEditState(state),
      expected:
        Overwritten({
          ...request,
          request: {
            method: "PUT",
            url: "http://example.com",
          },
        }),
    });
  };

  {
    let given = "an overwritten request is reverted";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        SetRequestToEdit(changedRequest),
        RevertEditState({...request, body: "other body"}),
      ]);
    assertEqual({
      given,
      should: "contain the reverted request",
      actual: () => getRequestToEdit(state),
      expected: Some({...request, body: "other body"}),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "a sent request is overwritten by a selected history entry";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        callSuccededAction,
        SetRequestToEdit(changedRequest),
      ]);
    assertEqual({
      given,
      should: "contain the request from the history",
      actual: () => getRequestToEdit(state),
      expected: Some(changedRequest),
    });
    assertEqual({
      given,
      should: "be unchanged",
      actual: () => getEditState(state),
      expected: Unchanged,
    });
  };

  {
    let given = "a edited request is overwritten twice";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        SetRequestToEdit(changedRequest),
        SetRequestToEdit({...changedRequest, body: "foobar"}),
      ]);
    assertEqual({
      given,
      should: "contain the last selected request from the history",
      actual: () => getRequestToEdit(state),
      expected: Some({...changedRequest, body: "foobar"}),
    });
    assertEqual({
      given,
      should: "have an overwritten request",
      actual: () => getEditState(state),
      expected:
        Overwritten({
          ...request,
          request: {
            method: "PUT",
            url: "http://example.com",
          },
        }),
    });
  };

  {
    let given = "a selected request which overwrote another gets changed";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        SetRequestToEdit(changedRequest),
        SetBody("foo"),
      ]);
    assertEqual({
      given,
      should: "contain the edited request from the history",
      actual: () => getRequestToEdit(state),
      expected: Some({...changedRequest, body: "foo"}),
    });
    assertEqual({
      given,
      should: "be changed",
      actual: () => getEditState(state),
      expected: Changed,
    });
  };

  {
    let given = "a selected request which overwrote another is executed";
    let state =
      reduceActions([
        SetMethod("PUT"),
        SetUrl("http://example.com"),
        SetRequestToEdit(changedRequest),
        callSuccededAction,
      ]);
    assertEqual({
      given,
      should: "contain the request from the history",
      actual: () => getRequestToEdit(state),
      expected: Some(changedRequest),
    });
    assertEqual({
      given,
      should: "be unchanged",
      actual: () => getEditState(state),
      expected: Unchanged,
    });
  };

  ();
});
