open Jest;
open TestUtility;
open FunctionUtil;
open AppViewActions;
open AppViewReducer;
open AppViewState;
module AppViewSelectorInternal =
  AppViewSelector.Make({
    type parentState = state;
    let getAppViewState = identity;
  });
open AppViewSelectorInternal;

let reduceActions = List.fold_left(reducer, initialState);

describe("app view state", () => {
  assertEqual({
    given: "initial state",
    should: "be in pro view",
    actual: () => getIsProViewVisible(reduceActions([])),
    expected: true,
  });

  assertEqual({
    given: "toggled once",
    should: "be in simple view",
    actual: () => getIsProViewVisible(reduceActions([ToggleProView])),
    expected: false,
  });

  assertEqual({
    given: "toggled twice",
    should: "be in pro view",
    actual: () =>
      getIsProViewVisible(reduceActions([ToggleProView, ToggleProView])),
    expected: true,
  });
});
