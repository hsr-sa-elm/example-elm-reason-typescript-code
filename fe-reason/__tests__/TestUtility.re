open Jest;
open JestDom;

type assertionDescription('a) = {
  given: string,
  should: string,
  actual: unit => 'a,
  expected: 'a,
};

let assertEqual =
    ({given, should, actual, expected}: assertionDescription('a)) =>
  test("Given " ++ given ++ ": should " ++ should, () =>
    Expect.(expect(actual()) |> toEqual(expected))
  );

// Adapted from https://github.com/wyze/bs-jest-dom/blob/master/src/JestDom.re
[%bs.raw {|require('jest-dom/extend-expect')|}];
[@bs.val] external expectJest: t => Js.t({..}) = "expect";

let toHaveValue = (value: string, element) =>
  Expect.expect(() =>
    expectJest(element)##toHaveValue(value)
  )
  |> Expect.not_
  |> Expect.toThrow;

let createMockFn = fn => JestJs.fn(fn) |> (spy => (spy, spy |> MockJs.fn));
let createMockFn2 = (fn: ('a, 'b) => 'c) =>
  JestJs.fn2((. x: 'a, y: 'b) => fn(x, y))
  |> (spy => (spy, spy |> MockJs.fn |> ((mockFn, a, b) => mockFn(. a, b))));
