open Jest;
open TestUtility;
open HistoryActions;
open HttpCallActions;
open HistoryReducer;
open HistorySelectorInternal;
open HttpTypes;

let request1: HttpTypes.httpCallRequest = {
  request: {
    method: "GET",
    url: "http://test.com/",
  },
  headers: [],
  body: "",
};
let request2: HttpTypes.httpCallRequest = {
  ...request1,
  request: {
    method: "POST",
    url: "http://example.com/",
  },
};

let response1: HttpTypes.httpCallResponse = {
  status: {
    code: 200,
    message: "OK",
  },
  headers: [],
  body: "",
};
let response2: HttpTypes.httpCallResponse = {
  ...response1,
  body: "other body",
};
let response3: HttpTypes.httpCallResponse = {
  ...response1,
  status: {
    code: 400,
    message: "Bad Request",
  },
};

let call1 = {
  id: "history-entry-1",
  request: request1,
  response: response1,
  requestTime: 1.,
  responseTime: 2.,
};
let call2 = {
  id: "history-entry-2",
  request: request2,
  response: response2,
  requestTime: 3.,
  responseTime: 4.,
};
let call3 = {
  id: "history-entry-3",
  request: request1,
  response: response3,
  requestTime: 5.,
  responseTime: 6.,
};

let callAction1 =
  CallSucceeded(
    call1.request,
    call1.response,
    call1.requestTime,
    call1.responseTime,
  );
let callAction2 =
  CallSucceeded(
    call2.request,
    call2.response,
    call2.requestTime,
    call2.responseTime,
  );
let callAction3 =
  CallSucceeded(
    call3.request,
    call3.response,
    call3.requestTime,
    call3.responseTime,
  );

let reduceActions = List.fold_left(reducer, initialState);

describe("getIsEmpty", () => {
  assertEqual({
    given: "initial state",
    should: "be empty",
    actual: () => reduceActions([]) |> getIsEmpty,
    expected: true,
  });

  assertEqual({
    given: "a call is added to the history",
    should: "not be empty",
    actual: () => reduceActions([callAction1]) |> getIsEmpty,
    expected: false,
  });
});

describe("getSelectedResponse", () => {
  assertEqual({
    given: "initial state",
    should: "return none",
    actual: () => reduceActions([]) |> getSelectedResponse,
    expected: None,
  });

  assertEqual({
    given: "a call is added to the history",
    should: "select the response",
    actual: () => reduceActions([callAction1]) |> getSelectedResponse,
    expected: Some(response1),
  });

  assertEqual({
    given: "multiple calls are added to the history",
    should: "select the last response",
    actual: () =>
      reduceActions([callAction1, callAction2]) |> getSelectedResponse,
    expected: Some(response2),
  });

  assertEqual({
    given: "multiple calls are added to the history and an old one is selected",
    should: "return the last selected response",
    actual: () =>
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        SelectHistoryEntry(call2),
        SelectHistoryEntry(call1),
      ])
      |> getSelectedResponse,
    expected: Some(response1),
  });
});

describe("getAllHttpCallsForView", () => {
  assertEqual({
    given: "initial state",
    should: "be an empty list",
    actual: () => reduceActions([]) |> getAllHttpCallsForView,
    expected: [],
  });

  assertEqual({
    given: "a call is added to the history",
    should: "add it to the list",
    actual: () => reduceActions([callAction1]) |> getAllHttpCallsForView,
    expected: [{performedHttpCall: call1, isSelected: true}],
  });

  assertEqual({
    given: "multiple calls are added to the history",
    should: "should push new calls to the front",
    actual: () =>
      reduceActions([callAction1, callAction2, callAction3])
      |> getAllHttpCallsForView,
    expected: [
      {performedHttpCall: call3, isSelected: true},
      {performedHttpCall: call2, isSelected: false},
      {performedHttpCall: call1, isSelected: false},
    ],
  });

  assertEqual({
    given: "multiple calls are added to the history and an old one is selected",
    should: "keep order but mark the selected one",
    actual: () =>
      reduceActions([
        callAction1,
        callAction2,
        SelectHistoryEntry(call2),
        callAction3,
        SelectHistoryEntry(call1),
      ])
      |> getAllHttpCallsForView,
    expected: [
      {performedHttpCall: call3, isSelected: false},
      {performedHttpCall: call2, isSelected: false},
      {performedHttpCall: call1, isSelected: true},
    ],
  });
});

describe("older and newer entries", () => {
  {
    let given = "initial state";
    let state = reduceActions([]);
    assertEqual({
      given,
      should: "not have newer entries",
      actual: () => getHasNewerHistoryEntries(state),
      expected: false,
    });
    assertEqual({
      given,
      should: "not have older entries",
      actual: () => getHasOlderHistoryEntries(state),
      expected: false,
    });
    assertEqual({
      given,
      should: "have zero entries",
      actual: () => getAllHttpCallsForView(state),
      expected: [],
    });
  };

  {
    let given = "three calls are added to the history";
    let state = reduceActions([callAction1, callAction2, callAction3]);
    assertEqual({
      given,
      should: "not have newer entries",
      actual: () => getHasNewerHistoryEntries(state),
      expected: false,
    });
    assertEqual({
      given,
      should: "have older entries",
      actual: () => getHasOlderHistoryEntries(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "select newest",
      actual: () => getAllHttpCallsForView(state),
      expected: [
        {performedHttpCall: call3, isSelected: true},
        {performedHttpCall: call2, isSelected: false},
        {performedHttpCall: call1, isSelected: false},
      ],
    });
  };

  {
    let given = "three calls are added to the history and oldest is selected";
    let state =
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        SelectHistoryEntry(call1),
      ]);
    assertEqual({
      given,
      should: "have newer entries",
      actual: () => getHasNewerHistoryEntries(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "not have older entries",
      actual: () => getHasOlderHistoryEntries(state),
      expected: false,
    });
    assertEqual({
      given,
      should: "select oldest",
      actual: () => getAllHttpCallsForView(state),
      expected: [
        {performedHttpCall: call3, isSelected: false},
        {performedHttpCall: call2, isSelected: false},
        {performedHttpCall: call1, isSelected: true},
      ],
    });
  };

  {
    let given = "three calls are added to the history and then the next older call is selected";
    let state =
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        SelectNextOlderHistoryEntry,
      ]);
    assertEqual({
      given,
      should: "have newer entries",
      actual: () => getHasNewerHistoryEntries(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "have older entries",
      actual: () => getHasOlderHistoryEntries(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "select second call",
      actual: () => getAllHttpCallsForView(state),
      expected: [
        {performedHttpCall: call3, isSelected: false},
        {performedHttpCall: call2, isSelected: true},
        {performedHttpCall: call1, isSelected: false},
      ],
    });
  };

  {
    let given = "oldest call is selected and then the next newer call is selected";
    let state =
      reduceActions([
        callAction1,
        callAction2,
        callAction3,
        SelectHistoryEntry(call1),
        SelectNextNewerHistoryEntry,
      ]);
    assertEqual({
      given,
      should: "have newer entries",
      actual: () => getHasNewerHistoryEntries(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "have older entries",
      actual: () => getHasOlderHistoryEntries(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "select second call",
      actual: () => getAllHttpCallsForView(state),
      expected: [
        {performedHttpCall: call3, isSelected: false},
        {performedHttpCall: call2, isSelected: true},
        {performedHttpCall: call1, isSelected: false},
      ],
    });
  };

  ();
});
