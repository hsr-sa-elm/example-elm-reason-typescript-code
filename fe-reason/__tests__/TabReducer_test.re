open Jest;
open TestUtility;
open FunctionUtil;
open TabActions;
open TabReducer;
open TabState;
module TabSelectorInternal =
  TabSelector.Make({
    type parentState = state;
    let getTabState = identity;
  });
open TabSelectorInternal;

let reduceActions = List.fold_left(reducer, initialState);

describe("tab state", () => {
  {
    let given = "initial state";
    let state = reduceActions([]);
    assertEqual({
      given,
      should: "have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "have a selected tab",
      actual: () => getSelectedTabId(state),
      expected: "tab-1",
    });
    assertEqual({
      given,
      should: "contain one tab",
      actual: () => getAllTabs(state),
      expected: [{id: "tab-1", method: "GET", url: "", isActive: true}],
    });
  };

  {
    let given = "a tab is added";
    let state = reduceActions([AddTab]);
    assertEqual({
      given,
      should: "have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "select the new tab",
      actual: () => getSelectedTabId(state),
      expected: "tab-2",
    });
    assertEqual({
      given,
      should: "contain two tabs",
      actual: () => getAllTabs(state),
      expected: [
        {id: "tab-1", method: "GET", url: "", isActive: false},
        {id: "tab-2", method: "GET", url: "", isActive: true},
      ],
    });
  };

  {
    let given = "a tab is added and the old one gets selected";
    let state = reduceActions([AddTab, ActivateTab("tab-1")]);
    assertEqual({
      given,
      should: "have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "select the old tab",
      actual: () => getSelectedTabId(state),
      expected: "tab-1",
    });
    assertEqual({
      given,
      should: "contain two tabs",
      actual: () => getAllTabs(state),
      expected: [
        {id: "tab-1", method: "GET", url: "", isActive: true},
        {id: "tab-2", method: "GET", url: "", isActive: false},
      ],
    });
  };

  {
    let given = "a tab is added and a non existing tab gets selected";
    let state = reduceActions([AddTab, ActivateTab("tab-3")]);
    assertEqual({
      given,
      should: "not have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: false,
    });
    assertEqual({
      given,
      should: "select the non existing tab",
      actual: () => getSelectedTabId(state),
      expected: "tab-3",
    });
    assertEqual({
      given,
      should: "contain the two not selected tabs",
      actual: () => getAllTabs(state),
      expected: [
        {id: "tab-1", method: "GET", url: "", isActive: false},
        {id: "tab-2", method: "GET", url: "", isActive: false},
      ],
    });
  };

  {
    let given = "a not selected tab is removed";
    let state = reduceActions([AddTab, AddTab, RemoveTab("tab-2")]);
    assertEqual({
      given,
      should: "have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "not changed the selected tab",
      actual: () => getSelectedTabId(state),
      expected: "tab-3",
    });
    assertEqual({
      given,
      should: "not contain the removed tab",
      actual: () => getAllTabs(state),
      expected: [
        {id: "tab-1", method: "GET", url: "", isActive: false},
        {id: "tab-3", method: "GET", url: "", isActive: true},
      ],
    });
  };

  {
    let given = "the selected tab is removed";
    let state = reduceActions([AddTab, AddTab, RemoveTab("tab-3")]);
    assertEqual({
      given,
      should: "have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: true,
    });
    assertEqual({
      given,
      should: "select the first tab",
      actual: () => getSelectedTabId(state),
      expected: "tab-1",
    });
    assertEqual({
      given,
      should: "not contain the removed tab",
      actual: () => getAllTabs(state),
      expected: [
        {id: "tab-1", method: "GET", url: "", isActive: true},
        {id: "tab-2", method: "GET", url: "", isActive: false},
      ],
    });
  };

  {
    let given = "the only tab is removed";
    let state = reduceActions([RemoveTab("tab-1")]);
    assertEqual({
      given,
      should: "not have tab data",
      actual: () => getHasCurrentTabData(state),
      expected: false,
    });
    assertEqual({
      given,
      should: "have no tab selected",
      actual: () => getSelectedTabId(state),
      expected: "",
    });
    assertEqual({
      given,
      should: "not contain any tabs",
      actual: () => getAllTabs(state),
      expected: [],
    });
  };

  assertEqual({
    given: "a tab action is triggered in a not selected tab",
    should: "not change the current tab data",
    actual: () =>
      reduceActions([
        AddTab,
        TabAction("tab-1", RequestViewActions.SetBody("foo")),
      ])
      |> getCurrentTabData,
    expected: Some(initialTabData),
  });

  assertEqual({
    given: "a tab action is triggered in a not selected tab which is afterwards selected",
    should: "contain the changed tab data",
    actual: () =>
      reduceActions([
        AddTab,
        TabAction("tab-1", RequestViewActions.SetBody("foo")),
        ActivateTab("tab-1"),
      ])
      |> getCurrentTabData,
    expected:
      Some({
        ...initialTabData,
        requestViewState: {
          editState: RequestViewState.Changed,
          requestToEdit: {
            ...initialTabData.requestViewState.requestToEdit,
            body: "foo",
          },
        },
      }),
  });

  assertEqual({
    given: "a tab action is triggered in the selected tab",
    should: "contain the changed tab data",
    actual: () =>
      reduceActions([
        AddTab,
        TabAction("tab-2", RequestViewActions.SetBody("foo")),
      ])
      |> getCurrentTabData,
    expected:
      Some({
        ...initialTabData,
        requestViewState: {
          editState: RequestViewState.Changed,
          requestToEdit: {
            ...initialTabData.requestViewState.requestToEdit,
            body: "foo",
          },
        },
      }),
  });

  ();
});
